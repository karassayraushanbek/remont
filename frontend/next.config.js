/** @type {import('next').NextConfig} */

const nextConfig = {
    output: "standalone",
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: process.env.API_HOST,
                port: '',
                pathname: '/storage/**',
            },
            {
                protocol: 'https',
                hostname: process.env.API_HOST,
                port: '',
                pathname: '/vendor/**',
            }
        ],
    },
    async rewrites() {
        return [
            {
                source: "/robots.txt",
                destination: "/api/robots"
            }
        ]
    }
};

module.exports = nextConfig;
