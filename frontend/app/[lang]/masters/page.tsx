import Carousel from "@/app/components/common/Carousel";
import FormCard from "@/app/components/common/FormCard";
import Hero from "@/app/components/common/Hero";
import MasterCard from "@/app/components/common/MasterCard";
import { MastersPageData, ServicesPageData } from "@/types";
import { Metadata } from "next";
import { getDictionary } from "../dictionaries";
import {} from "../services/page";
import { getPhoneNumbers } from "@/app/api/getPhoneNumbers";

async function getMastersPageData(lang: string): Promise<MastersPageData> {
  const res = await fetch(`${process.env.API_URL}/page/master?lang=${lang}`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch masters page data");
  }

  return res.json();
}

export async function generateMetadata({
  params: { lang },
}: {
  params: { lang: string };
}): Promise<Metadata> {
  const {
    meta_title: metaTitle,
    meta_description: metaDescription,
    meta_keywords: metaKeywords,
  } = await getMastersPageData(lang);

  const title = metaTitle;
  const description = metaDescription;
  const keywords = metaKeywords;

  return {
    title,
    description,
    keywords,
  };
}

async function getServices(lang: string): Promise<ServicesPageData> {
  const res = await fetch(`${process.env.API_URL}/page/service?lang=${lang}`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch services page data");
  }

  return res.json();
}

const MastersPage = async ({
  params: { lang },
}: {
  params: { lang: string };
}) => {
  const { banner, masters } = await getMastersPageData(lang);

  const { services } = await getServices(lang);

  const { data: phoneNumbers } = await getPhoneNumbers();

  const { buttons, inputs, mastersPage, mastersTitle } = await getDictionary(
    lang
  );

  return (
    <main className="space-y-14">
      <Hero imgSrc={banner.image}>
        <FormCard
          title={mastersPage.form.title}
          body={mastersPage.form.description}
          selectOptions={services}
          buttonTitle={buttons.buttonTitle1}
          inputsText={inputs}
        />
      </Hero>
      <Carousel
        title={mastersTitle}
        type=""
        style={{ marginTop: "var(--custom-margin-top-masters-page)" }}
      >
        {masters.slice(0, 8).map((master) => (
          <MasterCard
            key={master.id}
            tags={master.tags}
            name={master.name}
            location={master.distance}
            experience={master.experience}
            imgSrc={master.image}
            buttons={buttons}
            phoneNumbers={phoneNumbers}
          />
        ))}
      </Carousel>
    </main>
  );
};

export default MastersPage;
