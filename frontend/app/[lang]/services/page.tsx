import Carousel from "@/app/components/common/Carousel";
import FourCards2 from "@/app/components/common/FourCards2";
import ServiceCard from "@/app/components/common/ServiceCard";
import HeroServices from "@/app/components/services/HeroServices";
import ServicesDescription from "@/app/components/services/ServicesDescription";
import { ServicesPageData } from "@/types";
import { Metadata } from "next";
import Image from "next/image";
import { getDictionary } from "../dictionaries";
import Grid from "@/app/components/common/Grid";
import { getCall } from "@/app/api/getCall";
import { getPhoneNumbers } from "@/app/api/getPhoneNumbers";

async function getServicesPageData(lang: string): Promise<ServicesPageData> {
  const res = await fetch(`${process.env.API_URL}/page/service?lang=${lang}`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch services page data");
  }

  return res.json();
}

export async function generateMetadata({
  params: { lang },
}: {
  params: { lang: string };
}): Promise<Metadata> {
  const {
    meta_title: metaTitle,
    meta_description: metaDescription,
    meta_keywords: metaKeywords,
  } = await getServicesPageData(lang);

  const title = metaTitle;
  const description = metaDescription;
  const keywords = metaKeywords;

  return {
    title,
    description,
    keywords,
  };
}

const ServicesPage = async ({
  params: { lang },
}: {
  params: { lang: string };
}) => {
  const {
    banner,
    marks,
    services,
    description_services: descriptionServices,
    tags,
  } = await getServicesPageData(lang);
  // const { call } = await getCall();
  const { data: phoneNumbers } = await getPhoneNumbers();
  const { buttons, servicesPage, markTitle } = await getDictionary(lang);

  return (
    <main className="space-y-20">
      <HeroServices
        imgSrc={banner.image}
        title={banner.title}
        description={banner.description}
      />
      <Grid
        title={servicesPage.serviceTitle}
        style={{ marginTop: "var(--custom-margin-top-service-page)" }}
      >
        {services.map((service, i) => (
          <ServiceCard
            key={service.id}
            title={service.title}
            body={service.description}
            type={i + 1}
            buttons={buttons}
            // call={phoneNumbers[0].phone}
            call="+77071948259"
          />
        ))}
      </Grid>
      <ServicesDescription
        title={servicesPage.serviceDescription.title}
        tags={tags}
        descriptionServices={descriptionServices}
        buttons={buttons}
        // call={phoneNumbers[0].phone}
        call="+77071948259"
      />
      <Carousel title={markTitle} type="brand">
        {marks.map((mark) => {
          return (
            <div
              className="md:flex-[0_0_20%] relative flex-[0_0_50%]"
              key={mark.id}
            >
              {mark.image && (
                <Image
                  src={`${process.env.API_BASE_URL}${mark.image}`}
                  alt={mark.alt}
                  width={300}
                  height={300}
                  className="rounded-3xl"
                />
              )}
            </div>
          );
        })}
      </Carousel>
    </main>
  );
};

export default ServicesPage;
