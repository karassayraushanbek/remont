import "server-only";

type FlexibleDictionaryModule = {
  default?: Record<string, string>;
  [key: string]: any;
};

type Dictionaries = Record<string, () => Promise<FlexibleDictionaryModule>>;

const dictionaries: Dictionaries = {
  en: () => import("./dictionaries/en.json").then((module) => module.default),
  kz: () => import("./dictionaries/kz.json").then((module) => module.default),
  ru: () => import("./dictionaries/ru.json").then((module) => module.default),
};

export const getDictionary = async (
  locale: string
): Promise<FlexibleDictionaryModule> => {
  const dictionaryFunction = dictionaries[locale];

  if (!dictionaryFunction) {
    if (
      locale === "sw.js" ||
      locale === "react_devtools_backend_compact.js.map"
    )
      return {};
    console.error(`Dictionary for locale "${locale}" not found.`);
    return {};
  }

  try {
    const dictionaryModule = await dictionaryFunction();
    return dictionaryModule || {};
  } catch (error) {
    console.error(`Error loading dictionary for locale "${locale}":`, error);
    return {};
  }
};
