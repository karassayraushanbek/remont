import { Roboto } from "next/font/google";

import Footer from "../components/common/Footer";
import Navbar from "../components/common/Navbar";
import "../globals.css";
import Script from "next/script";

const roboto = Roboto({
  weight: ["300", "400", "500", "700"],
  subsets: ["latin"],
});

const schema = {
  "@context": "http://schema.org",
  "@type": "Service",
  name: "Ремонт бытовой техники",
  description:
    "Мы предоставляем качественные услуги по ремонту стиральных машин, посудомоечных машин, холодильников и другой бытовой техники. Наши опытные мастера оперативно и качественно устраняют любые неисправности, обеспечивая надежность и долговечность вашей техники.",
  provider: {
    "@type": "Organization",
    name: "Название вашей компании",
    url: "https://remontmaster.kz",
  },
  offers: {
    "@type": "Offer",
    priceCurrency: "Тенге",
    price: "[Цена за услугу]",
    availability: "http://schema.org/InStock",
  },
};

export default function RootLayout({
  children,
  params: { lang },
}: {
  children: React.ReactNode;
  params: { lang: string };
}) {
  return (
    <html lang={lang}>
      <head>
        {/* Google Tag Manager */}
        <Script
          async
          src={`https://www.googletagmanager.com/ns.html?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID}`}
        />
        <Script id="google-analytics">
          {`
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID}');
          `}
        </Script>
        {/* End Google Tag Manager */}
        {/* Yandex.Metrika counter */}
        <Script type="text/javascript" id="yandex-metrics">
          {`(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();
            for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
            k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(97049195, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });`}
        </Script>
        <noscript>
          <div>
            {/* eslint-disable-next-line @next/next/no-img-element */}
            <img
              src="https://mc.yandex.ru/watch/97049195"
              style={{ position: "absolute", left: "-9999px" }}
              alt="yandex metric"
            />
          </div>
        </noscript>
        {/* /Yandex.Metrika counter */}
        {/* Open Graph */}
        <meta property="og:title" content="Ремонт бытовой техники" />
        <meta
          property="og:description"
          content="Мы предоставляем качественные услуги по ремонту стиральных машин, посудомоечных машин, холодильников и другой бытовой техники. Наши опытные мастера оперативно и качественно устраняют любые неисправности, обеспечивая надежность и долговечность вашей техники."
        />
        <meta property="og:image" content="URL изображения" />
        <meta property="og:url" content="URL страницы" />
        <meta property="og:type" content="website" />
        {/* Open Graph */}
        {/* Google verification for seo analyzer */}
        <meta
          name="google-site-verification"
          content="mnUqj_WWTIyTYAFWkvSJRDBtWmelYqlaI6lTuslnSRA"
        />
        {/* Google verification for seo analyzer */}
        {/* Schema org */}
        <Script
          type="application/ld+json"
          id="schema"
          dangerouslySetInnerHTML={{ __html: JSON.stringify(schema) }}
        />
        {/* Schema org */}
      </head>
      <body
        className={`${roboto.className} md:pt-0 py-5 md:px-10 px-3 bg-[#F3F3F3]`}
      >
        {/* <!-- Google Tag Manager (noscript) --> */}
        <noscript>
          <iframe
            src={`https://www.googletagmanager.com/ns.html?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID}`}
            height="0"
            width="0"
            style={{ display: "none", visibility: "hidden" }}
          ></iframe>
        </noscript>
        {/* <!-- End Google Tag Manager (noscript) --> */}
        <Navbar lang={lang} />
        {children}
        <Footer lang={lang} />
      </body>
    </html>
  );
}
