import FactCard from "@/app/components/about/FactCard";
import HeroAbout from "@/app/components/about/HeroAbout";
import Carousel from "@/app/components/common/Carousel";
import FourCard from "@/app/components/common/FourCard";
import FourCards from "@/app/components/common/FourCards";
import { AboutPageData } from "@/types";
import { Metadata } from "next";
import Image from "next/image";
import { getDictionary } from "../dictionaries";
import FourCard3 from "@/app/components/common/FourCard3";
import FourCards3 from "@/app/components/common/FourCards3";

async function getAboutPageData(lang: string): Promise<AboutPageData> {
  const res = await fetch(`${process.env.API_URL}/page/about?lang=${lang}`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch about page data");
  }

  return res.json();
}

export async function generateMetadata({
  params: { lang },
}: {
  params: { lang: string };
}): Promise<Metadata> {
  const {
    meta_title: metaTitle,
    meta_description: metaDescription,
    meta_keywords: metaKeywords,
  } = await getAboutPageData(lang);

  const title = metaTitle;
  const description = metaDescription;
  const keywords = metaKeywords;

  return {
    title,
    description,
    keywords,
  };
}

const AboutPage = async ({
  params: { lang },
}: {
  params: { lang: string };
}) => {
  const {
    banner,
    about: aboutS,
    certificates,
    facts,
  } = await getAboutPageData(lang);

  const { aboutPage } = await getDictionary(lang);

  return (
    <main className="md:space-y-20 space-y-14">
      <HeroAbout
        imgSrc={banner.image}
        title={banner.title}
        description={banner.description}
      />
      <FourCards title={aboutPage.aboutTitle}>
        {aboutS.map((about, i) => (
          <FourCard
            key={about.id}
            title={about.title}
            body={about.description}
            imgSrc={about.image}
            type={i}
          />
        ))}
      </FourCards>
      <FourCards3>
        {aboutS.map((about, i) => (
          <FourCard3
            key={about.id}
            title={about.title}
            body={about.description}
            imgSrc={`/about${i + 1}.svg`}
            type={i + 1}
          />
        ))}
      </FourCards3>
      <Carousel
        title={aboutPage.certificateTitle}
        body={aboutPage.certificateDescription}
        type=""
      >
        {certificates.map((certificate) => {
          return (
            <div
              className="md:flex-[0_0_30%] relative flex-[0_0_70%]"
              key={certificate.id}
            >
              <Image
                src={`${process.env.API_BASE_URL}${certificate.image}`}
                alt={certificate.alt}
                width={400}
                height={200}
                className="rounded-2xl"
              />
            </div>
          );
        })}
      </Carousel>
      <FourCards title={aboutPage.factTitle} body={aboutPage.factDescription}>
        {facts.map((fact, i) => (
          <FactCard
            key={fact.id}
            title={fact.title}
            body={fact.description}
            imgSrc={fact.image}
            alt={fact.alt}
            type={i + 1}
          />
        ))}
      </FourCards>
    </main>
  );
};

export default AboutPage;
