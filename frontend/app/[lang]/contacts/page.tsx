import FormCard from "@/app/components/common/FormCard";
import { Metadata } from "next";
import React from "react";
import { getDictionary } from "../dictionaries";
import { ContactsPageData, ServicesPageData } from "@/types";

async function getContactsPageData(lang: string): Promise<ContactsPageData> {
  const res = await fetch(`${process.env.API_URL}/page/master?lang=${lang}`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch contacts page data");
  }

  return res.json();
}

export async function generateMetadata({
  params: { lang },
}: {
  params: { lang: string };
}): Promise<Metadata> {
  const {
    meta_title: metaTitle,
    meta_description: metaDescription,
    meta_keywords: metaKeywords,
  } = await getContactsPageData(lang);

  const title = metaTitle;
  const description = metaDescription;
  const keywords = metaKeywords;

  return {
    title,
    description,
    keywords,
  };
}

async function getServices(lang: string): Promise<ServicesPageData> {
  const res = await fetch(`${process.env.API_URL}/page/service?lang=${lang}`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch services page data");
  }

  return res.json();
}

const ContactsPage = async ({
  params: { lang },
}: {
  params: { lang: string };
}) => {
  const { services } = await getServices(lang);
  const { buttons, inputs, mainPage, markTitle, reviewsTitle, mastersTitle } =
    await getDictionary(lang);

  return (
    <main className="flex justify-center">
      <FormCard
        title={mainPage.form.title}
        body={mainPage.form.description}
        selectOptions={services}
        buttonTitle={buttons.buttonTitle1}
        inputsText={inputs}
        style={{ borderRadius: "15px", padding: "15px", marginTop: "15px" }}
      />
    </main>
  );
};

export default ContactsPage;
