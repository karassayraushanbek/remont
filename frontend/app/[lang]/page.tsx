import Carousel from "@/app/components/common/Carousel";
import FormCard from "@/app/components/common/FormCard";
import FourCard from "@/app/components/common/FourCard";
import FourCards from "@/app/components/common/FourCards";
import FourCards2 from "@/app/components/common/FourCards2";
import Hero from "@/app/components/common/Hero";
import MasterCard from "@/app/components/common/MasterCard";
import { MainPageData } from "@/types";
import { Metadata } from "next";
import Image from "next/image";
import ServiceCard from "../components/common/ServiceCard";
import { getDictionary } from "./dictionaries";
import ReviewCard from "@/app/components/common/ReviewCard";
import Grid from "../components/common/Grid";
import { getCall } from "../api/getCall";
import { getPhoneNumbers } from "../api/getPhoneNumbers";

async function getMainPageData(lang: string): Promise<MainPageData> {
  const res = await fetch(`${process.env.API_URL}/page/main?lang=${lang}`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch main page data");
  }

  return res.json();
}

export async function generateMetadata({
  params: { lang },
}: {
  params: { lang: string };
}): Promise<Metadata> {
  const {
    meta_title: metaTitle,
    meta_description: metaDescription,
    meta_keywords: metaKeywords,
  } = await getMainPageData(lang);

  const title = metaTitle;
  const description = metaDescription;
  const keywords = metaKeywords;

  return {
    title,
    description,
    keywords,
  };
}

export default async function MainPage({
  params: { lang },
}: {
  params: { lang: string };
}) {
  const {
    banner,
    why_we: whyWeS,
    marks,
    reviews,
    services,
    masters,
    meta_title: metaTitle,
    meta_description: metaDescription,
  } = await getMainPageData(lang);
  // const { call } = await getCall();
  const { data: phoneNumbers } = await getPhoneNumbers();
  const { buttons, inputs, mainPage, markTitle, reviewsTitle, mastersTitle } =
    await getDictionary(lang);

  return (
    <main className="md:space-y-20 space-y-14">
      {mainPage && (
        <>
          <Hero imgSrc={banner.image}>
            <FormCard
              title={metaTitle}
              body={metaDescription}
              selectOptions={services}
              buttonTitle={buttons.buttonTitle1}
              inputsText={inputs}
            />
          </Hero>
          <FourCards
            title={mainPage.whyWeTitle}
            style={{ marginTop: "var(--custom-margin-top-main-page)" }}
          >
            {whyWeS.map((whyWe) => (
              <FourCard
                key={whyWe.id}
                title={whyWe.title}
                body={whyWe.description}
                imgSrc={whyWe.image}
                type={999}
                className="justify-between"
                buttons={buttons}
                // call={phoneNumbers[0].phone}
                call="+77071948259"
              />
            ))}
          </FourCards>
        </>
      )}

      <Carousel title={markTitle} type="brand">
        {marks.map((mark) => {
          return (
            <div
              className="md:flex-[0_0_20%] relative flex-[0_0_50%]"
              key={mark.id}
            >
              {mark.image && (
                <Image
                  src={`${process.env.API_BASE_URL}${mark.image}`}
                  alt={mark.alt}
                  width={300}
                  height={300}
                  className="rounded-3xl"
                />
              )}
            </div>
          );
        })}
      </Carousel>
      <Carousel title={reviewsTitle} type="">
        {reviews.slice(0, 8).map((review) => (
          <ReviewCard
            key={review.id}
            rating={5}
            body={review.description}
            name={review.name}
            date={review.date}
          />
        ))}
      </Carousel>
      {mainPage && (
        <Grid title={mainPage.serviceTitle}>
          {services.map((service, i) => (
            <ServiceCard
              key={service.id}
              title={service.title}
              body={service.description}
              type={i + 1}
              buttons={buttons}
              // call={phoneNumbers[0].phone}
              call="+77071948259"
              typePage={1}
            />
          ))}
        </Grid>
      )}
      <div className="mt-40">
        <Carousel title={mastersTitle} type="">
          {masters.map((master) => (
            <MasterCard
              key={master.id}
              tags={master.tags}
              name={master.name}
              location={master.distance}
              experience={master.experience}
              imgSrc={master.image}
              phoneNumbers={phoneNumbers}
              buttons={buttons}
            />
          ))}
        </Carousel>
      </div>
    </main>
  );
}
