import KeepWithUs from "@/app/components/blog/KeepWithUs";
import ShareWithFriends from "@/app/components/blog/ShareWithFriends";
import SimilarArticles from "@/app/components/blog/SimilarArticles";
import Carousel from "@/app/components/common/Carousel";
import ReviewCard from "@/app/components/common/ReviewCard";
import { ArticlePageData } from "@/types";
import { Metadata } from "next";
import Image from "next/image";
import Link from "next/link";

async function getArticlePageData(
  id: number,
  lang: string
): Promise<ArticlePageData> {
  const res = await fetch(
    `${process.env.API_URL}/page/blog/${id}?lang=${lang}`,
    { cache: "no-cache" }
  );

  if (!res.ok) {
    throw new Error("Failed to fetch main page data");
  }

  return res.json();
}

export async function generateMetadata({
  params: { id, lang },
}: {
  params: { id: number; lang: string };
}): Promise<Metadata> {
  const {
    meta_title: metaTitle,
    meta_description: metaDescription,
    meta_keywords: metaKeywords,
  } = await getArticlePageData(id, lang);

  const title = metaTitle;
  const description = metaDescription;
  const keywords = metaKeywords;

  return {
    title,
    description,
    keywords,
  };
}

const AriclePage = async ({
  params: { id, lang },
}: {
  params: { id: string; lang: string };
}) => {
  const { tag, title, date, image, description, reviews, similar } =
    await getArticlePageData(parseInt(id), lang);

  return (
    <main className="space-y-10">
      <div className="md:flex md:items-center space-x-2">
        <div className="flex items-center space-x-2">
          <Link href="/blog">{tag}</Link>
          <Image
            src="/icons/arrow-right-icon.svg"
            alt="arrow right icon"
            width={10}
            height={10}
          />
        </div>
        <Link href={`/blog/${id}`}>{title}</Link>
      </div>
      <div className="flex">
        <div className="space-y-5">
          <div className="space-y-3">
            <h1 className="font-bold">{title}</h1>
            <h2 className="text-[#6E6E6E]">{date}</h2>
          </div>
          <Image
            src={`${process.env.API_BASE_URL}${image}`}
            alt={image}
            width={300}
            height={300}
            className="rounded-2xl hidden md:block"
          />
          <Image
            src={`${process.env.API_BASE_URL}${image}`}
            alt={image}
            width={400}
            height={300}
            className="rounded-2xl md:hidden"
          />
          <div className="space-y-5">
            <h2 className="font-bold text-xl">{title}</h2>
            <div dangerouslySetInnerHTML={{ __html: description }}></div>
          </div>
        </div>
        <div className="md:block space-y-5 hidden">
          <SimilarArticles similar={similar} />
          <ShareWithFriends />
        </div>
      </div>
      <div className="space-y-7">
        <h1 className="font-bold">Комментарии</h1>
        <Carousel type="">
          {reviews.map((review) => (
            <ReviewCard
              key={review.id}
              rating={5}
              body={review.description}
              name={review.name}
              date={review.date}
            />
          ))}
        </Carousel>
      </div>
      {typeof window !== "undefined" && window.innerWidth < 768 && (
        <SimilarArticles similar={similar} />
      )}
      {typeof window !== "undefined" && window.innerWidth < 768 && (
        <KeepWithUs />
      )}
    </main>
  );
};

export default AriclePage;
