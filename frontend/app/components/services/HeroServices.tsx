import Image from "next/image";

type HeroServicesProps = {
  imgSrc: string;
  title: string;
  description: string;
};

const HeroServices = ({ imgSrc, title, description }: HeroServicesProps) => {
  return (
    <div className="md:h-screen md:space-y-0 mt-10 flex flex-col items-center relative space-y-10">
      <Image
        src={`${process.env.API_BASE_URL}${imgSrc}`}
        alt="hero about bg"
        width={1920}
        height={1080}
        className="md:rounded-3xl rounded-xl"
      />
      <div className="md:py-8 md:h-auto md:top-1/4 md:right-[62%] md:absolute text-white bg-[#FFB627] p-4 rounded-3xl max-w-2xl h-[18rem]">
        <div className="md:space-y-7 space-y-4">
          <h2 className="md:text-3xl font-bold">{title}</h2>
          <h5 className="md:text-lg">{description}</h5>
        </div>
      </div>
    </div>
  );
};

export default HeroServices;
