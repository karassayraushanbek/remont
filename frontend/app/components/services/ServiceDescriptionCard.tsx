type ServiceDescriptionCardProps = {
  title: string;
  price: string;
  call: string;
  buttons: {
    buttonTitle1: string;
    buttonTitle2: string;
  };
};

const ServiceDescriptionCard = ({
  title,
  price,
  call,
  buttons,
}: ServiceDescriptionCardProps) => {
  return (
    <div className="bg-white rounded-xl flex flex-col justify-between space-y-4 px-4 py-6 max-w-xs">
      <div className="md:space-y-0 flex flex-col justify-between h-[80%] space-y-6">
        <h3 className="md:text-lg font-bold w-[70%]">{title}</h3>
        <h3 className="text-[#6E6E6E]">{price}</h3>
      </div>
      <a
        className="underline text-yellow-500 block cursor-pointer"
        href={`https://wa.me/${call}`}
      >
        {buttons.buttonTitle2}
      </a>
      <a
        className="button w-[70%] text-center cursor-pointer py-2 block"
        href={`tel:${call}`}
      >
        {buttons.buttonTitle1}
      </a>
    </div>
  );
};

export default ServiceDescriptionCard;
