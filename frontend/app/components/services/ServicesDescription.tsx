"use client";

import { useState } from "react";
import Carousel from "../common/Carousel";
import ServiceDescriptionCard from "./ServiceDescriptionCard";
import { DescriptionService, Tag } from "@/types";

type ServicesDescriptionProps = {
  title: string;
  tags: Tag[];
  descriptionServices: DescriptionService[];
  buttons: {
    buttonTitle1: string;
    buttonTitle2: string;
  };
  call: string;
};

const ServicesDescription = ({
  title,
  tags,
  descriptionServices,
  buttons,
  call,
}: ServicesDescriptionProps) => {
  const [serviceView, setServiceView] = useState(tags[0].id);

  const changeServiceView = (type: number) => {
    setServiceView(type);
  };

  const renderServiceView = (tagId: number) => {
    return (
      <div className="md:grid-cols-4 grid grid-rows-2 grid-cols-2 gap-4">
        {descriptionServices
          .filter((service) => service.tag_id === tagId)
          .map((service) => (
            <ServiceDescriptionCard
              key={service.id}
              title={service.title}
              price={service.description}
              buttons={buttons}
              call={call}
            />
          ))}
      </div>
    );
  };

  const renderButtonView = () => {
    return (
      <div className="flex space-x-14">
        {Object.values(tags).map((tag) => (
          <button
            key={tag.id}
            onClick={() => changeServiceView(tag.id)}
            className={`${
              serviceView === tag.id ? "button" : "text-[#6E6E6E]"
            } py-2 px-5`}
          >
            {tag.title}
          </button>
        ))}
      </div>
    );
  };

  const renderMobileButtonView = () => {
    return (
      <Carousel type="button">
        {Object.values(tags).map((tag) => (
          <button
            key={tag.id}
            onClick={() => changeServiceView(tag.id)}
            className={`${
              serviceView === tag.id ? "button" : "text-[#6E6E6E]"
            } md:text-lg md:px-5 md:flex-[0_0_30%] flex-[0_0_50%] py-2 px-5 text-left`}
          >
            {tag.title}
          </button>
        ))}
      </Carousel>
    );
  };

  return (
    <div className="space-y-10 flex flex-col md:items-center">
      <h2 className="md:text-center font-bold">{title}</h2>
      {typeof window !== undefined && window?.innerWidth < 768
        ? renderMobileButtonView()
        : renderButtonView()}
      {renderServiceView(serviceView)}
    </div>
  );
};

export default ServicesDescription;
