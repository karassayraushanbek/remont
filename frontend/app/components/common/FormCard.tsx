import { Service } from "@/types";
import Form from "./Form";

type FormCardProps = {
  title: string;
  body: string;
  selectOptions: Service[];
  buttonTitle: string;
  inputsText: {
    inputTitle1: string;
    inputTitle2: string;
  };
  style?: {};
};

const FormCard = ({
  title,
  body,
  selectOptions,
  buttonTitle,
  inputsText,
  style,
}: FormCardProps) => {
  const convertPhoneNumbers = (text: string) => {
    return text.replace(
      /(\+\d{6,15})/g,
      '<a href="tel:$1" style="text-decoration: underline; color: blue;">$1</a>'
    );
  };

  return (
    <div
      className="md:px-5 md:space-y-6 md:py-2 max-w-lg bg-white space-y-4"
      style={style}
    >
      <h2 className="md:font-bold md:max-w-md md:text-[32px] md:leading-10 font-semibold">
        {title}
      </h2>
      <h4
        className="md:text-base text-sm"
        dangerouslySetInnerHTML={{ __html: convertPhoneNumbers(body) }}
      ></h4>
      <Form
        selectOptions={selectOptions.map((selectOption) => selectOption.title)}
        buttonTitle={buttonTitle}
        inputs={inputsText}
      />
    </div>
  );
};

export default FormCard;
