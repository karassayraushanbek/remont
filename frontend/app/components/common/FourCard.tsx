import Image from "next/image";

type FourCardProps = {
  title: string;
  body: string;
  imgSrc: string;
  type?: number;
  className?: string;
  buttons?: {
    buttonTitle1: string;
    buttonTitle2: string;
  };
  call?: string;
};

const FourCard = ({
  title,
  body,
  imgSrc,
  type,
  className,
  buttons,
  call,
}: FourCardProps) => {
  return (
    <div
      className={`${className} md:pb-16 md:px-6 px-2 pt-10 pb-8 bg-white rounded-2xl max-w-[18rem] flex flex-col space-y-5 test`}
    >
      <div className="md:space-x-5 space-x-1 flex">
        {imgSrc && (
          <Image
            src={`${process.env.API_BASE_URL}${imgSrc}`}
            alt={imgSrc}
            width={30}
            height={30}
          />
        )}
        <h2 className={`md:text-2xl md:max-w-xs md:font-bold font-medium text-base`}>
          {title}
        </h2>
      </div>
      <h3 className="md:text-sm md:max-w-[15rem] text-xs max-w-[9rem] text-[#6E6E6E]">
        {body}
      </h3>
      {type === 999 && (
        <div className="md:space-y-5 space-y-2 text-center">
          <a className="button w-full py-2 block" href={`tel:${call}`}>
            {buttons?.buttonTitle1}
          </a>
          <a
            className="underline text-yellow-500 block"
            href={`https://wa.me/${call}`}
          >
            {buttons?.buttonTitle2}
          </a>
        </div>
      )}
    </div>
  );
};

export default FourCard;
