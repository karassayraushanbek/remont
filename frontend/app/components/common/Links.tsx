"use client";

import { LinkT } from "@/types";
import Link from "next/link";
import { usePathname } from "next/navigation";

const Links = ({ links }: { links: LinkT[] }) => {
  const pathname = usePathname();

  const modifiedPathname = pathname.slice(0, 1) + pathname.slice(4);

  return (
    <ul className="md:flex hidden space-x-10">
      {links.map((link) => {
        const isActive = modifiedPathname === link.slug;

        return (
          <li key={link.id}>
            <Link href={link.slug} className={`font-normal text-xl ${isActive ? "active-page" : ""}`}>
              {link.title}
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

export default Links;
