"use client";

import { useState } from "react";
import { handleFormSubmit } from "../../api/handleFormSubmit";

type FormProps = {
  selectOptions: string[];
  buttonTitle: string;
  inputs: {
    inputTitle1: string;
    inputTitle2: string;
  };
};

const Form = ({ selectOptions, buttonTitle, inputs }: FormProps) => {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [phone, setPhone] = useState("");
  const [name, setName] = useState("");
  const [selectedOption, setSelectedOption] = useState("");

  const handlePhoneChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPhone(event.target.value);
  };

  const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedOption(event.target.value);
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!phone || !name || !selectedOption) {
      alert("Заполните все поля!");
      return;
    }

    const formData = new FormData(event.currentTarget);

    try {
      await handleFormSubmit(formData);
      setIsSubmitted(true);
    } catch (error) {
      console.error("Form submission failed:", error);
    }
  };

  return (
    <>
      {!isSubmitted ? (
        <form
          onSubmit={handleSubmit}
          className="md:gap-5 grid grid-cols-2 grid-rows-3 gap-2"
        >
          <select
            name=""
            id=""
            className="md:py-3 md:px-4 md:text-base text-xs px-2 rounded-lg bg-white border-yellow-500 border-2 cursor-pointer"
            defaultValue=""
            onChange={handleSelectChange}
          >
            <option value="defaultValue" disabled>
              Выберите технику
            </option>
            {selectOptions.map((selectOption, i) => {
              return <option key={i}>{selectOption}</option>;
            })}
          </select>
          <input
            name="phone"
            type="text"
            value={phone}
            onChange={handlePhoneChange}
            placeholder={inputs.inputTitle1}
            className='md:indent-10 indent-5 md:bg-[15px] bg-[10px] bg-no-repeat bg-[url("/icons/phone-icon.svg")] md:bg-[length:20px_20px] bg-[length:15px_15px]'
          />
          <input
            name="name"
            type="text"
            value={name}
            onChange={handleNameChange}
            placeholder={inputs.inputTitle2}
            className='rounded-lg col-span-2 md:indent-10 indent-8 bg-[15px] bg-no-repeat bg-[url("/icons/people-icon.svg")] md:bg-[length:20px_20px] bg-[length:15px_15px]'
          />
          <button className="button col-span-2 py-2 md:py-0" type="submit">
            {buttonTitle}
          </button>
        </form>
      ) : (
        <h1 className="font-bold">
          Спасибо за заявку, перезвоним через 10 секунд!
        </h1>
      )}
    </>
  );
};

export default Form;
