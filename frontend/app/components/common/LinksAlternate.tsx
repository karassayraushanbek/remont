"use client";

import { LinkT } from "@/types";
import Link from "next/link";
import { usePathname } from "next/navigation";

const LinksAlternate = ({
  links,
  onClose,
}: {
  links: LinkT[];
  onClose: () => void;
}) => {
  const pathname = usePathname();

  const modifiedPathname = pathname.slice(0, 1) + pathname.slice(4);

  const handleLinkClick = () => {
    onClose();
  };

  return (
    <ul className="text-center text-white font-bold space-y-5 text-2xl">
      {links.map((link) => {
        const isActive = modifiedPathname === link.slug;

        return (
          <li key={link.id}>
            <Link
              href={link.slug}
              className={isActive ? "active-page-2" : ""}
              onClick={handleLinkClick}
            >
              {link.title}
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

export default LinksAlternate;
