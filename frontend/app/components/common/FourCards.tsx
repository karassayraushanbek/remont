type FourCardsProps = {
  title: string;
  body?: string;
  children: React.ReactNode;
  style?: Object;
  className?: string;
};

const FourCards = ({
  title,
  body,
  children,
  style,
  className,
}: FourCardsProps) => {
  return (
    <div
      className={`md:items-center md:space-y-14 flex flex-col space-y-10 ${className}`}
      style={style}
    >
      <div className="md:text-center space-y-3">
        <h2 className="md:max-w-full md:text-center max-w-[15rem] md:font-bold md:text-3xl font-semibold">
          {title}
        </h2>
        {body && <h3 className="text-[#6E6E6E] text-base">{body}</h3>}
      </div>
      <div className="md:flex md:space-x-20 grid grid-rows-2 grid-cols-2 gap-4">
        {children}
      </div>
    </div>
  );
};

export default FourCards;
