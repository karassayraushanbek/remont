type Props = {
  title: string;
  children: React.ReactNode;
  style?: Object;
};

const Grid = ({ title, children, style }: Props) => {
  return (
    <div className="md:items-center flex flex-col space-y-10" style={style}>
      <h2 className="md:max-w-full md:text-center max-w-[15rem] font-bold md:text-3xl">{title}</h2>
      <div className="md:grid-cols-3 md:grid-rows-1 grid grid-rows-2 grid-cols-2 md:gap-4 gap-3">
        {children}
      </div>
    </div>
  );
};

export default Grid;
