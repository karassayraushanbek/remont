import Image from "next/image";

type ReviewCardProps = {
  rating: number;
  body: string;
  name: string;
  date: string;
};

const ReviewCard = ({ rating, body, name, date }: ReviewCardProps) => {
  const renderStars = (rating: number) => {
    const stars = [];
    for (let i = 0; i < 5; i++) {
      if (i < rating) {
        stars.push(
          <Image
            key={i}
            src="/icons/star-filled.svg"
            alt="star"
            width={20}
            height={20}
          />
        );
      } else {
        stars.push(
          <Image
            key={i}
            src="/icons/star-empty.svg"
            alt="star"
            width={20}
            height={20}
          />
        );
      }
    }
    return stars;
  };

  return (
    <div className="md:space-y-7 md:px-5 space-y-5 md:flex-[0_0_20%] flex-[0_0_70%] py-8 px-4 flex flex-col justify-between bg-white rounded-2xl">
      <div className="flex flex-col justify-between space-y-3">
        <div className="md:flex md:justify-between md:items-center space-y-3">
          <div className="md:space-x-3 space-x-2 flex items-center">
            {renderStars(rating)}
          </div>
          <h3 className="text-[#6E6E6E]">{date}</h3>
        </div>
        <p className="max-w-[16rem] md:leading-5 text-base h-48 overflow-auto">{body}</p>
      </div>
      <div className="flex justify-between items-center">
        <div className="flex space-x-3">
          <Image
            src="/icons/people-icon-2-black.svg"
            alt="people icon 2"
            width={15}
            height={15}
          />
          <h3 className="text-[#6E6E6E] uppercase">{name}</h3>
        </div>
        <Image
          src="/2gis_logo.svg"
          alt="2gis logo"
          width={100}
          height={100}
          className="md:block hidden"
        />
        <Image
          src="/2gis_logo.svg"
          alt="2gis logo"
          width={50}
          height={50}
          className="md:hidden"
        />
      </div>
    </div>
  );
};

export default ReviewCard;
