import { getDictionary } from "@/app/[lang]/dictionaries";
import { getEmail } from "@/app/api/getEmail";
import { getPhoneNumbers } from "@/app/api/getPhoneNumbers";
import Image from "next/image";

const Footer = async ({ lang }: { lang: string }) => {
  const { data: phoneNumbers } = await getPhoneNumbers();
  const { data: emails } = await getEmail();

  const { contactTitle, footerTitle } = await getDictionary(lang);
  return (
    <footer className="md:space-y-10 space-y-5 mt-10 relative">
      <h2 className="md:text-center font-bold md:max-w-full md:text-3xl">
        {contactTitle}
      </h2>
      <Image
        src="/bg/footer-bg.svg"
        alt="footer bg"
        width={1920}
        height={1080}
        className="rounded-[2rem]"
      />
      <div className="md:block md:px-6 md:py-12 md:top-1/4 md:right-[65%] md:absolute max-w-xl h-[18rem] hidden">
        <div className="md:space-y-20 space-y-4">
          <h1 className="md:text-3xl font-bold">{footerTitle}</h1>
          <div className="space-y-5">
            <div className="flex space-x-5">
              <Image
                src="/icons/phone-icon.svg"
                alt="phone icon"
                width={40}
                height={40}
              />
              <div className="space-y-5">
                {/* {phoneNumbers.map(
                  (phoneNumberObj: { id: number; phone: string }) => (
                    <a
                      key={phoneNumberObj.id.toString()}
                      className="font-bold block text-2xl cursor-pointer"
                      href={`tel:${phoneNumberObj.phone}`}
                    >
                      {phoneNumberObj.phone}
                    </a>
                  )
                )} */}
                <a
                  className="font-bold block text-2xl cursor-pointer"
                  href={`tel:+77071948259`}
                >
                  +77071948259
                </a>
              </div>
            </div>
            <div className="flex space-x-5">
              <Image
                src="/icons/mail-icon.svg"
                alt="mail icon"
                width={40}
                height={40}
              />
              {emails.map((emailObj: { id: number; email: string }) => (
                <a
                  key={emailObj.id.toString()}
                  className="font-bold block text-2xl cursor-pointer"
                  href={`mailto:${emailObj.email}`}
                >
                  {emailObj.email}
                </a>
              ))}
            </div>
          </div>
        </div>
      </div>
      <div className="px-6 absolute top-[35%] md:hidden">
        <div className="md:space-y-20 space-y-4">
          <div className="space-y-5">
            <div className="flex space-x-5">
              <Image
                src="/icons/phone-icon.svg"
                alt="phone icon"
                width={20}
                height={20}
              />
              <div className="space-y-5">
                {/* {phoneNumbers.map(
                  (phoneNumberObj: { id: number; phone: string }) => (
                    <a
                      key={phoneNumberObj.id}
                      className="font-bold cursor-pointer"
                      href={`tel:${phoneNumberObj.phone}`}
                    >
                      {phoneNumberObj.phone}
                    </a>
                  )
                )} */}
                <a
                  className="font-bold cursor-pointer"
                  href={`tel:+77071948259`}
                >
                  +77071948259
                </a>
              </div>
            </div>
            <div className="flex space-x-5">
              <Image
                src="/icons/mail-icon.svg"
                alt="mail icon"
                width={20}
                height={20}
              />
              {emails.map((emailObj: { id: number; email: string }) => (
                <a
                  key={emailObj.id.toString()}
                  className="font-bold cursor-pointer"
                  href={`mailto:${emailObj.email}`}
                >
                  {emailObj.email}
                </a>
              ))}
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
