import Image from "next/image";

type HeroProps = {
  imgSrc: string;
  children: React.ReactNode;
};

const Hero = ({ imgSrc, children }: HeroProps) => {
  return (
    <div className="md:h-[90vh] h-[20vh] mt-5 flex flex-col items-center relative">
      {imgSrc && (
        <Image
          src={`${process.env.API_BASE_URL}${imgSrc}`}
          alt={imgSrc}
          width={1920}
          height={1080}
          className="md:rounded-3xl rounded-xl"
        />
      )}
      <div className="lg:top-1/4 md:top-2/3 md:shadow-lg top-[80%] rounded-xl absolute flex flex-col items-center bg-white p-8">
        {children}
      </div>
    </div>
  );
};

export default Hero;
