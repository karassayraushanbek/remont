import Image from "next/image";
import HamburgerMenu from "./HamburgerMenu";
import Links from "./Links";
import { getDictionary } from "@/app/[lang]/dictionaries";
import { getPhoneNumbers } from "@/app/api/getPhoneNumbers";
import { getCall } from "@/app/api/getCall";
import Link from "next/link";

async function getNavbarData(lang: string) {
  const res = await fetch(`${process.env.API_URL}/menus?lang=${lang}`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch navbar data");
  }

  return res.json();
}

async function getLogo() {
  try {
    const res = await fetch(`${process.env.API_URL}/logo`, {
      cache: "no-cache",
    });

    if (!res.ok) {
      throw new Error("Failed to fetch logo");
    }

    return res.json();
  } catch (err) {
    console.error(err);
  }
}

const Navbar = async ({ lang }: { lang: string }) => {
  const logoData = await getLogo();
  const logo = logoData?.logo;
  const { data: links } = await getNavbarData(lang);
  const { data: phoneNumbers } = await getPhoneNumbers();
  // const { call } = await getCall();
  const { buttons } = await getDictionary(lang);

  return (
    <nav className="md:bg-[#F3F3F3] md:p-0 md:pb-5 md:space-y-5 bg-white md:rounded-[0] rounded-2xl py-5 px-6 flex justify-between items-center sticky top-0 z-50">
      <HamburgerMenu links={links} />
      <a className="md:hidden text-xl" href={`tel:+77071948259}`}>
        {+77071948259}
      </a>
      {logo && (
        <Link href="/">
          {/* <Image src={`${logo}`} alt="logo" width={48} height={48} /> */}
          <Image src="/logo.png" alt="logo" width={48} height={48} className="rounded-lg"/>
        </Link>
      )}
      <Links links={links} />
      <div className="flex">
        {buttons && (
          <>
            <a
              className="md:block text-yellow-500 underline px-7 py-2 hidden"
              href={`https://wa.me/+77071948259`}
            >
              {buttons.buttonTitle2}
            </a>
            <a
              className="md:block button px-7 py-2 hidden font-medium text-xl"
              href={`tel:+77071948259`}
            >
              {buttons.buttonTitle1}
            </a>
          </>
        )}
      </div>
    </nav>
  );
};

export default Navbar;
