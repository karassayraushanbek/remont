import React from "react";

type FourCards3Props = {
  children: React.ReactNode;
};

const FourCards3 = ({ children }: FourCards3Props) => {
  return (
    <div className="md:block hidden">
      <div className="space-y-10">
        {children}
      </div>
    </div>
  );
};

export default FourCards3;
