type ServiceCardProps = {
  title: string;
  body: string;
  type: number;
  buttons: {
    buttonTitle1: string;
    buttonTitle2: string;
  };
  call: string;
  typePage?: number;
};

const ServiceCard = ({
  title,
  body,
  type,
  buttons,
  call,
  typePage,
}: ServiceCardProps) => {
  return (
    <div className="md:space-y-6 flex flex-col justify-between md:py-10 md:px-7 space-y-4 py-7 px-4 bg-white rounded-xl max-w-[24rem]">
      <div className="space-y-10">
        <div className="md:max-h-24 flex space-x-1 max-h-12">
          <h2 className="md:text-2xl md:w-4/5 text-base md:font-bold font-medium h-16">
          {type}. {title}
          </h2>
        </div>
        <h3 className="md:text-base md:font-normal md:leading-5 text-[#6E6E6E] overflow-hidden">
          {body}
        </h3>
      </div>
      {typePage !== 1 ? (
        <div className="md:space-y-5 space-y-2 text-center">
          <a className="button w-full py-2 block" href={`tel:${call}`}>
            {buttons.buttonTitle1}
          </a>
          <a
            className="underline text-yellow-500 block"
            href={`https://wa.me/${call}`}
          >
            {buttons.buttonTitle2}
          </a>
        </div>
      ) : (
        type === 1 && (
          <div className="md:space-y-5 space-y-2 text-center">
            <a className="button w-full py-2 block" href={`tel:${call}`}>
              {buttons.buttonTitle1}
            </a>
            <a
              className="underline text-yellow-500 block"
              href={`https://wa.me/${call}`}
            >
              {buttons.buttonTitle2}
            </a>
          </div>
        )
      )}
    </div>
  );
};

export default ServiceCard;
