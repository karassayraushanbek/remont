import { getDictionary } from "@/app/[lang]/dictionaries";
import { Tag } from "@/types";
import Image from "next/image";

type MasterCardProps = {
  name: string;
  tags: Tag[];
  location: string;
  experience: string;
  imgSrc: string;
  buttons: {
    buttonTitle1: string;
    buttonTitle2: string;
  };
  phoneNumbers: {
    phone: string;
  }[];
};

const MasterCard = ({
  name,
  tags,
  location,
  experience,
  imgSrc,
  buttons,
  phoneNumbers,
}: MasterCardProps) => {
  return (
    <div className="md:flex-[0_0_20%] flex-[0_0_80%] bg-white rounded-2xl py-6 space-y-5">
      <div className="flex items-center space-x-7 px-3">
        {imgSrc && (
          <Image
            src={`${process.env.API_BASE_URL}${imgSrc}`}
            alt={imgSrc}
            width={60}
            height={20}
            className="rounded-xl"
          />
        )}
        <div className="md:space-y-2 space-y-3">
          <h2 className="font-medium overflow-hidden md:max-w-full max-w-[6rem]">{name}</h2>
          <div className="flex items-center md:space-x-3 space-x-2">
            <Image
              src="/icons/marker-icon.svg"
              alt="marker icon"
              width={12}
              height={12}
            />
            <h3 className="text-[#6E6E6E]">{location}</h3>
          </div>
          <div className="flex items-center space-x-2">
            <Image
              src="/icons/work-icon.svg"
              alt="marker icon"
              width={15}
              height={15}
            />
            <h3 className="text-[#6E6E6E] overflow-hidden">Опыт работы: {experience}</h3>
          </div>
        </div>
      </div>
      <hr className="border-black" />
      <div className="px-7 flex flex-col justify-between">
        <div className="h-48 overflow-auto">
          {tags.map((tag, i) => (
            <p key={i}>{tag.title}</p>
          ))}
        </div>
        <div className="space-y-3">
          <a
            className="block text-yellow-500 underline"
            href={`https://wa.me/${phoneNumbers[0].phone}`}
          >
            {buttons.buttonTitle2}
          </a>
          <a
            className="md:px-0 block button py-2 text-center"
            href={`tel:${phoneNumbers[0].phone}`}
          >
            {buttons.buttonTitle1}
          </a>
        </div>
      </div>
    </div>
  );
};

export default MasterCard;
