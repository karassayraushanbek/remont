import Image from "next/image";
import React from "react";

type FourCard3Props = {
  title: string;
  body: string;
  imgSrc: string;
  type: number;
};

const FourCard3 = ({ title, body, imgSrc, type }: FourCard3Props) => {
  return (
    <div className="flex flex-col items-center space-y-10">
      <h1 className="text-center font-bold">
        {title}
      </h1>
      <div className="flex space-x-24 items-center">
        {type % 2 === 0 ? (
          <>
            <h2 className="bg-white max-w-sm text-base rounded-2xl px-5 py-10">
              {body}
            </h2>
            <Image src={imgSrc} alt={imgSrc} width={800} height={800} className="rounded-3xl"/>
          </>
        ) : (
          <>
            <Image src={imgSrc} alt={imgSrc} width={800} height={800} className="rounded-3xl"/>
            <h2 className="bg-white max-w-sm text-base rounded-2xl px-5 py-10">
              {body}
            </h2>
          </>
        )}
      </div>
    </div>
  );
};

export default FourCard3;
