import Image from "next/image";

type HeroBlogProps = {
  imgSrc: string;
  title: string;
};

const HeroBlog = ({ imgSrc, title }: HeroBlogProps) => {
  return (
    <div className="mt-10 md:h-screen md:space-y-0 space-y-14 flex flex-col items-center relative">
      {imgSrc && (
        <Image
          src={`${process.env.API_BASE_URL}${imgSrc}`}
          alt="hero blog bg"
          fill={true}
          className="md:rounded-3xl rounded-xl object-cover"
        />
      )}
      <div className="md:top-2/3 md:absolute text-white flex flex-col items-center bg-[#FFB627] px-6 py-9 rounded-3xl max-w-xl">
        <h1 className="font-bold text-3xl max-w-[30rem]">{title}</h1>
      </div>
    </div>
  );
};

export default HeroBlog;
