import Image from "next/image";

type FactCardProps = {
  title: string;
  body: string;
  imgSrc: string;
  alt: string;
  type?: number;
};

const FactCard = ({ title, body, imgSrc, alt, type }: FactCardProps) => {
  return (
    <div className="px-10 py-4 bg-white rounded-xl flex flex-col justify-between items-center space-y-4">
      <Image
        src={`${process.env.API_BASE_URL}${imgSrc}`}
        alt={alt}
        width={100}
        height={100}
        className=""
      />
      <div className="flex-grow md:hidden space-y-1">
        <h2
          className={`max-w-[15rem] md:text-lg text-center font-bold text-xl ${
            type === 1 && "pt-[0.6rem]"
          } `}
        >
          {title}
        </h2>
        <h2 className="max-w-[10rem] text-[#6E6E6E] text-center">{body}</h2>
      </div>
      <div className="hidden md:block">
        <h2 className="max-w-[15rem] md:text-lg text-center font-bold text-xl">
          {title}
        </h2>
        <h2 className="max-w-[10rem] text-[#6E6E6E] text-center">{body}</h2>
      </div>
    </div>
  );
};

export default FactCard;
