"use server";

export async function getPhoneNumbers() {
  const res = await fetch(`${process.env.API_URL}/phones`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch phone numbers");
  }

  return res.json();
}
