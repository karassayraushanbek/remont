const SITE_URL = "https://remontmasrter.kz";

export default function handler(req: any, res: any) {
  const robots = `User-agent: *
  Disallow: /admin/          # Запрещаем доступ к административной панели
  Disallow: /cgi-bin/        # Запрещаем доступ к cgi-bin
  Disallow: /images/         # Запрещаем доступ к изображениям
  Disallow: /scripts/        # Запрещаем доступ к скриптам
  Disallow: /styles/         # Запрещаем доступ к стилям
  Disallow: /temp/           # Запрещаем доступ к временным файлам
  
  # Разрешаем доступ к основному содержимому сайта
  Allow: /
  Allow: /blog/             # Разрешаем индексацию блога
  Allow: /about/            # Разрешаем индексацию страницы "О нас"
  Allow: /services/         # Разрешаем индексацию страницы с услугами
  Allow: /services/description/  # Разрешаем индексацию страниц с описанием услуг
  Allow: /masters/          # Разрешаем индексацию страницы с информацией о мастерах
  Allow: /reviews/          # Разрешаем индексацию страницы с отзывами
  Allow: /contacts/         # Разрешаем индексацию страницы с контактами
  
  # Указываем местоположение файла карты сайта
  Sitemap: ${SITE_URL}`;
  res.send(robots);
}
