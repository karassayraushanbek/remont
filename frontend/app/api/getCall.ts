"use server";

export async function getCall() {
  const res = await fetch(`${process.env.API_URL}/call`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch email");
  }

  return res.json();
}
