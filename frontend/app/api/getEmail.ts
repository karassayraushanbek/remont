"use server";

export async function getEmail() {
  const res = await fetch(`${process.env.API_URL}/emails`, {
    cache: "no-cache",
  });

  if (!res.ok) {
    throw new Error("Failed to fetch email");
  }

  return res.json();
}
