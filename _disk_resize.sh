#!/bin/bash
# Original post: https://networklessons.com/uncategorized/extend-lvm-partition

echo "Type follow: n, p, 3, w"
fdisk /dev/sda
fdisk -l
pvdisplay
pvcreate /dev/sda3
pvdisplay
vgdisplay | grep Name
vgextend vg-root /dev/sda3
vgdisplay
lvdisplay | grep Path
lvextend -l +100%FREE /dev/mapper/vg--root-lv--root
resize2fs /dev/mapper/vg--root-lv--root
df -lh
