## Первый запуск

```bash
cd /apps
git clone git@github.com:LuckIncome/remont.git ./remont && cd remont
./_init.sh
./_start.sh
./_migrate.sh
```

## Обновление (деплой)

```bash
cd /apps/remont
./_deploy.sh
```

## Админка

<table>
        <thead>
            <th>URL</th>
            <th>Логин</th>
            <th>Пароль</th>
            <th>Панель</th>
        </thead>
    <tbody>
        <tr> 
            <td> https://remont.its.bz/admin/login </td>
            <td> admin@admin.com </td>
            <td> 123admin </td>
            <td> Панель Администратора </td>
        </tr>
        <tr> 
            <td> https://rem,ont.its.bz/reader/login </td>
            <td> api@admin.com </td>
            <td> 123admin </td>
            <td> Панель API SWAGGER </td>
        </tr>
        <tr> 
            <td> http://remont.its.bz:9001 </td>
            <td> remont </td>
            <td> Heruvima80 (или взять из текущего `/backend/.env`) </td>
            <td> Панель Базы Данных(phpmyadmin) </td>
        </tr>
	<tr>
            <td> http://remont.its.bz:9000 </td>
            <td> admin </td>
            <td> Heruvima80$$ (или сбросить, удалив `/data/portainer` и перезапустить) </td>
            <td> Управление контейнерами (Portainer) </td>
        </tr>
    </tbody>
</table>
