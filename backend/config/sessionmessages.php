<?php

return [
    'successfully_added' => 'Успешно добавлен!',
    'successfully_updated' => 'Успешно обновлено!',
    'successfully_deleted' => 'Успешно удалена!',
    'successfully_registered' => 'Успешно зарегистрирован!',
    'successfully_logged_out' => 'Успешно вышел из системы!',
    'an_error_has_occurred' => 'Произошла ошибка!',
];
