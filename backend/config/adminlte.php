<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For detailed instructions you can look the title section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'title' => 'Remont',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For detailed instructions you can look the favicon section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Google Fonts
    |--------------------------------------------------------------------------
    |
    | Here you can allow or not the use of external google fonts. Disabling the
    | google fonts may be useful if your admin panel internet access is
    | restricted somehow.
    |
    | For detailed instructions you can look the google fonts section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'google_fonts' => [
        'allowed' => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For detailed instructions you can look the logo section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'logo' => '<b>R</b>emont',
    'logo_img' => 'vendor/adminlte/dist/img/AdminLTELogo.png',
    'logo_img_class' => 'brand-image img-circle elevation-3',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'Remont Logo',

    /*
    |--------------------------------------------------------------------------
    | Authentication Logo
    |--------------------------------------------------------------------------
    |
    | Here you can setup an alternative logo to use on your login and register
    | screens. When disabled, the admin panel logo will be used instead.
    |
    | For detailed instructions you can look the auth logo section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'auth_logo' => [
        'enabled' => false,
        'img' => [
            'path' => 'vendor/adminlte/dist/img/AdminLTELogo.png',
            'alt' => 'Auth Logo',
            'class' => '',
            'width' => 50,
            'height' => 50,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Preloader Animation
    |--------------------------------------------------------------------------
    |
    | Here you can change the preloader animation configuration.
    |
    | For detailed instructions you can look the preloader section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'preloader' => [
        'enabled' => true,
        'img' => [
            'path' => 'vendor/adminlte/dist/img/AdminLTELogo.png',
            'alt' => 'Remont Preloader Image',
            'effect' => 'animation__shake',
            'width' => 60,
            'height' => 60,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For detailed instructions you can look the user menu section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => false,
    'usermenu_header_class' => 'bg-primary',
    'usermenu_image' => false,
    'usermenu_desc' => false,
    'usermenu_profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For detailed instructions you can look the layout section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,
    'layout_dark_mode' => null,

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For detailed instructions you can look the auth classes section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For detailed instructions you can look the admin panel classes here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-dark-primary elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-dark',
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For detailed instructions you can look the sidebar section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'sidebar_mini' => 'lg',
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For detailed instructions you can look the right sidebar section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Layout-and-Styling-Configuration
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For detailed instructions you can look the urls section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Basic-Configuration
    |
    */

    'use_route_url' => false,
    'dashboard_url' => '/admin/home',
    'logout_url' => '/admin/logout',
    'login_url' => '/admin/login',
    'register_url' => '/admin/register',
    'password_reset_url' => 'password/reset',
    'password_email_url' => 'password/email',
    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For detailed instructions you can look the laravel mix section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Other-Configuration
    |
    */

    'enabled_laravel_mix' => false,
    'laravel_mix_css_path' => 'css/app.css',
    'laravel_mix_js_path' => 'js/app.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Menu-Configuration
    |
    */

    'menu' => [
        // Navbar items:
        [
            'type'         => 'navbar-search',
            'text'         => 'search',
            'topnav_right' => true,
        ],
        [
            'type'         => 'fullscreen-widget',
            'topnav_right' => true,
        ],

        // Sidebar items:
//        [
//            'type' => 'sidebar-menu-search',
//            'text' => 'search',
//        ],
        [
            'key'     => 'mastersd',
            'text'    => 'masters',
            'icon'    => 'fas fa-fw fa-user',
            'active' => ['admin/tag-masters*', 'admin/masters*'],
            'can' => ['все-тег-мастера', 'все-мастера'],
            'submenu' => [
                [
                    'key'         => 'tag_masters',
                    'text'        => 'tags',
                    'url'         => 'admin/tag-masters',
                    'icon'        => 'fa fa-fw fa-tag',
                    'label_color' => 'success',
                    'active'      => ['admin/tag-masters*'],
                    'can'         => 'все-тег-мастера',
                ],
                [
                    'key'         => 'masters',
                    'text'        => 'masters',
                    'url'         => 'admin/masters',
                    'icon'        => 'fa fa-fw fa-user',
                    'label_color' => 'success',
                    'active'      => ['admin/masters*'],
                    'can'         => 'все-мастера',
                ],
            ],
        ],
        [
            'key'     => 'blogsd',
            'text'    => 'blogs',
            'icon'    => 'fas fa-fw fa-share',
            'active' => ['admin/tags*', 'admin/blogs*'],
            'can' => ['все-тег-блог', 'все-блог'],
            'submenu' => [
                [
                    'key'         => 'tag_blogs',
                    'text'        => 'tags',
                    'url'         => 'admin/tags',
                    'icon'        => 'fa fa-fw fa-tag',
                    'label_color' => 'success',
                    'active'      => ['admin/tags*'],
                    'can'         => 'все-тег-блог',
                ],
                [
                    'key'         => 'blogs',
                    'text'        => 'blogs',
                    'url'         => 'admin/blogs',
                    'icon'        => 'fa fa-fw fa-file',
                    'label_color' => 'success',
                    'active'      => ['admin/blogs*'],
                    'can'         => 'все-блог',
                ],
            ],
        ],
        [
            'key'     => 'description_servicesd',
            'text'    => 'description_services',
            'icon'    => 'fas fa-fw fa-share',
            'active'  => ['admin/tag-description-services*', 'admin/description-services*'],
            'can'     => ['все-тег-услуги', 'все-описание-услуги'],
            'submenu' => [
                [
                    'key'         => 'tag_description_services',
                    'text'        => 'tags',
                    'url'         => 'admin/tag-description-services',
                    'icon'        => 'fa fa-fw fa-tag',
                    'label_color' => 'success',
                    'active'      => ['admin/tag-description-services*'],
                    'can'         => 'все-тег-услуги',
                ],
                [
                    'key'         => 'description_services',
                    'text'        => 'description_services',
                    'url'         => 'admin/description-services',
                    'icon'        => 'fa fa-fw fa-cogs',
                    'label_color' => 'success',
                    'active'      => ['admin/description-services*'],
                    'can'         => 'все-описание-услуги',
                ],
            ]
        ],
        [
            'key'         => 'services',
            'text'        => 'services',
            'url'         => 'admin/services',
            'icon'        => 'fa fa-fw fa-cogs',
            'label_color' => 'success',
            'active'      => ['admin/services*'],
            'can'         => 'все-услуги',
        ],
        [
            'key'         => 'abouts',
            'text'        => 'abouts',
            'url'         => 'admin/abouts',
            'icon'        => 'fa fa-fw fa-building',
            'label_color' => 'success',
            'active'      => ['admin/abouts*'],
            'can'         => 'все-о-нас',
        ],
        [
            'key'         => 'why_wes',
            'text'        => 'why_wes',
            'url'         => 'admin/why-wes',
            'icon'        => 'fa fa-fw fa-question',
            'label_color' => 'success',
            'active'      => ['admin/why-wes*'],
            'can'         => 'все-почему-мы',
        ],
        [
            'key'         => 'facts',
            'text'        => 'facts',
            'url'         => 'admin/facts',
            'icon'        => 'fa fa-fw fa-text-width',
            'label_color' => 'success',
            'active'      => ['admin/facts*'],
            'can'         => 'все-факты',
        ],
        [
            'key'         => 'certificates',
            'text'        => 'certificates',
            'url'         => 'admin/certificates',
            'icon'        => 'fa fa-fw fa-certificate',
            'label_color' => 'success',
            'active'      => ['admin/certificates*'],
            'can'         => 'все-сертификаты',
        ],
        [
            'key'         => 'marks',
            'text'        => 'marks',
            'url'         => 'admin/marks',
            'icon'        => 'fa fa-fw fa-trademark',
            'label_color' => 'success',
            'active'      => ['admin/marks*'],
            'can'         => 'все-марки',
        ],
        [
            'key'         => 'pages',
            'text'        => 'pages',
            'url'         => 'admin/pages',
            'icon'        => 'nav-icon fas fa-book',
            'label_color' => 'success',
            'active'      => ['admin/pages*'],
            'can'         => 'все-страницы',
        ],
        [
            'key'         => 'menus',
            'text'        => 'menus',
            'url'         => 'admin/menus',
            'icon'        => 'nav-icon fas fa-th',
            'label_color' => 'success',
            'active'      => ['admin/menus*'],
            'can'         => 'все-меню',
        ],
        [
            'key'     => 'contactsd',
            'text'    => 'contacts',
            'icon'    => 'fas fa-fw fa-share',
            'active' => ['admin/phones*', 'admin/emails*'],
            'can' => ['все-номер-тел', 'все-емэйл'],
            'submenu' => [
                [
                    'key'         => 'emails',
                    'text'        => 'emails',
                    'url'         => 'admin/emails',
                    'icon'        => 'fa fa-fw fa-envelope',
                    'label_color' => 'success',
                    'active'      => ['admin/emails*'],
                    'can'         => 'все-емэйл',
                ],
                [
                    'key'         => 'phones',
                    'text'        => 'phones',
                    'url'         => 'admin/phones',
                    'icon'        => 'fa fa-fw fa-phone',
                    'label_color' => 'success',
                    'active'      => ['admin/phones*'],
                    'can'         => 'все-номер-тел',
                ],
            ],
        ],
        [
            'key'     => 'settingsd',
            'text'    => 'settings',
            'icon'    => 'fas fa-fw fa-share',
            'active' => ['admin/admin*', 'admin/roles*', 'admin/settings*'],
            'can' => ['все-администраторы', 'все-роли', 'логотип'],
            'submenu' => [
                [
                    'key'  => 'admins',
                    'text' => 'admins',
                    'url'  => 'admin/admin',
                    'active' => ['admin/admin*'],
                    'can' => 'все-администраторы',
                ],
                [
                    'key'  => 'role',
                    'text' => 'role',
                    'url'  => 'admin/roles',
                    'active' => ['admin/roles*'],
                    'can' => 'все-роли',
                ],
                [
                    'key'  => 'settings',
                    'text' => 'settings',
                    'url'  => 'admin/settings',
                    'active' => ['admin/settings*'],
                    'can' => 'логотип',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For detailed instructions you can look the menu filters section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Menu-Configuration
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For detailed instructions you can look the plugins section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Plugins-Configuration
    |
    */

    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
        'summernote' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css',
                ],
            ],
        ],
        'jqueryUi' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js',
                ],
            ],
        ],
        'fancybox' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css',
                ],
            ],
        ],
        'mask-input' => [
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => 'https://unpkg.com/imask',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | IFrame
    |--------------------------------------------------------------------------
    |
    | Here we change the IFrame mode configuration. Note these changes will
    | only apply to the view that extends and enable the IFrame mode.
    |
    | For detailed instructions you can look the iframe mode section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/IFrame-Mode-Configuration
    |
    */

    'iframe' => [
        'default_tab' => [
            'url' => null,
            'title' => null,
        ],
        'buttons' => [
            'close' => true,
            'close_all' => true,
            'close_all_other' => true,
            'scroll_left' => true,
            'scroll_right' => true,
            'fullscreen' => true,
        ],
        'options' => [
            'loading_screen' => 1000,
            'auto_show_new_tab' => true,
            'use_navbar_items' => true,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Livewire
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Livewire support.
    |
    | For detailed instructions you can look the livewire here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Other-Configuration
    |
    */

    'livewire' => false,
];
