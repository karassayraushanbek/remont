<?php

use Illuminate\Support\Facades\Route;

Route::prefix('V1')->group(function () {
    Route::middleware(['auth.guard:user-api'])->prefix('user')->group(function () {
        Route::post('/login', [App\Http\Controllers\Api\V1\User\AuthController::class, 'login']);
        Route::post('/register', [App\Http\Controllers\Api\V1\User\AuthController::class, 'register']);
        Route::post('/update', [App\Http\Controllers\Api\V1\User\AuthController::class, 'update']);
        Route::post('/me', [App\Http\Controllers\Api\V1\User\AuthController::class, 'me']);
        Route::post('/logout', [App\Http\Controllers\Api\V1\User\AuthController::class, 'logout']);
        Route::post('/refresh', [App\Http\Controllers\Api\V1\User\AuthController::class, 'refresh']);
    });

    Route::get('/menus', [App\Http\Controllers\Api\V1\ApiController::class, 'getMenus']);
    Route::prefix('page')->group(function () {
        Route::get('main', [App\Http\Controllers\Api\V1\PagesController::class, 'mainPage']);
        Route::get('about', [App\Http\Controllers\Api\V1\PagesController::class, 'aboutPage']);
        Route::get('service', [App\Http\Controllers\Api\V1\PagesController::class, 'servicePage']);
        Route::get('review', [App\Http\Controllers\Api\V1\PagesController::class, 'reviewPage']);
        Route::get('master', [App\Http\Controllers\Api\V1\PagesController::class, 'masterPage']);
        Route::get('contact', [App\Http\Controllers\Api\V1\PagesController::class, 'contactPage']);
        Route::get('blog', [App\Http\Controllers\Api\V1\PagesController::class, 'blogPage']);
        Route::get('blog/{id}', [App\Http\Controllers\Api\V1\PagesController::class, 'firstBlogPage']);
    });
    Route::get('/logo', [App\Http\Controllers\Api\V1\ApiController::class, 'logo']);
    Route::get('/call', [App\Http\Controllers\Api\V1\ApiController::class, 'call']);

    Route::get('/phones', [App\Http\Controllers\Api\V1\ApiController::class, 'getPhones']);
    Route::get('/emails', [App\Http\Controllers\Api\V1\ApiController::class, 'getEmails']);

    Route::post('/subscribe', [App\Http\Controllers\Api\V1\FeedbackController::class, 'subscribe']);
    Route::post('/feedback', [App\Http\Controllers\Api\V1\FeedbackController::class, 'feedback']);
    Route::get('/sitemap', [App\Http\Controllers\Api\V1\SitemapController::class, 'index']);
});
