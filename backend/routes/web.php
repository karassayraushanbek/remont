<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('admin.login');
});

Route::prefix('reader')->name('reader.')->group(function() {
    Route::middleware(['guest:reader'])->group(function() {
        Route::get('/login', [App\Http\Controllers\Reader\Auth\LoginController::class, 'showLoginForm'])->name('login');
        Route::post('/login', [App\Http\Controllers\Reader\Auth\LoginController::class, 'login'])->name('check');
    });

    Route::middleware(['auth:reader'])->group(function() {
        //Route::get('/home', [App\Http\Controllers\Reader\HomeController::class, 'index'])->name('home');
        Route::get('/home', function () {
            return redirect('/api/documentation');
        })->name('home');
        Route::post('/logout', [App\Http\Controllers\Reader\Auth\LoginController::class, 'logout'])->name('logout');
    });
});

Route::prefix('admin')->name('admin.')->group(function() {
    Route::middleware(['guest:admin'])->group(function() {
        Route::get('/login', [App\Http\Controllers\Admin\Auth\LoginController::class, 'showLoginForm'])->name('login');
        Route::post('/login', [App\Http\Controllers\Admin\Auth\LoginController::class, 'login'])->name('check');
    });

    Route::middleware(['auth:admin', 'is_status_true'])->group(function() {
        Route::get('/home', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('home');
        Route::post('/logout', [App\Http\Controllers\Admin\Auth\LoginController::class, 'logout'])->name('logout');

        Route::get('/profile', [App\Http\Controllers\Admin\ProfileController::class, 'index'])->name('profile');
        Route::patch('/profile/update', [App\Http\Controllers\Admin\ProfileController::class, 'update'])->name('profile.update');

        Route::resource('/roles', App\Http\Controllers\Admin\RoleController::class);
        Route::resource('/admin', App\Http\Controllers\Admin\AdminController::class);
        Route::patch('/admin/{id}/update/status', [App\Http\Controllers\Admin\AdminController::class, 'updateAdminStatus'])->name('admin.update.status');

        Route::resource('/users', App\Http\Controllers\Admin\UserController::class);
        Route::patch('/users/{id}/update/status', [App\Http\Controllers\Admin\UserController::class, 'updateUserStatus'])->name('users.update.status');

        Route::prefix('settings')->name('setting.')->group(function() {
            Route::get('/', [App\Http\Controllers\Admin\SettingController::class, 'index'])->name('index');
            Route::post('/store', [App\Http\Controllers\Admin\SettingController::class, 'store'])->name('store');
            Route::patch('/{id}/update', [App\Http\Controllers\Admin\SettingController::class, 'update'])->name('update');
            Route::delete('/{id}/destroy', [App\Http\Controllers\Admin\SettingController::class, 'destroy'])->name('destroy');
        });

        Route::resource('/language', App\Http\Controllers\Admin\LanguageController::class);

        Route::resource('/menus', App\Http\Controllers\Admin\MenuController::class);
        Route::post('/menus/position/{id}/update', [App\Http\Controllers\Admin\MenuController::class, 'updatePosition'])->name('menus.position.update');

        Route::resource('/pages', App\Http\Controllers\Admin\PageController::class);
        Route::post('/pages/position/{id}/update', [App\Http\Controllers\Admin\PageController::class, 'updatePosition'])->name('pages.position.update');
        Route::resource('pages.banners', App\Http\Controllers\Admin\BannerController::class);

        Route::resource('/marks', App\Http\Controllers\Admin\MarkController::class);
        Route::post('/marks/position/{id}/update', [App\Http\Controllers\Admin\MarkController::class, 'updatePosition'])->name('marks.position.update');

        Route::resource('/certificates', App\Http\Controllers\Admin\CertificateController::class);
        Route::post('/certificates/position/{id}/update', [App\Http\Controllers\Admin\CertificateController::class, 'updatePosition'])->name('certificates.position.update');

        Route::resource('/facts', App\Http\Controllers\Admin\FactController::class);
        Route::post('/facts/position/{id}/update', [App\Http\Controllers\Admin\FactController::class, 'updatePosition'])->name('facts.position.update');

        Route::resource('/why-wes', App\Http\Controllers\Admin\WhyWeController::class);
        Route::post('/why-wes/position/{id}/update', [App\Http\Controllers\Admin\WhyWeController::class, 'updatePosition'])->name('why-wes.position.update');

        Route::resource('/abouts', App\Http\Controllers\Admin\AboutController::class);
        Route::post('/abouts/position/{id}/update', [App\Http\Controllers\Admin\AboutController::class, 'updatePosition'])->name('abouts.position.update');

        Route::resource('/services', App\Http\Controllers\Admin\ServiceController::class);
        Route::post('/services/position/{id}/update', [App\Http\Controllers\Admin\ServiceController::class, 'updatePosition'])->name('services.position.update');

        Route::resource('/tag-description-services', App\Http\Controllers\Admin\TagDescriptionServiceController::class);
        Route::post('/tag-description-services/position/{id}/update', [App\Http\Controllers\Admin\TagDescriptionServiceController::class, 'updatePosition'])->name('tag-description-services.position.update');

        Route::resource('/description-services', App\Http\Controllers\Admin\DescriptionServiceController::class);
        Route::post('/description-services/position/{id}/update', [App\Http\Controllers\Admin\DescriptionServiceController::class, 'updatePosition'])->name('description-services.position.update');

        Route::resource('/tags', App\Http\Controllers\Admin\TagController::class);
        Route::post('/tags/position/{id}/update', [App\Http\Controllers\Admin\TagController::class, 'updatePosition'])->name('tags.position.update');

        Route::resource('/blogs', App\Http\Controllers\Admin\BlogController::class);
        Route::post('/blogs/position/{id}/update', [App\Http\Controllers\Admin\BlogController::class, 'updatePosition'])->name('blogs.position.update');

        Route::resource('blogs.reviews', App\Http\Controllers\Admin\ReviewController::class);
        Route::post('/reviews/position/{id}/update', [App\Http\Controllers\Admin\ReviewController::class, 'updatePosition'])->name('reviews.position.update');

        Route::resource('/tag-masters', App\Http\Controllers\Admin\TagMasterController::class);
        Route::post('/tag-masters/position/{id}/update', [App\Http\Controllers\Admin\TagMasterController::class, 'updatePosition'])->name('tag-masters.position.update');

        Route::resource('/masters', App\Http\Controllers\Admin\MasterController::class);
        Route::post('/masters/position/{id}/update', [App\Http\Controllers\Admin\MasterController::class, 'updatePosition'])->name('masters.position.update');

        Route::resource('/phones', App\Http\Controllers\Admin\PhoneController::class);
        Route::resource('/emails', App\Http\Controllers\Admin\EmailController::class);

        Route::resource('/subscribes', App\Http\Controllers\Admin\SubscribeController::class);
        Route::resource('/feedbacks', App\Http\Controllers\Admin\FeedbackController::class);
    });
});

Route::get('optimize-clear', function () {
    Artisan::call('optimize:clear');
    return back()->with('fail', 'Optimize clear!');
})->name('optimizeClear');

Route::get('storage-link', function () {
    Artisan::call('storage:link');
    return back()->with('fail', 'Storage link!');
})->name('storageLink');
