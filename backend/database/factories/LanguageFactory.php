<?php

namespace Database\Factories;

use App\Models\Language;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Language>
 */
class LanguageFactory extends Factory
{
    protected $model = Language::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $slug = ['ru', 'en', 'kz'];
        $name = ['Русский', 'Английский', 'Казахский'];

        $index = $this->faker->unique()->randomElement([0, 1, 2]);

        $slug = array_splice($slug, $index, 1);
        $name = array_splice($name, $index, 1);

        Schema::table('translates', function (Blueprint $table) use ($slug) {
            $table->longText($slug[0])->nullable();
        });

        return [
            'slug' => $slug[0],
            'name' => $name[0],
        ];
    }
}
