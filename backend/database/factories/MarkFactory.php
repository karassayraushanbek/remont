<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use App\Models\Mark;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use App\Services\FileService;
use Illuminate\Http\UploadedFile;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Mark>
 */
class MarkFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $images = Storage::disk('public')->files('fake');
        $image = Arr::random($images);
        $fakeFile = new UploadedFile(
            storage_path("app/public/{$image}"),
            'fake.jpg', // имя файла
            mime_content_type(storage_path("app/public/{$image}")),
            null,
            true
        );

        $markCount = config('seeder.marks', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'image' => (new FileService())->saveFile($fakeFile, Mark::IMAGE_PATH),
            'title_tr' => $translationService->createTranslation($title)->id,
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $markCount),
        ];
    }
}
