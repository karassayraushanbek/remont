<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use App\Models\Certificate;
use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Certificate>
 */
class CertificateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $images = Storage::disk('public')->files('fake');
        $image = Arr::random($images);
        $fakeFile = new UploadedFile(
            storage_path("app/public/{$image}"),
            'fake.jpg',
            mime_content_type(storage_path("app/public/{$image}")),
            null,
            true
        );

        $certificateCount = config('seeder.certificates', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'image' => (new FileService())->saveFile($fakeFile, Certificate::IMAGE_PATH),
            'title_tr' => $translationService->createTranslation($title)->id,
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $certificateCount),
        ];
    }
}
