<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tag>
 */
class TagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tagCount = config('seeder.tags', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'title_tr' => $translationService->createTranslation($title)->id,
            'slug' => Str::slug($translationService->createTranslation($title)->ru),
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $tagCount),
        ];
    }
}
