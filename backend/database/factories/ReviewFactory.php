<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use App\Models\Blog;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Review>
 */
class ReviewFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $reviewsCount = config('seeder.reviews', 1);
        $description = ucfirst($this->faker->sentence()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'blog_id' => Blog::get()->random()->id,
            'image' => null,
            'name' => $this->faker->name(),
            'description_tr' => $translationService->createTranslation($description)->id,
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $reviewsCount),
        ];
    }
}
