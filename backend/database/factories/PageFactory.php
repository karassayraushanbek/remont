<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use App\Models\Menu;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Page>
 */
class PageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $pagesCount = config('seeder.pages', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $description = ucfirst($this->faker->sentence()) . uniqid();
        $metaTitle = ucfirst($this->faker->word()) . uniqid();
        $metaDescription = ucfirst($this->faker->paragraph($this->faker->numberBetween(1, 2))) . uniqid();
        $metaKeywords = ucfirst($this->faker->sentence()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'menu_id' => Menu::get()->random()->id,
            'title_tr' => $translationService->createTranslation($title)->id,
            'description_tr' => $translationService->createTranslation($description)->id,
            'meta_title_tr' => $translationService->createTranslation($metaTitle)->id,
            'meta_description_tr' => $translationService->createTranslation($metaDescription)->id,
            'meta_keywords_tr' => $translationService->createTranslation($metaKeywords)->id,
            'slug' => Str::slug($translationService->createTranslation($title)->ru),
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $pagesCount),
        ];
    }
}
