<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use App\Models\Fact;
use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Fact>
 */
class FactFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $images = Storage::disk('public')->files('fake');
        $image = Arr::random($images);
        $fakeFile = new UploadedFile(
            storage_path("app/public/{$image}"),
            'fake.jpg',
            mime_content_type(storage_path("app/public/{$image}")),
            null,
            true
        );

        $factCount = config('seeder.facts', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'image' => (new FileService())->saveFile($fakeFile, Fact::IMAGE_PATH),
            'number' => $this->faker->numberBetween(0, 8000),
            'title_tr' => $translationService->createTranslation($title)->id,
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $factCount),
        ];
    }
}
