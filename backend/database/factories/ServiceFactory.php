<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Service>
 */
class ServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $serviceCount = config('seeder.services', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $description = ucfirst($this->faker->sentence()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'title_tr' => $translationService->createTranslation($title)->id,
            'description_tr' => $translationService->createTranslation($description)->id,
            'phone' => $this->faker->numerify('+ 7 702 ### ####'),
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $serviceCount),
        ];
    }
}
