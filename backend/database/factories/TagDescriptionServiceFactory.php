<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\TagDescriptionService>
 */
class TagDescriptionServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tagDescriptionServiceCount = config('seeder.tdservices', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'title_tr' => $translationService->createTranslation($title)->id,
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $tagDescriptionServiceCount),
        ];
    }
}
