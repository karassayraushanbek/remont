<?php

namespace Database\Factories;

use App\Models\Setting;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Setting>
 */
class SettingFactory extends Factory
{
    protected $model = Setting::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $type = ['image', 'text', 'textarea', 'editor'];
        return [
            'key' => $this->faker->word(),
            'display_name' => $this->faker->word(),
            'value' => null,
            'details' => null,
            'type' => $type[$this->faker->numberBetween(0, 3)],
            'group' => 'admin',
        ];
    }
}
