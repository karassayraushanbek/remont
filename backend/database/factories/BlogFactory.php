<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use App\Models\Blog;
use App\Models\Tag;
use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Blog>
 */
class BlogFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $images = Storage::disk('public')->files('fake');
        $image = Arr::random($images);
        $fakeFile = new UploadedFile(
            storage_path("app/public/{$image}"),
            'fake.jpg',
            mime_content_type(storage_path("app/public/{$image}")),
            null,
            true
        );

        $blogsCount = config('seeder.blogs', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $description = ucfirst($this->faker->sentence()) . uniqid();
        $metaTitle = ucfirst($this->faker->word()) . uniqid();
        $metaDescription = ucfirst($this->faker->paragraph($this->faker->numberBetween(1, 2))) . uniqid();
        $metaKeywords = ucfirst($this->faker->sentence()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'tag_id' => Tag::get()->random()->id,
            'image' => (new FileService())->saveFile($fakeFile, Blog::IMAGE_PATH),
            'title_tr' => $translationService->createTranslation($title)->id,
            'description_tr' => $translationService->createTranslation($description)->id,
            'read' => 5,
            'meta_title_tr' => $translationService->createTranslation($metaTitle)->id,
            'meta_description_tr' => $translationService->createTranslation($metaDescription)->id,
            'meta_keywords_tr' => $translationService->createTranslation($metaKeywords)->id,
            'slug' => Str::slug($translationService->createTranslation($title)->ru),
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $blogsCount),
        ];
    }
}
