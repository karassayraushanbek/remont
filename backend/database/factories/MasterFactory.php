<?php

namespace Database\Factories;

use App\Contracts\TranslateServiceContract;
use App\Models\Master;
use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Master>
 */
class MasterFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $images = Storage::disk('public')->files('fake');
        $image = Arr::random($images);
        $fakeFile = new UploadedFile(
            storage_path("app/public/{$image}"),
            'fake.jpg',
            mime_content_type(storage_path("app/public/{$image}")),
            null,
            true
        );

        $mastersCount = config('seeder.masters', 1);
        $title = ucfirst($this->faker->word()) . uniqid();
        $description = ucfirst($this->faker->sentence()) . uniqid();
        $translationService = resolve(TranslateServiceContract::class);

        return [
            'image' => (new FileService())->saveFile($fakeFile, Master::IMAGE_PATH),
            'name' => fake()->name(),
            'reviews' => $this->faker->numberBetween(0, 200),
            'title_tr' => $translationService->createTranslation($title)->id,
            'description_tr' => $translationService->createTranslation($description)->id,
            'is_published' => $this->faker->numberBetween(0, 1),
            'position' => $this->faker->numberBetween(0, $mastersCount),
        ];
    }
}
