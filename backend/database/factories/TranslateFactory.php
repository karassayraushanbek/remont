<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Translate;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Translate>
 */
class TranslateFactory extends Factory
{
    protected $model = Translate::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ru' => $this->faker->text(300),
            'en' => $this->faker->text(300),
            'kz' => $this->faker->text(300)
        ];
    }
}
