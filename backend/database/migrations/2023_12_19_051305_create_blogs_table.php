<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Tag;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Tag::class)->nullable();
            $table->string('image')->nullable();
            $table->foreignId('title_tr')->nullable()->constrained('translates');
            $table->foreignId('description_tr')->nullable()->constrained('translates');
            $table->integer('read')->default(0);
            $table->foreignId('meta_title_tr')->nullable()->constrained('translates');
            $table->foreignId('meta_description_tr')->nullable()->constrained('translates');
            $table->foreignId('meta_keywords_tr')->nullable()->constrained('translates');
            $table->string('slug')->nullable();
            $table->boolean('is_published')->default(1);
            $table->integer('position')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('blogs');
    }
};
