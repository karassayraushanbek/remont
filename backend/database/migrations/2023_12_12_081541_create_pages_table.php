<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Menu;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Menu::class)->nullable();
            $table->foreignId('title_tr')->nullable()->constrained('translates');
            $table->foreignId('description_tr')->nullable()->constrained('translates');
            $table->foreignId('meta_title_tr')->nullable()->constrained('translates');
            $table->foreignId('meta_description_tr')->nullable()->constrained('translates');
            $table->foreignId('meta_keywords_tr')->nullable()->constrained('translates');
            $table->string('slug')->nullable();
            $table->boolean('is_published')->default(1);
            $table->integer('position')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pages');
    }
};
