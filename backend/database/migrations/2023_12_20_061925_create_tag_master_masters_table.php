<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tag_master_masters', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tag_master_id')->nullable()->index()->constrained('tag_masters')->nullOnDelete();
            $table->foreignId('master_id')->nullable()->index()->constrained('masters')->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tag_master_masters');
    }
};
