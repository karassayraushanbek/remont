<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('facts', function (Blueprint $table) {
            $table->foreignId('alt_title_tr')->nullable()->constrained('translates');
            $table->foreignId('description_tr')->nullable()->constrained('translates');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('facts', function (Blueprint $table) {
            $table->dropColumn(['alt_title_tr', 'description_tr']);
        });
    }
};
