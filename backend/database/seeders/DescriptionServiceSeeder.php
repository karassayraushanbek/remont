<?php

namespace Database\Seeders;

use App\Models\DescriptionService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DescriptionServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DescriptionService::factory()->count(config('seeder.dservices', 1))->create();
    }
}
