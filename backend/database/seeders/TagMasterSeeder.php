<?php

namespace Database\Seeders;

use App\Models\TagMaster;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TagMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TagMaster::factory()->count(config('seeder.tagmasters', 1))->create();
    }
}
