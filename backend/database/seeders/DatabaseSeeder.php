<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            PermissionTableSeeder::class,
            AdminSeeder::class,
            ReaderSeeder::class,
            LanguageSeeder::class,
            //UserSeeder::class,
            MenuSeeder::class,
            PageSeeder::class,
            MarkSeeder::class,
            CertificateSeeder::class,
            FactSeeder::class,
            WhyWeSeeder::class,
            AboutSeeder::class,
            ServiceSeeder::class,
            BannerSeeder::class,
            TagSeeder::class,
            BlogSeeder::class,
            ReviewSeeder::class,
            PhoneSeeder::class,
            EmailSeeder::class,
            TagMasterSeeder::class,
            MasterSeeder::class,
        ]);
    }
}
