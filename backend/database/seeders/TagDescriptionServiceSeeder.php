<?php

namespace Database\Seeders;

use App\Models\TagDescriptionService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TagDescriptionServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TagDescriptionService::factory()->count(config('seeder.tdservices', 1))->create();
    }
}
