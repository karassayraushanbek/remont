<?php

namespace Database\Seeders;

use App\Models\Banner;
use App\Models\Translate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Banner::factory()->count(config('seeder.banners', 1))->create();
        DB::table('banners')->truncate();

        foreach (config('seeder.bannerData') as $item) {
            $titleData = [
                'ru' => $item['title_ru'],
                'en' => $item['title_en'],
                'kz' => $item['title_kz'],
            ];
            $descriptionData = [
                'ru' => $item['description_ru'],
                'en' => $item['description_en'],
                'kz' => $item['description_kz'],
            ];

            Banner::create([
                'page_id' => $item['page_id'],
                'image' => null,
                'title_tr' => Translate::create($titleData)->id,
                'description_tr' => Translate::create($descriptionData)->id,
                'position' => $item['position'],
            ]);
        }
    }
}
