<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\Translate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Menu::factory()->count(config('seeder.menus', 1))->create();

        foreach (config('seeder.menusData') as $item) {
            $titleData = [
                'ru' => $item['title_ru'],
                'en' => $item['title_en'],
                'kz' => $item['title_kz'],
            ];

            Menu::create([
                'title_tr' => Translate::create($titleData)->id,
                'is_published' => $item['is_published'],
                'position' => $item['position'],
            ]);
        }
    }
}
