<?php

namespace Database\Seeders;

use App\Models\Fact;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Fact::factory()->count(config('seeder.facts', 1))->create();
    }
}
