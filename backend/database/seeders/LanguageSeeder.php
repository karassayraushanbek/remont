<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use App\Models\Language;
use Illuminate\Support\Facades\Schema;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $slug = ['ru', 'en', 'kz'];
        $name = ['Русский', 'Английский', 'Казахский'];

        foreach ($slug as $index => $languageSlug) {
            Language::create([
                'slug' => $languageSlug,
                'name' => $name[$index],
            ]);

            Schema::table('translates', function (Blueprint $table) use ($languageSlug) {
                $table->longText($languageSlug)->nullable();
            });
        }
    }
}
