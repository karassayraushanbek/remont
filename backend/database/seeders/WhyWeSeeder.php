<?php

namespace Database\Seeders;

use App\Models\WhyWe;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class WhyWeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        WhyWe::factory()->count(config('seeder.why_we', 1))->create();
    }
}
