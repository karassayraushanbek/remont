<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\Translate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Page::factory()->count(config('seeder.pages', 1))->create();

        foreach (config('seeder.pagesData') as $item) {
            $titleData = [
                'ru' => $item['title_ru'],
                'en' => $item['title_en'],
                'kz' => $item['title_kz'],
            ];
            $descriptionData = [
                'ru' => $item['description_ru'],
                'en' => $item['description_en'],
                'kz' => $item['description_kz'],
            ];
            $metaTitleData = [
                'ru' => $item['meta_title_ru'],
                'en' => $item['meta_title_en'],
                'kz' => $item['meta_title_kz'],
            ];
            $metaDescriptionData = [
                'ru' => $item['meta_description_ru'],
                'en' => $item['meta_description_en'],
                'kz' => $item['meta_description_kz'],
            ];
            $metaKeywordsData = [
                'ru' => $item['meta_keywords_ru'],
                'en' => $item['meta_keywords_en'],
                'kz' => $item['meta_keywords_kz'],
            ];

            Page::create([
                'menu_id' => $item['menu_id'],
                'title_tr' => Translate::create($titleData)->id,
                'description_tr' => Translate::create($descriptionData)->id,
                'meta_title_tr' => Translate::create($metaTitleData)->id,
                'meta_description_tr' => Translate::create($metaDescriptionData)->id,
                'meta_keywords_tr' => Translate::create($metaKeywordsData)->id,
                'slug' => $item['slug'],
                'is_published' => $item['is_published'],
                'position' => $item['position'],
            ]);
        }
    }
}
