## Установка

```bash
    
    # 1.Запуск Docker контейнеров
    ./vendor/bin/sail up -d
    # 2.Вход в контейнер src-laravel.test-1
    docker exec -it src-laravel.test-1 /bin/sh
    # 3.Команда Laravel для очистки кэша
    php artisan optimize:clear
    # 4.Команда Laravel для миграции всех таблиц в БД
    php artisan migrate
    # 5.Команда Laravel для заполнить БД данными
    php artisan db:seed

    # Остановка Docker контейнеров(Необезательно!)
    ./vendor/bin/sail down
    # Просмотреть логи(Необезательно!)
    docker-compose logs laravel.test

```

## Админка

<table>
        <thead>
            <th>URL</th>
            <th>Логин</th>
            <th>Пароль</th>
            <th>Панель</th>
        </thead>
    <tbody>
        <tr> 
            <td> http://localhost:80/admin/login </td>
            <td> admin@admin.com </td>
            <td> 123admin </td>
            <td> Панель Администратора </td>
        </tr>
        <tr> 
            <td> http://localhost:80/reader/login </td>
            <td> api@admin.com </td>
            <td> 123admin </td>
            <td> Панель API SWAGGER </td>
        </tr>
        <tr> 
            <td> http://localhost:54099 </td>
            <td> root </td>
            <td>  </td>
            <td> Панель Базы Данных(phpmyadmin) </td>
        </tr>
    </tbody>
</table>
