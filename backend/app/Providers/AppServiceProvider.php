<?php

namespace App\Providers;

use App\Contracts\FileServiceContract;
use App\Contracts\TranslateServiceContract;
use App\Services\Admin\Feedback\FeedbackService;
use App\Services\Admin\Subscribe\SubscribeService;
use App\Services\FileService;
use App\Services\TranslateService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Event;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(TranslateServiceContract::class, TranslateService::class);
        $this->app->singleton(FileServiceContract::class, FileService::class);
        $this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Paginator::useBootstrapFive();
        Paginator::useBootstrapFour();
        Schema::defaultStringLength(191);

        if (Schema::hasTable('subscribes')) {
            $today = Carbon::today();
            $subscribeLabel = [];
            $subscribeCount = (new SubscribeService())->getSubscribesCountToday($today);
            if ($subscribeCount > 0) {
                $subscribeLabel = ['label' => $subscribeCount];
            }

            $feedbackLabel = [];
            $feedbackCount = (new FeedbackService())->getFeedbackCountToday($today);
            if ($feedbackCount > 0) {
                $feedbackLabel = ['label' => $feedbackCount];
            }

            Event::listen(BuildingMenu::class, function (BuildingMenu $event) use($subscribeLabel, $feedbackLabel) {
                $subscribesItem = [
                    'key'         => 'subscribes',
                    'text'        => 'subscribes',
                    'url'         => 'admin/subscribes',
                    'icon'        => 'fa fa-fw fa-envelope',
                    'label_color' => 'success',
                    'active'      => ['admin/subscribes*'],
                    'can'         => 'все-подписчики',
                ];
                $subscribesItem = array_merge($subscribesItem, $subscribeLabel);

                $feedbackItem = [
                    'key'         => 'feedbacks',
                    'text'        => 'feedbacks',
                    'url'         => 'admin/feedbacks',
                    'icon'        => 'fa fa-fw fa-envelope',
                    'label_color' => 'success',
                    'active'      => ['admin/feedbacks*'],
                    'can'         => 'все-заявки',
                ];
                $feedbackItem = array_merge($feedbackItem, $feedbackLabel);

                $event->menu->addAfter('menus', $subscribesItem);
                $event->menu->addAfter('subscribes', $feedbackItem);
            });
        }
    }
}
