<?php

namespace App\Console\Commands;

use App\Models\Language;
use App\Models\Blog;
use Spatie\Sitemap\Sitemap;
use Illuminate\Console\Command;

class CreateSitemapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Sitemap Command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
      $sitemap = Sitemap::create();
      $sitemap->add('https://remontmaster.kz');
      $languages = Language::orderBy('id', 'asc')->get('slug');
      $blogs = Blog::where('is_published', 1)->orderBy('id', 'asc')->get('id');

      foreach ($languages as $language) {
        $sitemap->add('https://remontmaster.kz/'.$language->slug.'/about');
        $sitemap->add('https://remontmaster.kz/'.$language->slug.'/services');
        $sitemap->add('https://remontmaster.kz/'.$language->slug.'/reviews');
        $sitemap->add('https://remontmaster.kz/'.$language->slug.'/masters');
        $sitemap->add('https://remontmaster.kz/'.$language->slug.'/contacts');
        $sitemap->add('https://remontmaster.kz/'.$language->slug.'/blog');
        foreach ($blogs as $blog) {
          $sitemap->add('https://remontmaster.kz/'.$language->slug.'/blog/'.$blog->id);
        }
      }

      $sitemap->writeToDisk('public', 'sitemap.xml');

      return $this->info("The command was successful!");
    }
}
