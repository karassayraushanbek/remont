<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Setting;

class CreateLogoSettingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:logo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create unique Logo for table settings';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Setting::create([
            'key' => 'logo',
            'display_name' => 'Логотип',
            'type' => 'image',
            'group' => 'admin',
        ]);

        Setting::create([
            'key' => 'phone',
            'display_name' => 'Позвонить',
            'type' => 'text',
            'group' => 'admin',
        ]);

        return $this->info("The command was successful!");
    }
}
