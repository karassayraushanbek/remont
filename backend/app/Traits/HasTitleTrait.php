<?php

namespace App\Traits;

use App\Models\Translate;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasTitleTrait
{
    public function title(): BelongsTo
    {
        return $this->belongsTo(Translate::class, 'title_tr');
    }
}
