<?php

namespace App\Traits;

use App\Models\Translate;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasDescriptionTrait
{
    public function description(): BelongsTo
    {
        return $this->belongsTo(Translate::class, 'description_tr');
    }
}
