<?php

namespace App\Traits;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasAdminTrait
{
    public function admin(): BelongsTo
    {
        return $this->belongsTo(Admin::class);
    }
}
