<?php

namespace App\Traits;

use App\Contracts\TranslateServiceContract;

trait HasLangTrait
{
    public function getLangs(): array
    {
        $service = resolve(TranslateServiceContract::class);
        return $service->getLanguages();
    }
}
