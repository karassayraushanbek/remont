<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait HasImageTrait
{
    public function getImageUrlAttribute(): string|null
    {
        return $this->image ? Storage::disk('custom')->url(self::IMAGE_PATH . '/' . $this->image) : '';
    }
}
