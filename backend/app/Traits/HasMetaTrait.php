<?php

namespace App\Traits;

use App\Models\Translate;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasMetaTrait
{
    public function meta_description(): BelongsTo
    {
        return $this->belongsTo(Translate::class, 'meta_description_tr');
    }

    public function meta_keywords(): BelongsTo
    {
        return $this->belongsTo(Translate::class, 'meta_keywords_tr');
    }

    public function meta_title(): BelongsTo
    {
        return $this->belongsTo(Translate::class, 'meta_title_tr');
    }
}
