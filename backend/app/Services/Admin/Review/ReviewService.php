<?php

namespace App\Services\Admin\Review;

use App\Contracts\HasLangContract;
use App\Models\Blog;
use App\Models\Review;
use App\Models\Translate;
use App\Services\FileService;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

class ReviewService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'description',
    ];

    public function getReviews(Blog $blog): LengthAwarePaginator
    {
        $reviews = Review::where('blog_id', $blog->id)
            ->orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $reviews;
    }

    public function createReview(Blog $blog, array $data): ?bool
    {
        $review = new Review();

        foreach ($this->translationFields as $field) {
            $review->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

//        if (isset($data['image'])) {
//            $review->image = (new FileService())->saveFile($data['image'], Review::IMAGE_PATH);
//        }

        $review->blog_id = $data['blog_id'];
        $review->name = $data['name'];
        $review->is_published = $data['is_published'];
        $review->position = $data['position'];
        $review->rating = $data['rating'];

        return $review->save();
    }

    public function updateReview(Blog $blog, Review $review, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($review->{$field . '_tr'}) {
                $review->{$field}->update($data[$field]);
            } else {
                $review->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

//        if (isset($data['image'])) {
//            $review->image = (new FileService())->saveFile($data['image'], Blog::IMAGE_PATH, $review->image);
//        }

        $review->blog_id = $data['blog_id'];
        $review->name = $data['name'];
        $review->is_published = $data['is_published'];
        $review->position = $data['position'];
        $review->rating = $data['rating'];

        return $review->update();
    }

    public function deleteReview(Blog $blog, Review $review): ?bool
    {
        $review->delete();

//        if ($review->image) {
//            (new FileService())->deleteFile($review->image, Review::IMAGE_PATH);
//        }

        if ($review->description_tr) {
            Translate::find($review->description_tr)->delete();
        }

        return true;
    }

    public function updateReviewPosition(array $data, string $id): int|bool
    {
        $review = Review::find($id);
        $review->position = $data['position'];
        $review->update();

        return $id;
    }
}
