<?php

namespace App\Services\Admin\Banner;

use App\Contracts\HasLangContract;
use App\Models\Banner;
use App\Models\Page;
use App\Models\Translate;
use App\Services\FileService;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class BannerService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        'description',
    ];

    public function getBanners(Page $page): LengthAwarePaginator
    {
        $banners = Banner::where('page_id', $page->id)
            ->orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $banners;
    }

    public function createBanner(Page $page, array $data): ?bool
    {
        $banner = new Banner();

        foreach ($this->translationFields as $field) {
            $banner->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        if (isset($data['image'])) {
            $banner->image = (new FileService())->saveFile($data['image'], Banner::IMAGE_PATH);
        }

        $banner->page_id = $data['page_id'];
        //$banner->is_published = $data['is_published'];
        //$banner->position = $data['position'];

        return $banner->save();
    }

    public function updateBanner(Page $page, Banner $banner, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($banner->{$field . '_tr'}) {
                $banner->{$field}->update($data[$field]);
            } else {
                $banner->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        if (isset($data['image'])) {
            $banner->image = (new FileService())->saveFile($data['image'], Banner::IMAGE_PATH, $banner->image);
        }

        $banner->page_id = $data['page_id'];
        //$banner->is_published = $data['is_published'];
        //$banner->position = $data['position'];

        return $banner->update();
    }

    public function deleteBanner(Page $page, Banner $banner): ?bool
    {
        $banner->delete();

        if ($banner->title_tr) {
            Translate::find($banner->title_tr)->delete();
        }

        if ($banner->description_tr) {
            Translate::find($banner->description_tr)->delete();
        }

        if ($banner->image) {
            (new FileService())->deleteFile($banner->image, Banner::IMAGE_PATH);
        }

        return true;
    }

    public function updateBannerPosition(array $data, string $id): int|bool
    {
        $banner = Banner::find($id);
        $banner->position = $data['position'];
        $banner->update();

        return $id;
    }
}
