<?php

namespace App\Services\Admin\Tag;

use App\Contracts\HasLangContract;
use App\Models\Tag;
use App\Models\Translate;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class TagService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
    ];

    public function getTags(): LengthAwarePaginator
    {
        $tags = Tag::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $tags;
    }

    public function getTagCollection(): Collection
    {
        $tags = Tag::orderBy('position', 'ASC')
            ->latest()
            ->get();

        return $tags;
    }

    public function createTag(array $data): ?bool
    {
        $tags = new Tag();

        foreach ($this->translationFields as $field) {
            $tags->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        $tags->slug = Str::slug($data['title']['ru']);
        $tags->is_published = $data['is_published'];
        $tags->position = $data['position'];

        return $tags->save();
    }

    public function updateTag(Tag $tags, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($tags->{$field . '_tr'}) {
                $tags->{$field}->update($data[$field]);
            } else {
                $tags->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        $tags->slug = Str::slug($data['title']['ru']);
        $tags->is_published = $data['is_published'];
        $tags->position = $data['position'];

        return $tags->update();
    }

    public function deleteTag(Tag $tags): ?bool
    {
        $tags->delete();

        Translate::find($tags->title_tr)->delete();

        return true;
    }

    public function updateTagPosition(array $data, string $id): int|bool
    {
        $tags = Tag::find($id);
        $tags->position = $data['position'];
        $tags->update();

        return $id;
    }
}
