<?php

namespace App\Services\Admin\Setting;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Setting;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SettingService
{
    public $perPage = 25;

    public function getSettings(): Collection
    {
        return Setting::where('group', 'admin')->latest()->get();
    }

    public function createSetting(array $data): ?bool
    {
        $setting = new Setting();

        Setting::create([
            'key' => $data['key'],
            'display_name' => $data['display_name'],
            'type' => $data['type'],
            'group' => 'admin',
        ]);

        return $setting->save();
    }

    public function updateSetting(Setting $setting, array $data): ?bool
    {
        if($setting->type == 'image') {
            if (array_key_exists('image', $data) && $data['image']->isValid()) {
                if($setting->key == 'logo') {
                    $file = $data['image'];
                    $name = 'AdminLTELogo.' . $data['image']->extension();
                    $path = '/vendor/adminlte/dist/img/';
                    $fullPath =  $path . $name;

                    if ($setting->value != null) {
                        File::delete($fullPath);
                    }
                    $file->move(public_path($path), $name);
                    $data['image'] = $fullPath;
                    $setting->value = $data['image'];
                    $setting->update();
                } else {
                    if ($setting->value != null) {
                        Storage::disk('public')->delete($setting->value);
                    }
                    $name = hexdec(uniqid()) . '.' . $data['image']->extension();
                    $path = 'settings';
                    $data['image'] = $data['image']->storeAs($path, $name, 'public');
                    $setting->value = $data['image'];
                    $setting->update();
                }
            }
        }
        if($setting->type == 'text') {
            $setting->update([
                'value' => $data['text'],
            ]);
        }
        if($setting->type == 'textarea') {
            $setting->update([
                'value' => $data['textarea'],
            ]);
        }
        if($setting->type == 'editor') {
            $setting->update([
                'value' => $data['editor'],
            ]);
        }

        return $setting->update();
    }

    public function deleteSetting(Setting $setting): ?bool
    {
        $setting->delete();

        return true;
    }
}
