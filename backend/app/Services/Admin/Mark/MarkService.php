<?php

namespace App\Services\Admin\Mark;

use App\Contracts\HasLangContract;
use App\Models\Mark;
use App\Models\Translate;
use App\Services\FileService;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class MarkService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
    ];

    public function getMarks(): LengthAwarePaginator
    {
        $marks = Mark::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $marks;
    }

    public function createMark(array $data): ?bool
    {
        $mark = new Mark();

        foreach ($this->translationFields as $field) {
            $mark->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        if (isset($data['image'])) {
            $mark->image = (new FileService())->saveFile($data['image'], Mark::IMAGE_PATH);
        }

        $mark->is_published = $data['is_published'];
        $mark->position = $data['position'];

        return $mark->save();
    }

    public function updateMark(Mark $mark, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($mark->{$field . '_tr'}) {
                $mark->{$field}->update($data[$field]);
            } else {
                $mark->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        if (isset($data['image'])) {
            $mark->image = (new FileService())->saveFile($data['image'], Mark::IMAGE_PATH, $mark->image);
        }

        $mark->is_published = $data['is_published'];
        $mark->position = $data['position'];

        return $mark->update();
    }

    public function deleteMark(Mark $mark): ?bool
    {
        $mark->delete();

        if ($mark->title_tr) {
            Translate::find($mark->title_tr)->delete();
        }

        if ($mark->image) {
            (new FileService())->deleteFile($mark->image, Mark::IMAGE_PATH);
        }

        return true;
    }

    public function updateMarkPosition(array $data, string $id): int|bool
    {
        $mark = Mark::find($id);
        $mark->position = $data['position'];
        $mark->update();

        return $id;
    }
}
