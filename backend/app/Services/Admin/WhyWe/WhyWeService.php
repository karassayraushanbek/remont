<?php

namespace App\Services\Admin\WhyWe;

use App\Contracts\HasLangContract;
use App\Models\WhyWe;
use App\Models\Translate;
use App\Services\FileService;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class WhyWeService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        'description',
    ];

    public function getWhyWes(): LengthAwarePaginator
    {
        $whyWes = WhyWe::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $whyWes;
    }

    public function createWhyWe(array $data): ?bool
    {
        $whyWe = new WhyWe();

        foreach ($this->translationFields as $field) {
            $whyWe->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        if (isset($data['image'])) {
            $whyWe->image = (new FileService())->saveFile($data['image'], WhyWe::IMAGE_PATH);
        }

        $whyWe->is_published = $data['is_published'];
        $whyWe->position = $data['position'];

        return $whyWe->save();
    }

    public function updateWhyWe(WhyWe $whyWe, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($whyWe->{$field . '_tr'}) {
                $whyWe->{$field}->update($data[$field]);
            } else {
                $whyWe->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        if (isset($data['image'])) {
            $whyWe->image = (new FileService())->saveFile($data['image'], WhyWe::IMAGE_PATH, $whyWe->image);
        }

        $whyWe->is_published = $data['is_published'];
        $whyWe->position = $data['position'];

        return $whyWe->update();
    }

    public function deleteWhyWe(WhyWe $whyWe): ?bool
    {
        $whyWe->delete();

        if ($whyWe->title_tr) {
            Translate::find($whyWe->title_tr)->delete();
        }

        if ($whyWe->description_tr) {
            Translate::find($whyWe->description_tr)->delete();
        }

        if ($whyWe->image) {
            (new FileService())->deleteFile($whyWe->image, WhyWe::IMAGE_PATH);
        }

        return true;
    }

    public function updateWhyWePosition(array $data, string $id): int|bool
    {
        $whyWe = WhyWe::find($id);
        $whyWe->position = $data['position'];
        $whyWe->update();

        return $id;
    }
}
