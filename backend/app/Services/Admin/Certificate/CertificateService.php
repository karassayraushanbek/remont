<?php

namespace App\Services\Admin\Certificate;

use App\Contracts\HasLangContract;
use App\Traits\HasLangTrait;
use App\Models\Certificate;
use App\Models\Translate;
use App\Services\FileService;
use Illuminate\Pagination\LengthAwarePaginator;

class CertificateService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
    ];

    public function getCertificates(): LengthAwarePaginator
    {
        $certificates = Certificate::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $certificates;
    }

    public function createCertificate(array $data): ?bool
    {
        $certificate = new Certificate();

        foreach ($this->translationFields as $field) {
            $certificate->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        if (isset($data['image'])) {
            $certificate->image = (new FileService())->saveFile($data['image'], Certificate::IMAGE_PATH);
        }

        $certificate->is_published = $data['is_published'];
        $certificate->position = $data['position'];

        return $certificate->save();
    }

    public function updateCertificate(Certificate $certificate, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($certificate->{$field . '_tr'}) {
                $certificate->{$field}->update($data[$field]);
            } else {
                $certificate->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        if (isset($data['image'])) {
            $certificate->image = (new FileService())->saveFile($data['image'], Certificate::IMAGE_PATH, $certificate->image);
        }

        $certificate->is_published = $data['is_published'];
        $certificate->position = $data['position'];

        return $certificate->update();
    }

    public function deleteCertificate(Certificate $certificate): ?bool
    {
        $certificate->delete();

        if ($certificate->title_tr) {
            Translate::find($certificate->title_tr)->delete();
        }

        if ($certificate->image) {
            (new FileService())->deleteFile($certificate->image, Certificate::IMAGE_PATH);
        }

        return true;
    }

    public function updateCertificatePosition(array $data, string $id): int|bool
    {
        $certificate = Certificate::find($id);
        $certificate->position = $data['position'];
        $certificate->update();

        return $id;
    }
}
