<?php

namespace App\Services\Admin\TagMaster;

use App\Contracts\HasLangContract;
use App\Models\TagMaster;
use App\Models\Translate;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class TagMasterService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
    ];

    public function getTagMasters(): LengthAwarePaginator
    {
        $tagMasters = TagMaster::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $tagMasters;
    }

    public function getTagMasterCollection(): Collection
    {
        $tagMasters = TagMaster::orderBy('position', 'ASC')
            ->latest()
            ->get();

        return $tagMasters;
    }

    public function createTagMaster(array $data): ?bool
    {
        $tagMaster = new TagMaster();

        foreach ($this->translationFields as $field) {
            $tagMaster->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        $tagMaster->is_published = $data['is_published'];
        $tagMaster->position = $data['position'];

        return $tagMaster->save();
    }

    public function updateTagMaster(TagMaster $tagMaster, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($tagMaster->{$field . '_tr'}) {
                $tagMaster->{$field}->update($data[$field]);
            } else {
                $tagMaster->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        $tagMaster->is_published = $data['is_published'];
        $tagMaster->position = $data['position'];

        return $tagMaster->update();
    }

    public function deleteTagMaster(TagMaster $tagMaster): ?bool
    {
        $tagMaster->delete();

        if ($tagMaster->title_tr) {
            Translate::find($tagMaster->title_tr)->delete();
        }

        return true;
    }

    public function updateTagMasterPosition(array $data, string $id): int|bool
    {
        $tagMaster = TagMaster::find($id);
        $tagMaster->position = $data['position'];
        $tagMaster->update();

        return $id;
    }
}
