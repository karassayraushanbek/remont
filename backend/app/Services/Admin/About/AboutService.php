<?php

namespace App\Services\Admin\About;

use App\Contracts\HasLangContract;
use App\Models\Translate;
use App\Models\About;
use App\Services\FileService;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class AboutService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        'description',
    ];

    public function getAbouts(): LengthAwarePaginator
    {
        $abouts = About::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $abouts;
    }

    public function createAbout(array $data): ?bool
    {
        $about = new About();

        foreach ($this->translationFields as $field) {
            $about->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        if (isset($data['image'])) {
            $about->image = (new FileService())->saveFile($data['image'], About::IMAGE_PATH);
        }

        $about->is_published = $data['is_published'];
        $about->position = $data['position'];

        return $about->save();
    }

    public function updateAbout(About $about, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($about->{$field . '_tr'}) {
                $about->{$field}->update($data[$field]);
            } else {
                $about->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        if (isset($data['image'])) {
            $about->image = (new FileService())->saveFile($data['image'], About::IMAGE_PATH, $about->image);
        }

        $about->is_published = $data['is_published'];
        $about->position = $data['position'];

        return $about->update();
    }

    public function deleteAbout(About $about): ?bool
    {
        $about->delete();

        if ($about->title_tr) {
            Translate::find($about->title_tr)->delete();
        }

        if ($about->description_tr) {
            Translate::find($about->description_tr)->delete();
        }

        if ($about->image) {
            (new FileService())->deleteFile($about->image, About::IMAGE_PATH);
        }

        return true;
    }

    public function updateAboutPosition(array $data, string $id): int|bool
    {
        $about = About::find($id);
        $about->position = $data['position'];
        $about->update();

        return $id;
    }
}
