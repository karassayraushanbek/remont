<?php

namespace App\Services\Admin\Admin;

use App\Models\Admin;
use App\Events\Admin\AdminEvent;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AdminService
{
    public $perPage = 25;

    public function getAdmins(): LengthAwarePaginator
    {
        return Admin::where('id', '!=', auth()->user()->id)->latest()->paginate($this->perPage);
    }

    public function getAdminRoles(Admin $admin): string
    {
        return $admin->roles->pluck('name','name')->first();
    }

    public function createAdmin(array $data, string $adminEmail): ?bool
    {
        $subject = "Создан Администратор для сайта ";
        $password = $data['password'];

        $admin = new Admin();

        $admin->name = $data['name'];
        $admin->email = $data['email'];
        $admin->password = Hash::make($password);

        $admin->save();

        $admin->assignRole($data['roles']);

        event(new AdminEvent($adminEmail, $subject, $admin, $password));

        return true;
    }

    public function updateAdmin(Admin $admin, array $data, string $adminEmail): ?string
    {
        $subject = "Обновлен Администратор для сайта ";
        $password = $data['password'];
        $m = '';

        if (isset($password)) {
            $m = ' Пароль отправлен на эту почту ' . $adminEmail;
            $admin->password = Hash::make($password);
            event(new AdminEvent($adminEmail, $subject, $admin, $password));
        }

        $admin->name = $data['name'];
        $admin->email = $data['email'];

        $admin->update();

        DB::table('model_has_roles')->where('model_id', $admin->id)->delete();
        $admin->assignRole($data['roles']);

        return $m;
    }

    public function deleteAdmin(Admin $admin): ?bool
    {
        $admin->delete();

        return true;
    }

    public function updateAdminStatus(string $id, $data): ?bool
    {
        $admin = Admin::find($id);

        $admin->update(['status' => $data['status']]);

        return true;
    }
}
