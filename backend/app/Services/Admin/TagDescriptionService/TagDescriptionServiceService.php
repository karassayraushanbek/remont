<?php

namespace App\Services\Admin\TagDescriptionService;

use App\Contracts\HasLangContract;
use App\Models\TagDescriptionService;
use App\Models\Translate;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class TagDescriptionServiceService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
    ];

    public function getTagDescriptionServices(): LengthAwarePaginator
    {
        $tagDescriptionServices = TagDescriptionService::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $tagDescriptionServices;
    }

    public function getTagDescriptionServiceCollection(): Collection
    {
        $tagDescriptionService = TagDescriptionService::orderBy('position', 'ASC')
            ->latest()
            ->get();

        return $tagDescriptionService;
    }

    public function createTagDescriptionService(array $data): ?bool
    {
        $tagDescriptionService = new TagDescriptionService();

        foreach ($this->translationFields as $field) {
            $tagDescriptionService->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        $tagDescriptionService->is_published = $data['is_published'];
        $tagDescriptionService->position = $data['position'];

        return $tagDescriptionService->save();
    }

    public function updateTagDescriptionService(TagDescriptionService $tagDescriptionService, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($tagDescriptionService->{$field . '_tr'}) {
                $tagDescriptionService->{$field}->update($data[$field]);
            } else {
                $tagDescriptionService->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        $tagDescriptionService->is_published = $data['is_published'];
        $tagDescriptionService->position = $data['position'];

        return $tagDescriptionService->update();
    }

    public function deleteTagDescriptionService(TagDescriptionService $tagDescriptionService): ?bool
    {
        $tagDescriptionService->delete();

        Translate::find($tagDescriptionService->title_tr)->delete();

        return true;
    }

    public function updateTagDescriptionServicePosition(array $data, string $id): int|bool
    {
        $tagDescriptionService = TagDescriptionService::find($id);
        $tagDescriptionService->position = $data['position'];
        $tagDescriptionService->update();

        return $id;
    }
}
