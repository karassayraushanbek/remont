<?php

namespace App\Services\Admin\Blog;

use App\Contracts\HasLangContract;
use App\Models\Blog;
use App\Models\Translate;
use App\Services\FileService;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class BlogService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        'description',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    public function getBlogs(): LengthAwarePaginator
    {
        $blogs = Blog::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $blogs;
    }

    public function getBlogCollection(): Collection
    {
        $blogs = Blog::orderBy('position', 'ASC')
            ->latest()
            ->get();

        return $blogs;
    }

    public function createBlog(array $data): ?bool
    {
        $blog = new Blog();

        foreach ($this->translationFields as $field) {
            $blog->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        if (isset($data['image'])) {
            $blog->image = (new FileService())->saveFile($data['image'], Blog::IMAGE_PATH);
        }

        $blog->tag_id = $data['tag_id'];
        $blog->read = $data['read'];
        $blog->slug = Str::slug($data['title']['ru']);
        $blog->is_published = $data['is_published'];
        $blog->position = $data['position'];

        return $blog->save();
    }

    public function updateBlog(Blog $blog, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($blog->{$field . '_tr'}) {
                $blog->{$field}->update($data[$field]);
            } else {
                $blog->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        if (isset($data['image'])) {
            $blog->image = (new FileService())->saveFile($data['image'], Blog::IMAGE_PATH, $blog->image);
        }

        $blog->tag_id = $data['tag_id'];
        $blog->read = $data['read'];
        $blog->slug = Str::slug($data['title']['ru']);
        $blog->is_published = $data['is_published'];
        $blog->position = $data['position'];

        return $blog->update();
    }

    public function deleteBlog(Blog $blog): ?bool
    {
        $blog->delete();

        if ($blog->image) {
            (new FileService())->deleteFile($blog->image, Blog::IMAGE_PATH);
        }

        if ($blog->title_tr) {
            Translate::find($blog->title_tr)->delete();
        }

        if ($blog->description_tr) {
            Translate::find($blog->description_tr)->delete();
        }

        if ($blog->meta_title_tr) {
            Translate::find($blog->meta_title_tr)->delete();
        }

        if ($blog->meta_description_tr) {
            Translate::find($blog->meta_description_tr)->delete();
        }

        if ($blog->meta_keywords_tr) {
            Translate::find($blog->meta_keywords_tr)->delete();
        }

        return true;
    }

    public function updateBlogPosition(array $data, string $id): int|bool
    {
        $blog = Blog::find($id);
        $blog->position = $data['position'];
        $blog->update();

        return $id;
    }
}
