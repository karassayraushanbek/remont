<?php

namespace App\Services\Admin\User;

use App\Events\User\UserEvent;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public $perPage = 25;

    public function getUsers(): LengthAwarePaginator|array
    {
        if (!empty(request()->get('search'))) {
            $searchParams = request()->get('search');

            $users = User::query();

            if (!empty($searchParams['name']) || isset($searchParams['status'])) {
                $where = [];
                foreach ($searchParams as $name => $param) {
                    $where[] = [$name, 'LIKE', '%' . $param . '%'];
                }

                $searchCatalog = $users->where($where)->get()->keyBy('id')->keys()->toArray();
            }

            if (!empty($searchCatalog)) {
                $users = User::whereIn('id', $searchCatalog)
                    ->latest()
                    ->paginate($this->perPage)
                    ->appends(request()->query());
            } else {
                $users = [];
            }
        } else {
            $users = User::latest()
                ->paginate($this->perPage)
                ->appends(request()->query());
        }

        return $users;
    }

    public function createUser(array $data, string $adminEmail): ?bool
    {
        $subject = "Пользователь создан Администратором для сайта ";
        $password = $data['password'];

        $user = new User();

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($password);

        event(new UserEvent($adminEmail, $subject, $user, $password));

        return $user->save();
    }

    public function updateUser(User $user, array $data, string $adminEmail): ?string
    {
        $subject = "Пользователь обновлен Администратором для сайта ";
        $password = $data['password'];
        $m = '';

        if (isset($password)) {
            $m = ' Пароль отправлен на эту почту ' . $adminEmail;
            $user->password = Hash::make($password);
            event(new UserEvent($adminEmail, $subject, $user, $password));
        }

        $user->name = $data['name'];
        $user->email = $data['email'];

        $user->update();

        return $m;
    }

    public function deleteUser(User $user): ?bool
    {
        $user->delete();

        return true;
    }

    public function updateUserStatus(string $id, $data): ?bool
    {
        $user = User::find($id);

        $user->update(['status' => $data['status']]);

        return true;
    }
}
