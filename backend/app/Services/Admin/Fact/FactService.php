<?php

namespace App\Services\Admin\Fact;

use App\Contracts\HasLangContract;
use App\Models\Fact;
use App\Models\Translate;
use App\Services\FileService;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class FactService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        'alt_title',
        'description',
    ];

    public function getFacts(): LengthAwarePaginator
    {
        $facts = Fact::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $facts;
    }

    public function createFact(array $data): ?bool
    {
        $fact = new Fact();

        foreach ($this->translationFields as $field) {
            $fact->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        if (isset($data['image'])) {
            $fact->image = (new FileService())->saveFile($data['image'], Fact::IMAGE_PATH);
        }

        $fact->number = $data['number'];
        $fact->is_published = $data['is_published'];
        $fact->position = $data['position'] ?? 0;

        return $fact->save();
    }

    public function updateFact(Fact $fact, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($fact->{$field . '_tr'}) {
                $fact->{$field}->update($data[$field]);
            } else {
                $fact->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        if (isset($data['image'])) {
            $fact->image = (new FileService())->saveFile($data['image'], Fact::IMAGE_PATH, $fact->image);
        }

        $fact->number = $data['number'];
        $fact->is_published = $data['is_published'];
        $fact->position = $data['position'] ?? 0;

        return $fact->update();
    }

    public function deleteFact(Fact $fact): ?bool
    {
        $fact->delete();

        foreach ($this->translationFields as $field) {
            if ($fact->{$field . '_tr'}) {
                Translate::find($fact->{$field . '_tr'})->delete();
            }
        }

        if ($fact->image) {
            (new FileService())->deleteFile($fact->image, Fact::IMAGE_PATH);
        }

        return true;
    }

    public function updateFactPosition(array $data, string $id): int|bool
    {
        $fact = Fact::find($id);
        $fact->position = $data['position'];
        $fact->update();

        return $id;
    }
}
