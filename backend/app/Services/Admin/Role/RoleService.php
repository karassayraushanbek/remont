<?php

namespace App\Services\Admin\Role;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class RoleService
{
    public $perPage = 25;
    public $permissionIdArray = [10,11,12,13,14,15,16,17,19,21,23,25,50,51,53,63,64,65,95,96,97];

    public function getRoles(): LengthAwarePaginator
    {
        return Role::orderBy('id','DESC')->paginate($this->perPage);
    }

    public function getRoleNamesNames(): array
    {
        return Role::pluck('name','name')->all();
    }

    public function getRoleNames(): array
    {
        return Role::pluck('name')->all();
    }

    public function getPermissions(): Collection
    {
        return Permission::whereNotIn('id', $this->permissionIdArray)->get();
    }

    public function getRolePermissionsByRoleId(Role $role): Collection
    {
        return Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->whereNotIn('id', $this->permissionIdArray)
            ->where("role_has_permissions.role_id", $role->id)
            ->get();
    }

    public function getRolePermissionsByRoleIdAll(Role $role): array
    {
        return DB::table("role_has_permissions")
            ->where("role_has_permissions.role_id", $role->id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
    }

    public function createRole(array $data): ?bool
    {
        $role = Role::create(['name' => $data['name']]);
        $role->syncPermissions($data['permission']);

        return true;
    }

    public function updateRole(Role $role, array $data): ?bool
    {
        $role->name = $data['name'];
        $role->save();

        $role->syncPermissions($data['permission']);

        return true;
    }

    public function deleteRole(Role $role): ?bool
    {
        DB::table("roles")->where('id', $role->id)->delete();

        return true;
    }
}
