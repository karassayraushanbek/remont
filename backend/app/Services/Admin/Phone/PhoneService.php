<?php

namespace App\Services\Admin\Phone;

use App\Models\Phone;
use Illuminate\Pagination\LengthAwarePaginator;

class PhoneService
{
    public $perPage = 25;

    public function getPhones(): LengthAwarePaginator
    {
        $phones = Phone::latest()->paginate($this->perPage);

        return $phones;
    }

    public function createPhone(array $data): ?bool
    {
        $phone = new Phone();

        $phone->phone = $data['phone'];

        return $phone->save();
    }

    public function updatePhone(Phone $phone, array $data): ?bool
    {
        $phone->phone = $data['phone'];

        return $phone->update();
    }

    public function deletePhone(Phone $phone): ?bool
    {
        $phone->delete();

        return true;
    }
}
