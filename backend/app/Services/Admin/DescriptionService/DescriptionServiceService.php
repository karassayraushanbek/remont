<?php

namespace App\Services\Admin\DescriptionService;

use App\Contracts\HasLangContract;
use App\Models\DescriptionService;
use App\Models\Translate;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class DescriptionServiceService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        'description',
    ];

    public function getDescriptionServices(): LengthAwarePaginator
    {
        $descriptionServices = DescriptionService::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $descriptionServices;
    }

    public function createDescriptionService(array $data): ?bool
    {
        $descriptionService = new DescriptionService();

        foreach ($this->translationFields as $field) {
            $descriptionService->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        $descriptionService->tag_description_service_id = $data['tag_description_service_id'];
        $descriptionService->phone = $data['phone'];
        $descriptionService->is_published = $data['is_published'];
        $descriptionService->position = $data['position'];

        return $descriptionService->save();
    }

    public function updateDescriptionService(DescriptionService $descriptionService, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($descriptionService->{$field . '_tr'}) {
                $descriptionService->{$field}->update($data[$field]);
            } else {
                $descriptionService->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        $descriptionService->tag_description_service_id = $data['tag_description_service_id'];
        $descriptionService->phone = $data['phone'];
        $descriptionService->is_published = $data['is_published'];
        $descriptionService->position = $data['position'];

        return $descriptionService->update();
    }

    public function deleteDescriptionService(DescriptionService $descriptionService): ?bool
    {
        $descriptionService->delete();

        if ($descriptionService->title_tr) {
            Translate::find($descriptionService->title_tr)->delete();
        }

        if ($descriptionService->description_tr) {
            Translate::find($descriptionService->description_tr)->delete();
        }

        return true;
    }

    public function updateDescriptionServicePosition(array $data, string $id): int|bool
    {
        $descriptionService = DescriptionService::find($id);
        $descriptionService->position = $data['position'];
        $descriptionService->update();

        return $id;
    }
}
