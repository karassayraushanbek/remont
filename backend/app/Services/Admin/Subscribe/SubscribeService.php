<?php

namespace App\Services\Admin\Subscribe;

use App\Models\Subscribe;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class SubscribeService
{
    public $perPage = 25;

    public function getSubscribes(): LengthAwarePaginator
    {
        $subscribes = Subscribe::latest()->paginate($this->perPage);

        return $subscribes;
    }

    public function getSubscribesCountToday(Carbon $today): int
    {
        return Subscribe::whereDate('created_at', $today)->get()->count();
    }

    public function createSubscribe(array $data): ?bool
    {
        $subscribe = new Subscribe();

        $subscribe->email = $data['email'];

        return $subscribe->save();
    }

    public function updateSubscribe(Subscribe $subscribe, array $data): ?bool
    {
        $subscribe->email = $data['email'];

        return $subscribe->update();
    }

    public function deleteSubscribe(Subscribe $subscribe): ?bool
    {
        $subscribe->delete();

        return true;
    }
}
