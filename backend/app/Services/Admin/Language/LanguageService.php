<?php

namespace App\Services\Admin\Language;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Language;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LanguageService
{
    public $perPage = 25;

    public function getLanguages(): LengthAwarePaginator
    {
        return Language::paginate($this->perPage);
    }

    public function createLanguage(array $data): ?bool
    {
        $language = new Language();

        $language->slug = $data['slug'];
        $language->name = $data['name'];

        Schema::table('translates', function (Blueprint $table) use ($data) {
            $table->longText($data['slug'])->nullable();
        });

        return $language->save();
    }

    public function updateLanguage(Language $language, array $data): ?bool
    {
        $language->slug = $data['slug'];
        $language->name = $data['name'];

        return $language->update();
    }

    public function deleteLanguage(Language $language): ?bool
    {
        $language->delete();

        Schema::table('translates', function (Blueprint $table) use ($language) {
            $table->dropColumn($language['slug']);
        });

        return true;
    }
}
