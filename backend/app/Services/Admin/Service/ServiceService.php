<?php

namespace App\Services\Admin\Service;

use App\Contracts\HasLangContract;
use App\Models\Service;
use App\Models\Translate;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;

class ServiceService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        'description',
    ];

    public function getServices(): LengthAwarePaginator
    {
        $services = Service::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $services;
    }

    public function createService(array $data): ?bool
    {
        $service = new Service();

        foreach ($this->translationFields as $field) {
            $service->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        $service->phone = $data['phone'];
        $service->is_published = $data['is_published'];
        $service->position = $data['position'];

        return $service->save();
    }

    public function updateService(Service $service, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($service->{$field . '_tr'}) {
                $service->{$field}->update($data[$field]);
            } else {
                $service->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        $service->phone = $data['phone'];
        $service->is_published = $data['is_published'];
        $service->position = $data['position'];

        return $service->update();
    }

    public function deleteService(Service $service): ?bool
    {
        $service->delete();

        if ($service->title_tr) {
            Translate::find($service->title_tr)->delete();
        }

        if ($service->description_tr) {
            Translate::find($service->description_tr)->delete();
        }

        return true;
    }

    public function updateServicePosition(array $data, string $id): int|bool
    {
        $service = Service::find($id);
        $service->position = $data['position'];
        $service->update();

        return $id;
    }
}
