<?php

namespace App\Services\Admin\Page;

use App\Contracts\HasLangContract;
use App\Models\Page;
use App\Models\Translate;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

class PageService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        //'description',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    public function getPages(): LengthAwarePaginator
    {
        $pages = Page::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $pages;
    }

    public function createPage(array $data): ?bool
    {
        $page = new Page();

        foreach ($this->translationFields as $field) {
            $page->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        $page->menu_id = $data['menu_id'];
        $page->slug = Str::slug($data['title']['ru']);
        $page->is_published = $data['is_published'];
        //$page->position = $data['position'];

        return $page->save();
    }

    public function updatePage(Page $page, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($page->{$field . '_tr'}) {
                $page->{$field}->update($data[$field]);
            } else {
                $page->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        //$page->menu_id = $data['menu_id'];
        //$page->slug = Str::slug($data['title']['ru']);
        $page->is_published = $data['is_published'];
        //$page->position = $data['position'];

        return $page->update();
    }

    public function deletePage(Page $page): ?bool
    {
        $page->delete();

        if ($page->title_tr) {
            Translate::find($page->title_tr)->delete();
        }

        if ($page->description_tr) {
            Translate::find($page->description_tr)->delete();
        }

        if ($page->meta_title_tr) {
            Translate::find($page->meta_title_tr)->delete();
        }

        if ($page->meta_description_tr) {
            Translate::find($page->meta_description_tr)->delete();
        }

        if ($page->meta_keywords_tr) {
            Translate::find($page->meta_keywords_tr)->delete();
        }

        return true;
    }

    public function updatePagePosition(array $data, string $id): int|bool
    {
        $page = Page::find($id);
        $page->position = $data['position'];
        $page->update();

        return $id;
    }
}
