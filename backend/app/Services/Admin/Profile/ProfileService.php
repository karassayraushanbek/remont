<?php

namespace App\Services\Admin\Profile;

use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use App\Events\Admin\AdminEvent;

class ProfileService
{
    public function updateProfile(Admin $user, array $data): ?string
    {
        $m = '';

        if(isset($data['password'])) {
            $email = $user->email;
            $subject = "Администратор обновил свои пароль для сайта ";
            $password = $data['password'];

            $m = ' Пароль отправлен на эту почту ' . $email;
            $user->update(['password' => Hash::make($password)]);

            event(new AdminEvent($email, $subject, $user, $password));
        }

        $user->name = $data['name'];
        $user->email = $data['email'];

        $user->update();

        return $m;
    }
}
