<?php

namespace App\Services\Admin\Email;

use App\Models\Email;
use Illuminate\Pagination\LengthAwarePaginator;

class EmailService
{
    public $perPage = 25;

    public function getEmails(): LengthAwarePaginator
    {
        $emails = Email::latest()->paginate($this->perPage);

        return $emails;
    }

    public function createEmail(array $data): ?bool
    {
        $email = new Email();

        $email->email = $data['email'];

        return $email->save();
    }

    public function updateEmail(Email $email, array $data): ?bool
    {
        $email->email = $data['email'];

        return $email->update();
    }

    public function deleteEmail(Email $email): ?bool
    {
        $email->delete();

        return true;
    }
}
