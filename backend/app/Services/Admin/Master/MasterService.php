<?php

namespace App\Services\Admin\Master;

use App\Contracts\HasLangContract;
use App\Models\Master;
use App\Models\Translate;
use App\Services\FileService;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class MasterService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
        'description',
    ];

    public function getMasters(): LengthAwarePaginator
    {
        $masters = Master::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $masters;
    }

    public function getMasterCollection(): Collection
    {
        $masters = Master::orderBy('position', 'ASC')
            ->latest()
            ->get();

        return $masters;
    }

    public function createMaster(array $data): ?bool
    {
        $master = new Master();

        foreach ($this->translationFields as $field) {
            $master->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        if (isset($data['image'])) {
            $master->image = (new FileService())->saveFile($data['image'], Master::IMAGE_PATH);
        }

        $master->name = $data['name'];
        $master->reviews = $data['reviews'];
        $master->is_published = $data['is_published'];
        $master->position = $data['position'];

        if(!empty($data['tags'])) {
            $master->tagMasters()->attach($data['tags']);
        }

        return $master->save();
    }

    public function updateMaster(Master $master, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($master->{$field . '_tr'}) {
                $master->{$field}->update($data[$field]);
            } else {
                $master->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        if (isset($data['image'])) {
            $master->image = (new FileService())->saveFile($data['image'], Master::IMAGE_PATH, $master->image);
        }

        $master->name = $data['name'];
        $master->reviews = $data['reviews'];
        $master->is_published = $data['is_published'];
        $master->position = $data['position'];

        if(!empty($data['tags'])) {
            $master->tagMasters()->sync($data['tags']);
        }

        return $master->update();
    }

    public function deleteMaster(Master $master): ?bool
    {
        $master->delete();

        if ($master->image) {
            (new FileService())->deleteFile($master->image, Master::IMAGE_PATH);
        }

        if ($master->title_tr) {
            Translate::find($master->title_tr)->delete();
        }

        if ($master->description_tr) {
            Translate::find($master->description_tr)->delete();
        }

        $master->tagMasters()->detach();

        return true;
    }

    public function updateMasterPosition(array $data, string $id): int|bool
    {
        $master = Master::find($id);
        $master->position = $data['position'];
        $master->update();

        return $id;
    }
}
