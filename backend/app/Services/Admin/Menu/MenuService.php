<?php

namespace App\Services\Admin\Menu;

use App\Contracts\HasLangContract;
use App\Models\Menu;
use App\Models\Translate;
use App\Traits\HasLangTrait;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class MenuService implements HasLangContract
{
    use HasLangTrait;

    public $perPage = 25;

    public $translationFields = [
        'title',
    ];

    public function getMenus(): LengthAwarePaginator
    {
        $menus = Menu::orderBy('position', 'ASC')
            ->latest()
            ->paginate($this->perPage);

        return $menus;
    }

    public function getMenuCollection(): Collection
    {
        $menus = Menu::orderBy('position', 'ASC')
            ->latest()
            ->get();

        return $menus;
    }

    public function createMenu(array $data): ?bool
    {
        $menu = new Menu();

        foreach ($this->translationFields as $field) {
            $menu->{$field . '_tr'} = Translate::create($data[$field])->id;
        }

        $menu->is_published = $data['is_published'];
        $menu->position = $data['position'];

        return $menu->save();
    }

    public function updateMenu(Menu $menu, array $data): ?bool
    {
        foreach ($this->translationFields as $field) {
            if ($menu->{$field . '_tr'}) {
                $menu->{$field}->update($data[$field]);
            } else {
                $menu->{$field . '_tr'} = Translate::create($data[$field])->id;
            }
        }

        $menu->is_published = $data['is_published'];
        $menu->position = $data['position'];

        return $menu->update();
    }

    public function deleteMenu(Menu $menu): ?bool
    {
        $menu->delete();

        if ($menu->title_tr) {
            Translate::find($menu->title_tr)->delete();
        }

        return true;
    }

    public function updateMenuPosition(array $data, string $id): int|bool
    {
        $menu = Menu::find($id);
        $menu->position = $data['position'];
        $menu->update();

        return $id;
    }
}
