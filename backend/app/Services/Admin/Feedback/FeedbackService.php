<?php

namespace App\Services\Admin\Feedback;

use App\Models\Feedback;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class FeedbackService
{
    public $perPage = 25;

    public function getFeedbacks(): LengthAwarePaginator
    {
        $feedbacks = Feedback::latest()->paginate($this->perPage);

        return $feedbacks;
    }

    public function getFeedbackCountToday(Carbon $today): int
    {
        return Feedback::whereDate('created_at', $today)->get()->count();
    }

    public function createFeedback(array $data): ?bool
    {
        $feedback = new Feedback();

        $feedback->name = $data['name'];
        $feedback->phone = $data['phone'];

        return $feedback->save();
    }

    public function updateFeedback(Feedback $feedback, array $data): ?bool
    {
        $feedback->name = $data['name'];
        $feedback->phone = $data['phone'];

        return $feedback->update();
    }

    public function deleteFeedback(Feedback $feedback): ?bool
    {
        $feedback->delete();

        return true;
    }
}
