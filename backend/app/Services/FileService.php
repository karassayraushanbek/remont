<?php

namespace App\Services;

use App\Contracts\FileServiceContract;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class FileService implements FileServiceContract
{
    public function deleteFile(string $name, string $path): mixed
    {
        if ($name && Storage::disk('custom')->exists($path . '/' . $name)) {
            Storage::disk('custom')->delete($path . '/' . $name);
        }
        return null;
    }

    public function saveFile(object $file, string $path, string $oldFileName = null): string
    {
        $directory = storage_path('app/public/uploads/' . $path);
        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0755, true);
        }

        $fileName = $this->generateUniqueFileName($file, $path);

        if ($file->getClientOriginalExtension() == 'mp4' || $file->getClientOriginalExtension() == 'gif' || $file->getClientOriginalExtension() == 'svg') {
            Storage::disk('custom')->putFileAs($path, $file, $fileName);
        } else {
            $this->resizeAndSaveImage($file, $fileName, $path);
        }

        if ($oldFileName) {
            $this->deleteFile($oldFileName, $path);
        }

        return $fileName;
    }

    private function generateUniqueFileName(object $file, string $path): string
    {
        $fileName = hexdec(uniqid()) . $file->getClientOriginalName();

        while (Storage::disk('custom')->exists($path . "/$fileName")) {
            $fileName = $this->makeUniqueFileName($fileName);
        }

        return $fileName;
    }

    private function makeUniqueFileName(string $fileName): string
    {
        $nameParts = pathinfo($fileName);
        $newFileName = $nameParts['filename'] . "_" . rand(1, 99) . '.' . $nameParts['extension'];
        return $this->generateUniqueFileName($newFileName);
    }

    private function resizeAndSaveImage(object $file, string $fileName, string $path): void
    {
        $image = Image::make($file);
        if ($file->getSize() > 2097152) {
            $newWidth = $image->width() * 0.1;
            $newHeight = $image->height() * 0.1;
            $image->resize($newWidth, $newHeight);
        }
        $image->save(storage_path("app/public/uploads/{$path}/{$fileName}"), 70);
    }
}
