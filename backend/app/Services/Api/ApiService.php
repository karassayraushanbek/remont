<?php

namespace App\Services\Api;

use App\Models\Email;
use App\Models\Menu;
use App\Models\Phone;
use App\Models\Setting;
use Illuminate\Database\Eloquent\Collection;

class ApiService
{
    public function getMenus(): Collection
    {
        $menus = Menu::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();

        return $menus;
    }

    public function getSetting($key): Setting|null
    {
        return Setting::where('key', $key)->first() ?? null;
    }

    public function getPhones(): Collection
    {
        $phones = Phone::latest()->get();

        return $phones;
    }

    public function getEmails(): Collection
    {
        $emails = Email::latest()->get();

        return $emails;
    }
}
