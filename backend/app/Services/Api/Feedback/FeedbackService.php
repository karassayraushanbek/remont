<?php

namespace App\Services\Api\Feedback;

use App\Models\Feedback;
use App\Models\Subscribe;

class FeedbackService
{
    public $token = "6925144674:AAHuy-Xqd_vtHHJ3XHn8WwkvmLcx2Wfy1pA";
    public $chatId = "-1002001305942";

    public function subscribe(array $data): ?bool
    {
        $site = env('APP_URL');
        $txt = "<b>Email:</b> ".$data['email']."%0A<b>Откуда:</b> ".$site;
        $url = "https://api.telegram.org/bot".$this->token."/sendMessage?chat_id=".$this->chatId."&parse_mode=html&text=".$txt;

        $subscribe = new Subscribe();

        $subscribe->email = $data['email'];

        if ($subscribe->save()) {
            fopen($url,"r");
        }

        return true;
    }

    public function feedback(array $data): ?bool
    {
        $site = env('APP_URL');
        if (!(new AmoCRMService())->send($site, $data['name'], $data['phone'])) {
          return false;
        }

        $txt = "<b>Имя:</b> ".$data['name']."%0A<b>Номер телефона:</b> ".$data['phone']."%0A<b>Откуда:</b> ".$site;
        $url = "https://api.telegram.org/bot".$this->token."/sendMessage?chat_id=".$this->chatId."&parse_mode=html&text=".$txt;

        $feedback = new Feedback();

        $feedback->name = $data['name'];
        $feedback->phone = $data['phone'];

        if ($feedback->save()) {
            fopen($url,"r");
        }

        return true;
    }
}
