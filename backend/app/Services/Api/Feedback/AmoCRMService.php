<?php

namespace App\Services\Api\Feedback;

class AmoCRMService
{
  public function send(string $link, string $name, string $phone): ?bool
  {
    $subdomain = env('AMO_SUBDOMAIN');
    $access_token = env('AMO_CLIENT_LONG_KEY');
    $headers = [
      'Authorization: Bearer ' . $access_token,
      'Content-Type: application/json',
    ];

    $data = [
      [
        "name" => "Заявка с сайта",
        "custom_fields_values" => [
          [
            "field_id" => 370967,
            "values" => [
              [
                "value" => $link,
              ]
            ]
          ],
          [
            "field_id" => 370973,
            "values" => [
              [
                "value" => $name,
              ]
            ]
          ],
          [
            "field_id" => 370975,
            "values" => [
              [
                "value" => $phone,
              ]
            ]
          ],
        ],
      ]
    ];

    $url = "https://$subdomain.amocrm.ru/api/v4/leads";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookie.txt');
    curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookie.txt');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    $out = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    $code = (int) $code;

    $errors = [
      400 => 'Bad request',
      401 => 'Unauthorized',
      403 => 'Forbidden',
      404 => 'Not found',
      500 => 'Internal server error',
      502 => 'Bad gateway',
      503 => 'Service unavailable',
    ];

    if ($code < 200 || $code > 204) die( "Error $code. " . (isset($errors[$code]) ? $errors[$code] : 'Undefined error') );

    $response = json_decode($out, true);

    if (isset($response['_embedded']['leads'])) {
      return true;
    } else {
      return false;
    }
  }
}
