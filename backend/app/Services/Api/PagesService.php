<?php

namespace App\Services\Api;

use App\Models\About;
use App\Models\Banner;
use App\Models\Blog;
use App\Models\Certificate;
use App\Models\DescriptionService;
use App\Models\Fact;
use App\Models\Mark;
use App\Models\Master;
use App\Models\Page;
use App\Models\Review;
use App\Models\Service;
use App\Models\Tag;
use App\Models\TagDescriptionService;
use App\Models\WhyWe;
use Illuminate\Support\Collection;
class PagesService
{
    public function getFirstPage(int $id): Page
    {
        return Page::where('id', $id)->first();
    }

    public function getFirstBanner(int $page_id): Banner
    {
        return Banner::where('page_id', $page_id)->first();
    }

    public function getMarks(): Collection
    {
        return Mark::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getWhyWes(): Collection
    {
        return WhyWe::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getServices(): Collection
    {
        return Service::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getAbouts(): Collection
    {
        return About::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getCertificates(): Collection
    {
        return Certificate::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getFacts(): Collection
    {
        return Fact::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getTagDescriptionServices(): Collection
    {
        return TagDescriptionService::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getDescriptionServices(): Collection
    {
        $descriptionService = DescriptionService::query();

        if (!empty(request()->get('tag'))) {
            $descriptionService = $descriptionService->where('tag_description_service_id', request()->get('tag'))
                ->where('is_published', 1)
                ->latest()
                ->get();
        } else {
            $descriptionService = $descriptionService->where('is_published', 1)
                ->orderBy('position', 'ASC')
                ->latest()
                ->get();
        }

        return $descriptionService;
    }

    public function getTagBlogs(): Collection
    {
        return Tag::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getBlogs(): Collection
    {
        $blogs = Blog::query();

        if (!empty(request()->get('tag'))) {
            $blogs = $blogs->where('tag_id', request()->get('tag'))
                ->where('is_published', 1)
                ->latest()
                ->get();
        } else {
            $blogs = Blog::where('is_published', 1)
                ->orderBy('position', 'ASC')
                ->latest()
                ->get();
        }

        return $blogs;
    }

    public function getFirstBlog(int $id): Blog
    {
        return Blog::where('id', $id)->first();
    }

    public function getBlogByTagId(int $tag_id): Collection
    {
        return Blog::where('tag_id', $tag_id)
            ->where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getReviews(): Collection
    {
        return Review::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getReviewsByBlogId(int $blog_id): Collection
    {
        return Review::where('blog_id', $blog_id)
            ->where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }

    public function getMasters(): Collection
    {
        return Master::where('is_published', 1)
            ->orderBy('position', 'ASC')
            ->latest()
            ->get();
    }
}
