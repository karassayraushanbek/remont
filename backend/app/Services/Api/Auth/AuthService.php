<?php

namespace App\Services\Api\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function login(array $credentials): ?string
    {
        return Auth::attempt($credentials) ? auth()->getToken() : null;
    }

    public function respondWithToken(?string $token): ?array
    {
        if (!$token) {
            return ['error' => 'Неавторизованный'];
        }

        return [
            'user' => auth()->user(),
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
    }

    public function registerUser(array $data): ?User
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        return $user;
    }

    public function updateUser(User $user, array $data): ?User
    {
        if (isset($data['name'])) {
            $user->name = $data['name'];
        }

        if (isset($data['email'])) {
            $user->email = $data['email'];
        }

        $user->update();

        return $user;
    }
}
