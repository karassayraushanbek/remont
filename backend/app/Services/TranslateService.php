<?php

namespace App\Services;

use App\Contracts\TranslateServiceContract;
use App\Models\Language;
use App\Models\Translate;

class TranslateService implements TranslateServiceContract
{
    public function getLanguages(): array
    {
        return Language::get()->keyBy('slug')->keys()->toArray();
    }

    public function createTranslation(string $string): Translate
    {
        $languages = $this->getLanguages();
        foreach ($languages as $language) {
            $data[$language] = $string . ' ' . $language;
        }
        return Translate::create($data);
    }

    public function updateTranslation(int $id, string $string = null, string $lang): Translate
    {
        $languages = $this->getLanguages();
        foreach ($languages as $language) {
            if ($language == $lang) {
                $data[$language] = $string;
            }
        }

        Translate::find($id)->update($data);

        return Translate::find($id);
    }

    public function deleteTranslation(int $id): Translate
    {
        $translation = Translate::find($id);

        if ($translation) {
            $translation->delete();
        }

        return $translation;
    }
}
