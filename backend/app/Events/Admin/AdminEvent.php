<?php

namespace App\Events\Admin;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AdminEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $admin;
    public $password;
    public $adminEmail;
    public $subject;

    /**
     * Create a new event instance.
     */
    public function __construct($adminEmail, $subject, $admin, $password)
    {
        $this->admin = $admin;
        $this->password = $password;
        $this->adminEmail = $adminEmail;
        $this->subject = $subject;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('channel-name'),
        ];
    }
}
