<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $lang = $request->lang;

        return [
            'id' => $this->id,
            'number' => $this->number,
            'image' => $this->image_url,
            'alt' => $this->alt_title?->{$lang},
            'title' => $this->title?->{$lang},
            'description' => $this->description?->{$lang},
        ];
    }
}
