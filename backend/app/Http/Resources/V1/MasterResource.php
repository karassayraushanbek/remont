<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MasterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $lang = $request->lang;

        return [
            'id' => $this->id,
            'image' => $this->image_url,
            'name' => $this->name,
            'reviews' => $this->reviews,
            'distance' => $this->title?->{$lang},
            'experience' => $this->description?->{$lang},
            'tags' => TagMasterResource::collection($this->tagMasters),
        ];
    }
}
