<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $lang = $request->lang;

        return [
            'id' => $this->id,
            'blog_id' => $this->blog_id,
            //'image' => $this->image_url,
            'name' => $this->name,
            'description' => $this->description?->{$lang},
            'rating' => $this->rating,
            'date' => $this->created_at?->format('d.m.Y'),
        ];
    }
}
