<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $lang = $request->lang;

        return [
            'id' => $this->id,
            'title' => $this->title?->{$lang},
            'description' => $this->description?->{$lang},
            'phone' => preg_replace("/[^+,0-9]/", '', $this->phone),
        ];
    }
}
