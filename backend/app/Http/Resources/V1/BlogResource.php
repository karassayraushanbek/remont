<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $lang = $request->lang;

        return [
            'id' => $this->id,
            'tag_id' => $this->tag_id,
            'tag' => $this->tag?->title?->{$lang},
            'image' => $this->image_url,
            'title' => $this->title?->{$lang},
            'description' => $this->description?->{$lang},
            'meta_title' => $this->meta_title?->{$lang},
            'meta_description' => $this->meta_description?->{$lang},
            'meta_keywords' => $this->meta_keywords?->{$lang},
            'slug' => $this->slug,
            'read' => $this->read,
            'date' => $this->created_at?->format('d/m/Y'),
        ];
    }
}
