<?php

namespace App\Http\Requests\Admin\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = request()->input('admin_id');

        return [
            'name' => 'nullable',
            'email' => 'required|email|unique:admins,email,'.$id,
            'password' => 'nullable|string|min:5|max:30',
            'roles' => 'nullable|string',
        ];
    }
}
