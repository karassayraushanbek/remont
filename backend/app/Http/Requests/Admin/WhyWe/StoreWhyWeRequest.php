<?php

namespace App\Http\Requests\Admin\WhyWe;

use Illuminate\Foundation\Http\FormRequest;

class StoreWhyWeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'image' => 'required|image|mimes:svg|max:4096',
            'title' => 'array',
            'description' => 'array',
            'title.*' => 'string|nullable',
            'description.*' => 'string|nullable',
            'is_published' => 'boolean',
            'position' => 'integer|nullable',
        ];
    }
}
