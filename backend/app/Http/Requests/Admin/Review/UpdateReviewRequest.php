<?php

namespace App\Http\Requests\Admin\Review;

use Illuminate\Foundation\Http\FormRequest;

class UpdateReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            //'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'blog_id' => 'integer|nullable',
            'name' => 'string|nullable',
            'description' => 'array',
            'description.*' => 'string|nullable',
            'is_published' => 'boolean',
            'position' => 'integer|nullable',
            'rating' => 'integer|min:1|max:5',
        ];
    }
}
