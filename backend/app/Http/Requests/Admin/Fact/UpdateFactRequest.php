<?php

namespace App\Http\Requests\Admin\Fact;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'image' => 'nullable|image|mimes:svg,svg+xml|max:4096',
            'title' => 'array',
            'title.*' => 'string|nullable',
            'alt_title' => 'array',
            'alt_title.*' => 'string|nullable',
            'description' => 'array',
            'description.*' => 'string|nullable',
            'is_published' => 'boolean',
            'position' => 'integer|nullable',
            'number' => 'integer|nullable',
        ];
    }
}
