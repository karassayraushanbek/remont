<?php

namespace App\Http\Requests\Admin\Service;

use Illuminate\Foundation\Http\FormRequest;

class StoreServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'array',
            'description' => 'array',
            'title.*' => 'string|nullable',
            'description.*' => 'string|nullable',
            'is_published' => 'boolean',
            'position' => 'integer|nullable',
            'phone' => 'string|nullable',
        ];
    }
}
