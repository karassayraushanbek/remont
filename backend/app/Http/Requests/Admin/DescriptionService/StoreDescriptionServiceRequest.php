<?php

namespace App\Http\Requests\Admin\DescriptionService;

use Illuminate\Foundation\Http\FormRequest;

class StoreDescriptionServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'tag_description_service_id' => 'integer|nullable',
            'title' => 'array',
            'description' => 'array',
            'title.*' => 'string|nullable',
            'description.*' => 'string|nullable',
            'phone' => 'string|nullable',
            'is_published' => 'boolean',
            'position' => 'integer|nullable',
        ];
    }
}
