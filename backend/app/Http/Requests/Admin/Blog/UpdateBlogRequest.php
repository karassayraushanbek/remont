<?php

namespace App\Http\Requests\Admin\Blog;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'tag_id' => 'integer|nullable',
            'title' => 'array',
            'description' => 'array',
            'meta_title' => 'array',
            'meta_description' => 'array',
            'meta_keywords' => 'array',
            'title.*' => 'string|nullable',
            'description.*' => 'string|nullable',
            'meta_title.*' => 'string|nullable',
            'meta_description.*' => 'string|nullable',
            'meta_keywords.*' => 'string|nullable',
            'is_published' => 'boolean',
            'position' => 'integer|nullable',
            'read' => 'integer|nullable',
        ];
    }
}
