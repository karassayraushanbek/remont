<?php

namespace App\Http\Requests\Admin\Mark;

use Illuminate\Foundation\Http\FormRequest;

class StoreMarkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'title' => 'array',
            'title.*' => 'string|nullable',
            'is_published' => 'boolean',
            'position' => 'integer|nullable',
        ];
    }
}
