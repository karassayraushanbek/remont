<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        if (! $request->expectsJson()) {
            if($request->routeIs('admin.*')) {
                return route('admin.login');
            }
            if($request->routeIs('reader.*') || $request->routeIs('l5-swagger.default.api')) {
                return route('reader.login');
            }
            if(!Auth::guard('user-api')->check() && $request->is('api/*')) {
                abort(response()->json('Неаутентифицируемый.', 401));
            }
            return route('login');
        }

        return null;
    }
}
