<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Feedback\StoreFeedbackRequest;
use App\Http\Requests\Api\Subscribe\StoreSubscribeRequest;
use App\Services\Api\Feedback\FeedbackService;
use Illuminate\Http\JsonResponse;

class FeedbackController extends Controller
{
    public function __construct(protected FeedbackService $feedback)
    {

    }

    /**
     * @OA\Post(
     *   path="/api/V1/subscribe",
     *   tags={"Подписка"},
     *   summary="Подписка на сайт через Email",
     *   operationId="subscribe",
     *   description="Подписка на сайт через Email",
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
     *               required={"email"},
     *               @OA\Property(
     *                   property="email",
     *                   type="string",
     *                   description="Email",
     *                   example="test@mail.ru",
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *      response=201,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *      response=422,
     *      description="Validation errors"
     *   ),
     *)
     **/
    public function subscribe(StoreSubscribeRequest $request): JsonResponse
    {
        $data = $request->validated();

        $this->feedback->subscribe($data);

        return response()->json([
            'message' => config('sessionmessages.successfully_added'),
        ], 201);
    }

    /**
     * @OA\Post(
     *   path="/api/V1/feedback",
     *   tags={"Заявка"},
     *   summary="Заявка на сайт",
     *   operationId="feedback",
     *   description="Заявка на сайт",
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
     *               required={"name","phone"},
     *               @OA\Property(
     *                   property="name",
     *                   type="string",
     *                   description="Имя",
     *                   example="Тест",
     *               ),
     *               @OA\Property(
     *                   property="phone",
     *                   type="string",
     *                   description="Номер телефона",
     *                   example="+ 7 702 818 8187",
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *      response=201,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *      response=422,
     *      description="Validation errors"
     *   ),
     *)
     **/
    public function feedback(StoreFeedbackRequest $request): JsonResponse
    {
        $data = $request->validated();

        $this->feedback->feedback($data);

        return response()->json([
            'message' => config('sessionmessages.successfully_added'),
        ], 201);
    }
}
