<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use SimpleXMLElement;

class SitemapController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/V1/sitemap",
     *     tags={"Sitemap"},
     *     summary="Sitemap",
     *     operationId="sitemap",
     *     description="Sitemap",
     *     @OA\Response(
     *         response="200",
     *         description="List of Sitemap"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function index(): Response
    {
      $contents = Storage::disk('public')->get('sitemap.xml');
      $xml = new SimpleXMLElement($contents);
      $json = json_encode($xml, JSON_PRETTY_PRINT);
      $array = json_decode($json, true);

      if (isset($array['url'])) {
        $newArray = $array['url'];
      }

      $json = json_encode($newArray, JSON_PRETTY_PRINT);

      return response($json)->header('Content-Type', 'application/json');
    }
}
