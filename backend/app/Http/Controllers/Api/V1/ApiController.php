<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\EmailResource;
use App\Http\Resources\V1\MenuResource;
use App\Http\Controllers\Controller;
use App\Http\Resources\V1\PhoneResource;
use App\Services\Api\ApiService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class ApiController extends Controller
{
    public function __construct(protected ApiService $apiService)
    {

    }

    /**
     * @OA\Get(
     *     path="/api/V1/menus",
     *     tags={"Меню"},
     *     summary="Меню сайта",
     *     operationId="menus",
     *     description="Меню сайта",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of Menu"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function getMenus(): JsonResponse
    {
        $menus = $this->apiService->getMenus();

        return response()->json([
            'data' => MenuResource::collection($menus),
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/logo",
     *     tags={"Логотип"},
     *     summary="Логотип сайта",
     *     operationId="logo",
     *     description="Логотип сайта",
     *     @OA\Response(
     *         response="200",
     *         description="List of Logo"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function logo(): JsonResponse
    {
        $settings = $this->apiService->getSetting('logo');

        return response()->json([
            'logo' =>  $settings ? env('APP_URL') . $settings->value : null,
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/call",
     *     tags={"Позвонить"},
     *     summary="Позвонить(номер телефона)",
     *     operationId="call",
     *     description="Позвонить(номер телефона)",
     *     @OA\Response(
     *         response="200",
     *         description="List of Call"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function call(): JsonResponse
    {
        $settings = $this->apiService->getSetting('phone');

        return response()->json([
            'call' =>  $settings ? preg_replace("/[^+,0-9]/", '', $settings->value) : null,
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/phones",
     *     tags={"Номера телефонов"},
     *     summary="Номера телефонов сайта",
     *     operationId="phones",
     *     description="Номера телефонов сайта",
     *     @OA\Response(
     *         response="200",
     *         description="List of Phone"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function getPhones(): JsonResponse
    {
        $phones = $this->apiService->getPhones();

        return response()->json([
            'data' => PhoneResource::collection($phones),
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/emails",
     *     tags={"Email"},
     *     summary="Email сайта",
     *     operationId="emails",
     *     description="Email сайта",
     *     @OA\Response(
     *         response="200",
     *         description="List of Email"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function getEmails(): JsonResponse
    {
        $phones = $this->apiService->getEmails();

        return response()->json([
            'data' => EmailResource::collection($phones),
        ], 200);
    }
}
