<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\AboutResource;
use App\Http\Resources\V1\BannerResource;
use App\Http\Resources\V1\BlogResource;
use App\Http\Resources\V1\CertificateResource;
use App\Http\Resources\V1\DescriptionServiceResource;
use App\Http\Resources\V1\FactResource;
use App\Http\Resources\V1\MarkResource;
use App\Http\Resources\V1\MasterResource;
use App\Http\Resources\V1\ReviewResource;
use App\Http\Resources\V1\TagDescriptionServiceResource;
use App\Http\Resources\V1\TagResource;
use App\Http\Resources\V1\WhyWeResource;
use App\Http\Resources\V1\ServiceResource;
use App\Services\Api\PagesService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class PagesController extends Controller
{
    public function __construct(protected PagesService $pagesService)
    {

    }

    /**
     * @OA\Get(
     *     path="/api/V1/page/main",
     *     tags={"Главная страница"},
     *     summary="Главная страница сайта",
     *     operationId="page_main",
     *     description="Главная страница сайта",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of Main Page"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function mainPage(Request $request): JsonResponse
    {
        $lang = $request->lang;
        $page = $this->pagesService->getFirstPage(1);
        $banner = $this->pagesService->getFirstBanner($page->id);
        $whyWes = $this->pagesService->getWhyWes();
        $marks = $this->pagesService->getMarks();
        $reviews = $this->pagesService->getReviews();
        $services = $this->pagesService->getServices();
        $masters = $this->pagesService->getMasters();

        return response()->json([
            'id' => $page->id,
            'menu_id' => $page->menu_id,
            'title' => $page->title?->{$lang},
            'description' => $page->description?->{$lang},
            'meta_title' => $page->meta_title?->{$lang},
            'meta_description' => $page->meta_description?->{$lang},
            'meta_keywords' => $page->meta_keywords?->{$lang},
            'slug' => $page->slug,
            'banner' => new BannerResource($banner),
            'why_we' => WhyWeResource::collection($whyWes),
            'marks' => MarkResource::collection($marks),
            'reviews' => ReviewResource::collection($reviews),
            'services' => ServiceResource::collection($services),
            'masters' => MasterResource::collection($masters),
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/page/about",
     *     tags={"О нас"},
     *     summary="Страница О нас",
     *     operationId="page_about",
     *     description="Страница О нас",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of About Page"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function aboutPage(Request $request): JsonResponse
    {
        $lang = $request->lang;
        $page = $this->pagesService->getFirstPage(2);
        $banner = $this->pagesService->getFirstBanner($page->id);
        $about = $this->pagesService->getAbouts();
        $certificates = $this->pagesService->getCertificates();
        $facts = $this->pagesService->getFacts();

        return response()->json([
            'id' => $page->id,
            'menu_id' => $page->menu_id,
            'title' => $page->title?->{$lang},
            'description' => $page->description?->{$lang},
            'meta_title' => $page->meta_title?->{$lang},
            'meta_description' => $page->meta_description?->{$lang},
            'meta_keywords' => $page->meta_keywords?->{$lang},
            'slug' => $page->slug,
            'banner' => new BannerResource($banner),
            'about' => AboutResource::collection($about),
            'certificates' => CertificateResource::collection($certificates),
            'facts' => FactResource::collection($facts),
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/page/service",
     *     tags={"Услуги"},
     *     summary="Страница Услуги",
     *     operationId="page_service",
     *     description="Страница Услуги",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Parameter(
     *         name="tag",
     *         in="query",
     *         description="Тег ID",
     *         example="1",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of Service Page"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function servicePage(Request $request): JsonResponse
    {
        $lang = $request->lang;
        $page = $this->pagesService->getFirstPage(3);
        $banner = $this->pagesService->getFirstBanner($page->id);
        $services = $this->pagesService->getServices();
        $tags = $this->pagesService->getTagDescriptionServices();
        $descriptionServices = $this->pagesService->getDescriptionServices();
        $marks = $this->pagesService->getMarks();

        return response()->json([
            'id' => $page->id,
            'menu_id' => $page->menu_id,
            'title' => $page->title?->{$lang},
            'description' => $page->description?->{$lang},
            'meta_title' => $page->meta_title?->{$lang},
            'meta_description' => $page->meta_description?->{$lang},
            'meta_keywords' => $page->meta_keywords?->{$lang},
            'slug' => $page->slug,
            'banner' => new BannerResource($banner),
            'services' => ServiceResource::collection($services),
            'tags' => TagDescriptionServiceResource::collection($tags),
            'description_services' => DescriptionServiceResource::collection($descriptionServices),
            'marks' => MarkResource::collection($marks),
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/page/review",
     *     tags={"Отзывы"},
     *     summary="Страница Отзывы",
     *     operationId="page_review",
     *     description="Страница Отзывы",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of Review Page"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function reviewPage(Request $request): JsonResponse
    {
        $lang = $request->lang;
        $page = $this->pagesService->getFirstPage(4);
        $banner = $this->pagesService->getFirstBanner($page->id);
        $reviews = $this->pagesService->getReviews();

        return response()->json([
            'id' => $page->id,
            'menu_id' => $page->menu_id,
            'title' => $page->title?->{$lang},
            'description' => $page->description?->{$lang},
            'meta_title' => $page->meta_title?->{$lang},
            'meta_description' => $page->meta_description?->{$lang},
            'meta_keywords' => $page->meta_keywords?->{$lang},
            'slug' => $page->slug,
            'banner' => new BannerResource($banner),
            'reviews' => ReviewResource::collection($reviews),
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/page/master",
     *     tags={"Мастера"},
     *     summary="Страница Мастера",
     *     operationId="page_master",
     *     description="Страница Мастера",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of Master Page"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function masterPage(Request $request): JsonResponse
    {
        $lang = $request->lang;
        $page = $this->pagesService->getFirstPage(5);
        $banner = $this->pagesService->getFirstBanner($page->id);
        $masters = $this->pagesService->getMasters();

        return response()->json([
            'id' => $page->id,
            'menu_id' => $page->menu_id,
            'title' => $page->title?->{$lang},
            'description' => $page->description?->{$lang},
            'meta_title' => $page->meta_title?->{$lang},
            'meta_description' => $page->meta_description?->{$lang},
            'meta_keywords' => $page->meta_keywords?->{$lang},
            'slug' => $page->slug,
            'banner' => new BannerResource($banner),
            'masters' => MasterResource::collection($masters),
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/page/contact",
     *     tags={"Контакты"},
     *     summary="Страница Контакты",
     *     operationId="page_contact",
     *     description="Страница Контакты",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of Contact Page"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function contactPage(Request $request): JsonResponse
    {
        $lang = $request->lang;
        $page = $this->pagesService->getFirstPage(6);
        $banner = $this->pagesService->getFirstBanner($page->id);

        return response()->json([
            'id' => $page->id,
            'menu_id' => $page->menu_id,
            'title' => $page->title?->{$lang},
            'description' => $page->description?->{$lang},
            'meta_title' => $page->meta_title?->{$lang},
            'meta_description' => $page->meta_description?->{$lang},
            'meta_keywords' => $page->meta_keywords?->{$lang},
            'slug' => $page->slug,
            'banner' => new BannerResource($banner),
        ], 200);
    }

    /**
     * @OA\Get(
     *     path="/api/V1/page/blog",
     *     tags={"Блог"},
     *     summary="Страница Блог",
     *     operationId="page_blog",
     *     description="Страница Блог",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Parameter(
     *         name="tag",
     *         in="query",
     *         description="Тег ID",
     *         example="1",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of Blog Page"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function blogPage(Request $request): JsonResponse
    {
        $lang = $request->lang;
        $page = $this->pagesService->getFirstPage(7);
        $banner = $this->pagesService->getFirstBanner($page->id);
        $tags = $this->pagesService->getTagBlogs();
        $blogs = $this->pagesService->getBlogs();

        return response()->json([
            'id' => $page->id,
            'menu_id' => $page->menu_id,
            'title' => $page->title?->{$lang},
            'description' => $page->description?->{$lang},
            'meta_title' => $page->meta_title?->{$lang},
            'meta_description' => $page->meta_description?->{$lang},
            'meta_keywords' => $page->meta_keywords?->{$lang},
            'slug' => $page->slug,
            'banner' => new BannerResource($banner),
            'tags' => TagResource::collection($tags),
            'blogs' => BlogResource::collection($blogs),
        ], 200);
    }


    /**
     * @OA\Get(
     *     path="/api/V1/page/blog/{id}",
     *     tags={"Блог"},
     *     summary="Страница Одного Блога",
     *     operationId="page_blog_by_id",
     *     description="Страница Одного Блога",
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         description="lang должен быть ru|en|kz и т.д.",
     *         required=false,
     *         example="ru",
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Блог ID",
     *         example="1",
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of First Blog Page"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     * )
     */
    public function firstBlogPage(Request $request, int $id): JsonResponse
    {
        $lang = $request->lang;
        $blog = $this->pagesService->getFirstBlog($id);
        $blogs = $this->pagesService->getBlogByTagId($blog->tag_id);
        $reviews = $this->pagesService->getReviewsByBlogId($blog->id);

        return response()->json([
            'id' => $blog->id,
            'tag_id' => $blog->tag_id,
            'tag' => $blog->tag?->title?->{$lang},
            'image' => $blog->image_url,
            'title' => $blog->title?->{$lang},
            'description' => $blog->description?->{$lang},
            'meta_title' => $blog->meta_title?->{$lang},
            'meta_description' => $blog->meta_description?->{$lang},
            'meta_keywords' => $blog->meta_keywords?->{$lang},
            'slug' => $blog->slug,
            'read' => $blog->read,
            'date' => $blog->created_at?->format('d/m/Y'),
            'similar' => BlogResource::collection($blogs),
            'reviews' => ReviewResource::collection($reviews),
        ], 200);
    }
}
