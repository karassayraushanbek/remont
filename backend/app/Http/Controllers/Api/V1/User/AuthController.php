<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\UpdateUserRequest;
use App\Http\Resources\V1\UserResource;
use App\Services\Api\Auth\AuthService;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    public function __construct(protected AuthService $authService)
    {
        $this->middleware('auth:user-api', [
            'except' => ['login', 'register']
        ]);
    }

    /**
     * @OA\Tag(
     *   name="Регистрация / Вход",
     *   description="API endpoints связанные с Пользователем"
     * )
     * @OA\Post(
     *   path="/api/V1/user/login",
     *   tags={"Регистрация / Вход"},
     *   summary="Вход для Пользователя",
     *   operationId="user_login",
     *   description="Вход для Пользователя",
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *               required={"email", "password"},
     *               @OA\Property(
     *                   property="email",
     *                   type="string",
     *                   description="Email Пользователя",
     *                   example="test@mail.ru"
     *               ),
     *               @OA\Property(
     *                   property="password",
     *                   type="string",
     *                   description="Пароль Пользователя",
     *                   example="123admin"
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *          mediaType="multipart/form-data",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *      response=403,
     *      description="Forbidden"
     *   )
     *)
     **/
    public function login(LoginRequest $request): JsonResponse
    {
        $data = $request->validated();

        if(!$token = $this->authService->login($data)) {
            return response()->json(['error' => 'Неавторизованный'], 401);
        }

        return response()->json($this->authService->respondWithToken($token));
    }

    /**
     * @OA\Tag(
     *   name="Регистрация / Вход",
     *   description="API endpoints связанные с Пользователем"
     * )
     * @OA\Post(
     *   path="/api/V1/user/register",
     *   tags={"Регистрация / Вход"},
     *   summary="Регистрация Пользователя",
     *   operationId="user_register",
     *   description="Регистрация Пользователя",
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *               required={"name", "email", "password", "password_confirmation"},
     *               @OA\Property(
     *                   property="name",
     *                   type="string",
     *                   description="Создать Имя для Пользователя",
     *                   example="Test",
     *               ),
     *               @OA\Property(
     *                   property="email",
     *                   type="string",
     *                   description="Создать Email для Пользователя",
     *                   example="test@mail.ru",
     *               ),
     *               @OA\Property(
     *                   property="password",
     *                   type="string",
     *                   description="Создать Пароль для Пользователя",
     *                   example="123admin",
     *               ),
     *               @OA\Property(
     *                   property="password_confirmation",
     *                   type="string",
     *                   description="Повторить Пароль для Пользователя",
     *                   example="123admin",
     *               ),
     *           )
     *       )
     *   ),
     *   @OA\Response(
     *      response=201,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="multipart/form-data",
     *      )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response="401",
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=403,
     *      description="Forbidden"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *      response=422,
     *      description="Validation errors"
     *   ),
     *)
     **/
    public function register(RegisterRequest $request): JsonResponse
    {
        $data = $request->validated();

        try {
            $this->authService->registerUser($data);

        } catch (\Exception $e) {
            return response()->json([
                'message' => config('sessionmessages.an_error_has_occurred'),
                'error' => $e->getMessage(),
            ], 500);
        }

        return response()->json([
            'message' => config('sessionmessages.successfully_registered'),
        ], 201);
    }

    /**
     * @OA\Tag(
     *   name="Регистрация / Вход",
     *   description="API endpoints связанные с Пользователем"
     * )
     * @OA\Post(
     ** path="/api/V1/user/update",
     *   tags={"Регистрация / Вход"},
     *   summary="Обновление данных Пользователя",
     *   operationId="user_update",
     *   description="Обновление данных Пользователя",
     *   security={{"Bearer": {}}},
     *   @OA\RequestBody(
     *      @OA\MediaType(
     *          mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="name",
     *                  type="string",
     *                  description="Обновить Имя для Пользователя",
     *                  example="Test1"
     *              ),
     *              @OA\Property(
     *                  property="email",
     *                  type="string",
     *                  description="Обновить Email для Пользователя",
     *                  example="test1@mail.ru"
     *              ),
     *          )
     *      )
     *   ),
     *   @OA\Response(
     *      response=201,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="multipart/form-data",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *      description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *   @OA\Response(
     *      response=403,
     *      description="Forbidden"
     *   ),
     *   @OA\Response(
     *      response=422,
     *      description="Validation errors"
     *   ),
     *)
     **/
    public function update(UpdateUserRequest $request): JsonResponse
    {
        $data = $request->validated();
        $user = auth()->user();

        try {
            $this->authService->updateUser($user, $data);

        } catch (\Exception $e) {
            return response()->json([
                'message' => config('sessionmessages.an_error_has_occurred'),
                'error' => $e->getMessage(),
            ], 500);
        }

        return response()->json([
            'message' => config('sessionmessages.successfully_updated'),
        ], 201);
    }

    /**
     * @OA\Post(
     *     path="/api/V1/user/me",
     *     tags={"Регистрация / Вход"},
     *     summary="Данные Пользователя",
     *     operationId="user_me",
     *     description="Данные Пользователя",
     *     security={{"Bearer": {}}},
     *     @OA\Response(
     *         response="200",
     *         description="List of User Data"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     *     @OA\Response(
     *          response=422,
     *          description="Validation errors."
     *     ),
     * )
     */
    public function me(): JsonResponse
    {
        return response()->json([
            'user' => new UserResource(auth()->user())
        ], 200);
    }

    /**
     * @OA\Post(
     *     path="/api/V1/user/logout",
     *     tags={"Регистрация / Вход"},
     *     summary="Выход для Пользователя",
     *     operationId="user_logout",
     *     description="Выход для Пользователя",
     *     security={{"Bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     *     @OA\Response(
     *          response=422,
     *          description="Validation errors."
     *     ),
     * )
     */
    public function logout(): JsonResponse
    {
        auth()->logout();

        return response()->json(['message' => config('sessionmessages.successfully_logged_out')]);
    }

    /**
     * @OA\Post(
     *     path="/api/V1/user/refresh",
     *     tags={"Регистрация / Вход"},
     *     summary="Обновление JWT токена",
     *     operationId="user_refresh",
     *     description="Обновление JWT токена",
     *     security={{"Bearer": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="Page not found."
     *     ),
     *     @OA\Response(
     *          response=422,
     *          description="Validation errors."
     *     ),
     * )
     */
    public function refresh(): array
    {
        return $this->authService->respondWithToken(auth()->refresh());
    }
}
