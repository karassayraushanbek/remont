<?php

namespace App\Http\Controllers\Reader\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = null;

    public function __construct()
    {
        $this->middleware('guest:reader')->except('logout');
        $this->redirectTo = route('reader.home');
    }

    public function showLoginForm()
    {
        return view('reader.auth.login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return redirect()->route('reader.login');
    }

    protected function guard()
    {
        return Auth::guard('reader');
    }
}
