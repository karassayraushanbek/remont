<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Blog\StoreBlogRequest;
use App\Http\Requests\Admin\Blog\UpdateBlogPositionRequest;
use App\Http\Requests\Admin\Blog\UpdateBlogRequest;

use App\Models\Blog;
use App\Services\Admin\Tag\TagService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Blog\BlogService;

class BlogController extends Controller
{
    public function __construct(protected BlogService $blogService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-блог', ['only' => ['index','show']]);
        $this->middleware('permission:создание-блог', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-блог', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-блог', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $blogs = $this->blogService->getBlogs();

        return view('admin.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->blogService->getLangs();
        $tags = (new TagService())->getTagCollection();

        return view('admin.blogs.create', compact('langs', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBlogRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->blogService->createBlog($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.blogs.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Blog $blog): View
    {
        return view('admin.blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Blog $blog): View
    {
        $langs = $this->blogService->getLangs();
        $tags = (new TagService())->getTagCollection();

        return view('admin.blogs.edit', compact('blog', 'langs', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBlogRequest $request, Blog $blog): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->blogService->updateBlog($blog, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.blogs.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->blogService->deleteBlog($blog);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.blogs.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateBlogPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->blogService->updateBlogPosition($data, $id);
        }

        return false;
    }
}
