<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Language\StoreLanguageRequest;
use App\Http\Requests\Admin\Language\UpdateLanguageRequest;
use App\Services\Admin\Language\LanguageService;
use App\Models\Language;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

class LanguageController extends Controller
{
    public function __construct(protected LanguageService $languageService)
    {
        $this->middleware('permission:все-языки', ['only' => ['index','show']]);
        $this->middleware('permission:создание-языки', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-языки', ['only' => ['edit','update']]);
        $this->middleware('permission:удаление-языки', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $languages = $this->languageService->getLanguages();

        return view('admin.language.index',compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.language.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLanguageRequest $request): RedirectResponse
    {
        $data = $request->validated();

        //DB::beginTransaction();
        try {
            $this->languageService->createLanguage($data);

            //DB::commit();
        } catch (\Exception $exception) {
            //DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.language.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Language $language): View
    {
        return view('admin.language.show', compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Language $language): View
    {
        return view('admin.language.edit', compact('language'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLanguageRequest $request, Language $language): RedirectResponse
    {
        $data = $request->validated();

        //DB::beginTransaction();
        try {
            $this->languageService->updateLanguage($language, $data);

            //DB::commit();
        } catch (\Exception $exception) {
           //DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.language.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Language $language): RedirectResponse
    {
        //DB::beginTransaction();
        try {
            $this->languageService->deleteLanguage($language);

            //DB::commit();
        } catch (\Exception $exception) {
            //DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.language.index')->with('success', config('sessionmessages.successfully_deleted'));
    }
}
