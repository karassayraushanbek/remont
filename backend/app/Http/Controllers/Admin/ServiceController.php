<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Service\StoreServiceRequest;
use App\Http\Requests\Admin\Service\UpdateServicePositionRequest;
use App\Http\Requests\Admin\Service\UpdateServiceRequest;

use App\Models\Service;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Service\ServiceService;

class ServiceController extends Controller
{
    public function __construct(protected ServiceService $serviceService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-услуги', ['only' => ['index','show']]);
        $this->middleware('permission:создание-услуги', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-услуги', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-услуги', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $services = $this->serviceService->getServices();

        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->serviceService->getLangs();

        return view('admin.services.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreServiceRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->serviceService->createService($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.services.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Service $service): View
    {
        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Service $service): View
    {
        $langs = $this->serviceService->getLangs();

        return view('admin.services.edit', compact('service', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateServiceRequest $request, Service $service): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->serviceService->updateService($service, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.services.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Service $service): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->serviceService->deleteService($service);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.services.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateServicePositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->serviceService->updateServicePosition($data, $id);
        }

        return false;
    }
}
