<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Tag\StoreTagRequest;
use App\Http\Requests\Admin\Tag\UpdateTagPositionRequest;
use App\Http\Requests\Admin\Tag\UpdateTagRequest;

use App\Models\Tag;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Tag\TagService;

class TagController extends Controller
{
    public function __construct(protected TagService $tagService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-тег-блог', ['only' => ['index','show']]);
        $this->middleware('permission:создание-тег-блог', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-тег-блог', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-тег-блог', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $tags = $this->tagService->getTags();

        return view('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->tagService->getLangs();

        return view('admin.tags.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTagRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->tagService->createTag($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tags.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Tag $tag): View
    {
        return view('admin.tags.show', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Tag $tag): View
    {
        $langs = $this->tagService->getLangs();

        return view('admin.tags.edit', compact('tag', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTagRequest $request, Tag $tag): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->tagService->updateTag($tag, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tags.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Tag $tag): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->tagService->deleteTag($tag);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tags.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateTagPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->tagService->updateTagPosition($data, $id);
        }

        return false;
    }
}
