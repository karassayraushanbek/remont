<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Master\StoreMasterRequest;
use App\Http\Requests\Admin\Master\UpdateMasterPositionRequest;
use App\Http\Requests\Admin\Master\UpdateMasterRequest;

use App\Models\Master;
use App\Services\Admin\TagMaster\TagMasterService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Master\MasterService;

class MasterController extends Controller
{
    public function __construct(protected MasterService $masterService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-мастера', ['only' => ['index','show']]);
        $this->middleware('permission:создание-мастера', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-мастера', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-мастера', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $masters = $this->masterService->getMasters();

        return view('admin.masters.index', compact('masters'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->masterService->getLangs();
        $tags = (new TagMasterService())->getTagMasterCollection();

        return view('admin.masters.create', compact('langs', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMasterRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->masterService->createMaster($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.masters.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Master $master): View
    {
        return view('admin.masters.show', compact('master'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Master $master): View
    {
        $langs = $this->masterService->getLangs();
        $tags = (new TagMasterService())->getTagMasterCollection();

        return view('admin.masters.edit', compact('master', 'langs', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMasterRequest $request, Master $master): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->masterService->updateMaster($master, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.masters.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Master $master): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->masterService->deleteMaster($master);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.masters.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateMasterPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->masterService->updateMasterPosition($data, $id);
        }

        return false;
    }
}
