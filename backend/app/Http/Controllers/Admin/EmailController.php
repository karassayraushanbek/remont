<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Email\StoreEmailRequest;
use App\Http\Requests\Admin\Email\UpdateEmailRequest;

use App\Models\Email;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Email\EmailService;

class EmailController extends Controller
{
    public function __construct(protected EmailService $emailService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-емэйл', ['only' => ['index','show']]);
        $this->middleware('permission:создание-емэйл', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-емэйл', ['only' => ['edit','update']]);
        $this->middleware('permission:удаление-емэйл', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $emails = $this->emailService->getEmails();

        return view('admin.emails.index', compact('emails'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.emails.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEmailRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->emailService->createEmail($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.emails.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Email $email): View
    {
        return view('admin.emails.show', compact('email'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Email $email): View
    {
        return view('admin.emails.edit', compact('email'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEmailRequest $request, Email $email): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->emailService->updateEmail($email, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.emails.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Email $email): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->emailService->deleteEmail($email);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.emails.index')->with('success', config('sessionmessages.successfully_deleted'));
    }
}
