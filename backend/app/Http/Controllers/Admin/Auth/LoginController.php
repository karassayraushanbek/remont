<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = null;

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
        $this->redirectTo = route('admin.home');
    }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string|email|exists:admins,email',
            'password' => 'required|string',
        ], [
            $this->username() . '.required' => iconv('cp1251//IGNORE', 'utf-8//IGNORE','Email не может быть пустым'),
            'password.required' => iconv('cp1251//IGNORE', 'utf-8//IGNORE','Пароль не может быть пустым'),
        ]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return redirect()->route('admin.login');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }
}
