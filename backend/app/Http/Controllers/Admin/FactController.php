<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Fact\StoreFactRequest;
use App\Http\Requests\Admin\Fact\UpdateFactPositionRequest;
use App\Http\Requests\Admin\Fact\UpdateFactRequest;

use App\Models\Fact;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Fact\FactService;

class FactController extends Controller
{
    public function __construct(protected FactService $factService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-факты', ['only' => ['index','show']]);
        $this->middleware('permission:создание-факты', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-факты', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-факты', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $facts = $this->factService->getFacts();

        return view('admin.facts.index', compact('facts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->factService->getLangs();

        return view('admin.facts.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFactRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->factService->createFact($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.facts.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Fact $fact): View
    {
        return view('admin.facts.show', compact('fact'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Fact $fact): View
    {
        $langs = $this->factService->getLangs();

        return view('admin.facts.edit', compact('fact', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFactRequest $request, Fact $fact): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->factService->updateFact($fact, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.facts.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Fact $fact): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->factService->deleteFact($fact);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.facts.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateFactPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->factService->updateFactPosition($data, $id);
        }

        return false;
    }
}
