<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\TagDescriptionService\StoreTagDescriptionServiceRequest;
use App\Http\Requests\Admin\TagDescriptionService\UpdateTagDescriptionServicePositionRequest;
use App\Http\Requests\Admin\TagDescriptionService\UpdateTagDescriptionServiceRequest;

use App\Models\TagDescriptionService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\TagDescriptionService\TagDescriptionServiceService;

class TagDescriptionServiceController extends Controller
{
    public function __construct(protected TagDescriptionServiceService $tagDescriptionServiceService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-тег-услуги', ['only' => ['index','show']]);
        $this->middleware('permission:создание-тег-услуги', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-тег-услуги', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-тег-услуги', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $tagDescriptionServices = $this->tagDescriptionServiceService->getTagDescriptionServices();

        return view('admin.tag-description-services.index', compact('tagDescriptionServices'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->tagDescriptionServiceService->getLangs();

        return view('admin.tag-description-services.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTagDescriptionServiceRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->tagDescriptionServiceService->createTagDescriptionService($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tag-description-services.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(TagDescriptionService $tagDescriptionService): View
    {
        return view('admin.tag-description-services.show', compact('tagDescriptionService'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TagDescriptionService $tagDescriptionService): View
    {
        $langs = $this->tagDescriptionServiceService->getLangs();

        return view('admin.tag-description-services.edit', compact('tagDescriptionService', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTagDescriptionServiceRequest $request, TagDescriptionService $tagDescriptionService): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->tagDescriptionServiceService->updateTagDescriptionService($tagDescriptionService, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tag-description-services.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TagDescriptionService $tagDescriptionService): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->tagDescriptionServiceService->deleteTagDescriptionService($tagDescriptionService);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tag-description-services.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateTagDescriptionServicePositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->tagDescriptionServiceService->updateTagDescriptionServicePosition($data, $id);
        }

        return false;
    }
}
