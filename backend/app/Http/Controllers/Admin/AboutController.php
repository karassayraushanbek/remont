<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\About\StoreAboutRequest;
use App\Http\Requests\Admin\About\UpdateAboutPositionRequest;
use App\Http\Requests\Admin\About\UpdateAboutRequest;

use App\Models\About;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\About\AboutService;

class AboutController extends Controller
{
    public function __construct(protected AboutService $aboutService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-о-нас', ['only' => ['index','show']]);
        $this->middleware('permission:создание-о-нас', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-о-нас', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-о-нас', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $abouts = $this->aboutService->getAbouts();

        return view('admin.abouts.index', compact('abouts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->aboutService->getLangs();

        return view('admin.abouts.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAboutRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->aboutService->createAbout($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.abouts.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(About $about): View
    {
        return view('admin.abouts.show', compact('about'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(About $about): View
    {
        $langs = $this->aboutService->getLangs();

        return view('admin.abouts.edit', compact('about', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAboutRequest $request, About $about): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->aboutService->updateAbout($about, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.abouts.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(About $about): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->aboutService->deleteAbout($about);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.abouts.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateAboutPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->aboutService->updateAboutPosition($data, $id);
        }

        return false;
    }
}
