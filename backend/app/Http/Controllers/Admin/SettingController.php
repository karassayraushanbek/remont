<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\StoreSettingRequest;
use App\Http\Requests\Admin\Setting\UpdateSettingRequest;
use App\Services\Admin\Setting\SettingService;
use App\Models\Setting;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function __construct(protected SettingService $settingService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:логотип', ['only' => ['index','show']]);
    }

    public function index(): View
    {
        $settings = $this->settingService->getSettings();

        return view('admin.setting.index', compact('settings'));
    }

    public function store(StoreSettingRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->settingService->createSetting($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.setting.index')->with('flash_message', config('sessionmessages.successfully_added'));
    }

    public function update(UpdateSettingRequest $request, string $id): RedirectResponse
    {
        $data = $request->validated();
        $setting = Setting::find($id);

        DB::beginTransaction();
        try {
            $this->settingService->updateSetting($setting, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.setting.index')->with('flash_message', config('sessionmessages.successfully_updated'));
    }

    public function destroy(string $id): RedirectResponse
    {
        $setting = Setting::find($id);

        DB::beginTransaction();
        try {
            $this->settingService->deleteSetting($setting);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.setting.index')->with('flash_message', config('sessionmessages.successfully_deleted'));
    }
}
