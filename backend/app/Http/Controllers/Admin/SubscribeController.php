<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Subscribe\StoreSubscribeRequest;
use App\Http\Requests\Admin\Subscribe\UpdateSubscribeRequest;

use App\Models\Subscribe;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Subscribe\SubscribeService;

class SubscribeController extends Controller
{
    public function __construct(protected SubscribeService $subscribeService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-подписчики', ['only' => ['index','show']]);
        $this->middleware('permission:создание-подписчики', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-подписчики', ['only' => ['edit','update']]);
        $this->middleware('permission:удаление-подписчики', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $subscribes = $this->subscribeService->getSubscribes();

        return view('admin.subscribes.index', compact('subscribes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.subscribes.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSubscribeRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->subscribeService->createSubscribe($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.subscribes.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Subscribe $subscribe): View
    {
        return view('admin.subscribes.show', compact('subscribe'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Subscribe $subscribe): View
    {
        return view('admin.subscribes.edit', compact('subscribe'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSubscribeRequest $request, Subscribe $subscribe): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->subscribeService->updateSubscribe($subscribe, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.subscribes.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Subscribe $subscribe): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->subscribeService->deleteSubscribe($subscribe);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.subscribes.index')->with('success', config('sessionmessages.successfully_deleted'));
    }
}
