<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Mark\StoreMarkRequest;
use App\Http\Requests\Admin\Mark\UpdateMarkPositionRequest;
use App\Http\Requests\Admin\Mark\UpdateMarkRequest;

use App\Models\Mark;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Mark\MarkService;

class MarkController extends Controller
{
    public function __construct(protected MarkService $markService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-марки', ['only' => ['index','show']]);
        $this->middleware('permission:создание-марки', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-марки', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-марки', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $marks = $this->markService->getMarks();

        return view('admin.marks.index', compact('marks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->markService->getLangs();

        return view('admin.marks.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMarkRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->markService->createMark($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.marks.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Mark $mark): View
    {
        return view('admin.marks.show', compact('mark'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Mark $mark): View
    {
        $langs = $this->markService->getLangs();

        return view('admin.marks.edit', compact('mark', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMarkRequest $request, Mark $mark): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->markService->updateMark($mark, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.marks.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Mark $mark): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->markService->deleteMark($mark);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.marks.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateMarkPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->markService->updateMarkPosition($data, $id);
        }

        return false;
    }
}
