<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\WhyWe\StoreWhyWeRequest;
use App\Http\Requests\Admin\WhyWe\UpdateWhyWePositionRequest;
use App\Http\Requests\Admin\WhyWe\UpdateWhyWeRequest;

use App\Models\WhyWe;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\WhyWe\WhyWeService;

class WhyWeController extends Controller
{
    public function __construct(protected WhyWeService $whyWeService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-почему-мы', ['only' => ['index','show']]);
        $this->middleware('permission:создание-почему-мы', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-почему-мы', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-почему-мы', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $whyWes = $this->whyWeService->getWhyWes();

        return view('admin.why-wes.index', compact('whyWes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->whyWeService->getLangs();

        return view('admin.why-wes.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreWhyWeRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->whyWeService->createWhyWe($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.why-wes.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(WhyWe $whyWe): View
    {
        return view('admin.why-wes.show', compact('whyWe'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(WhyWe $whyWe): View
    {
        $langs = $this->whyWeService->getLangs();

        return view('admin.why-wes.edit', compact('whyWe', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateWhyWeRequest $request, WhyWe $whyWe): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->whyWeService->updateWhyWe($whyWe, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.why-wes.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(WhyWe $whyWe): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->whyWeService->deleteWhyWe($whyWe);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.why-wes.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateWhyWePositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->whyWeService->updateWhyWePosition($data, $id);
        }

        return false;
    }
}
