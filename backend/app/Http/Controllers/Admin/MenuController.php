<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Menu\StoreMenuRequest;
use App\Http\Requests\Admin\Menu\UpdateMenuPositionRequest;
use App\Http\Requests\Admin\Menu\UpdateMenuRequest;

use App\Models\Menu;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Menu\MenuService;

class MenuController extends Controller
{
    public function __construct(protected MenuService $menuService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-меню', ['only' => ['index','show']]);
        $this->middleware('permission:создание-меню', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-меню', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-меню', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $menus = $this->menuService->getMenus();

        return view('admin.menus.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->menuService->getLangs();

        return view('admin.menus.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMenuRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->menuService->createMenu($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.menus.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Menu $menu): View
    {
        return view('admin.menus.show', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Menu $menu): View
    {
        $langs = $this->menuService->getLangs();

        return view('admin.menus.edit', compact('menu', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMenuRequest $request, Menu $menu): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->menuService->updateMenu($menu, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.menus.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Menu $menu): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->menuService->deleteMenu($menu);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.menus.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateMenuPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->menuService->updateMenuPosition($data, $id);
        }

        return false;
    }
}
