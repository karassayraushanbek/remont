<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Profile\UpdateProfileRequest;
use App\Services\Admin\Profile\ProfileService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function __construct(protected ProfileService $profileService)
    {
        $this->middleware('auth:admin');
    }

    public function index(): View
    {
        $user = Auth::user();

        return view('admin.profile.index', compact('user'));
    }

    public function update(UpdateProfileRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $user = Auth::user();

        DB::beginTransaction();
        try {
            $m = $this->profileService->updateProfile($user, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.profile')->with('success', config('sessionmessages.successfully_updated') . $m);
    }
}
