<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\DescriptionService\StoreDescriptionServiceRequest;
use App\Http\Requests\Admin\DescriptionService\UpdateDescriptionServicePositionRequest;
use App\Http\Requests\Admin\DescriptionService\UpdateDescriptionServiceRequest;

use App\Models\DescriptionService;
use App\Services\Admin\TagDescriptionService\TagDescriptionServiceService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\DescriptionService\DescriptionServiceService;

class DescriptionServiceController extends Controller
{
    public function __construct(protected DescriptionServiceService $descriptionServiceService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-описание-услуги', ['only' => ['index','show']]);
        $this->middleware('permission:создание-описание-услуги', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-описание-услуги', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-описание-услуги', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $descriptionServices = $this->descriptionServiceService->getDescriptionServices();

        return view('admin.description-services.index', compact('descriptionServices'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->descriptionServiceService->getLangs();
        $tags = (new TagDescriptionServiceService())->getTagDescriptionServiceCollection();

        return view('admin.description-services.create', compact('langs', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDescriptionServiceRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->descriptionServiceService->createDescriptionService($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.description-services.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(DescriptionService $descriptionService): View
    {
        return view('admin.description-services.show', compact('descriptionService'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DescriptionService $descriptionService): View
    {
        $langs = $this->descriptionServiceService->getLangs();
        $tags = (new TagDescriptionServiceService())->getTagDescriptionServiceCollection();

        return view('admin.description-services.edit', compact('descriptionService', 'langs', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateDescriptionServiceRequest $request, DescriptionService $descriptionService): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->descriptionServiceService->updateDescriptionService($descriptionService, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.description-services.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DescriptionService $descriptionService): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->descriptionServiceService->deleteDescriptionService($descriptionService);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.description-services.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateDescriptionServicePositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->descriptionServiceService->updateDescriptionServicePosition($data, $id);
        }

        return false;
    }
}
