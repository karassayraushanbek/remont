<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Certificate\StoreCertificateRequest;
use App\Http\Requests\Admin\Certificate\UpdateCertificatePositionRequest;
use App\Http\Requests\Admin\Certificate\UpdateCertificateRequest;

use App\Models\Certificate;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Certificate\CertificateService;

class CertificateController extends Controller
{
    public function __construct(protected CertificateService $certificateService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-сертификаты', ['only' => ['index','show']]);
        $this->middleware('permission:создание-сертификаты', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-сертификаты', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-сертификаты', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $certificates = $this->certificateService->getCertificates();

        return view('admin.certificates.index', compact('certificates'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->certificateService->getLangs();

        return view('admin.certificates.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCertificateRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->certificateService->createCertificate($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.certificates.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Certificate $certificate): View
    {
        return view('admin.certificates.show', compact('certificate'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Certificate $certificate): View
    {
        $langs = $this->certificateService->getLangs();

        return view('admin.certificates.edit', compact('certificate', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCertificateRequest $request, Certificate $certificate): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->certificateService->updateCertificate($certificate, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.certificates.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Certificate $certificate): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->certificateService->deleteCertificate($certificate);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.certificates.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateCertificatePositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->certificateService->updateCertificatePosition($data, $id);
        }

        return false;
    }
}
