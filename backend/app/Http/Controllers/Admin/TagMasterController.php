<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\TagMaster\StoreTagMasterRequest;
use App\Http\Requests\Admin\TagMaster\UpdateTagMasterPositionRequest;
use App\Http\Requests\Admin\TagMaster\UpdateTagMasterRequest;

use App\Models\TagMaster;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\TagMaster\TagMasterService;

class TagMasterController extends Controller
{
    public function __construct(protected TagMasterService $tagMasterService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-тег-мастера', ['only' => ['index','show']]);
        $this->middleware('permission:создание-тег-мастера', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-тег-мастера', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-тег-мастера', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $tagMasters = $this->tagMasterService->getTagMasters();

        return view('admin.tag-masters.index', compact('tagMasters'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->tagMasterService->getLangs();

        return view('admin.tag-masters.create', compact('langs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTagMasterRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->tagMasterService->createTagMaster($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tag-masters.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(TagMaster $tagMaster): View
    {
        return view('admin.tag-masters.show', compact('tagMaster'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TagMaster $tagMaster): View
    {
        $langs = $this->tagMasterService->getLangs();

        return view('admin.tag-masters.edit', compact('tagMaster', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTagMasterRequest $request, TagMaster $tagMaster): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->tagMasterService->updateTagMaster($tagMaster, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tag-masters.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TagMaster $tagMaster): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->tagMasterService->deleteTagMaster($tagMaster);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.tag-masters.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateTagMasterPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->tagMasterService->updateTagMasterPosition($data, $id);
        }

        return false;
    }
}
