<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Review\StoreReviewRequest;
use App\Http\Requests\Admin\Review\UpdateReviewPositionRequest;
use App\Http\Requests\Admin\Review\UpdateReviewRequest;

use App\Models\Blog;
use App\Models\Review;
use App\Services\Admin\Blog\BlogService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Review\ReviewService;

class ReviewController extends Controller
{
    public function __construct(protected ReviewService $reviewService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-отзывы', ['only' => ['index','show']]);
        $this->middleware('permission:создание-отзывы', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-отзывы', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-отзывы', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Blog $blog): View
    {
        $reviews = $this->reviewService->getReviews($blog);

        return view('admin.reviews.index', compact('reviews', 'blog'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Blog $blog): View
    {
        $langs = $this->reviewService->getLangs();
        $blogs = (new BlogService())->getBlogCollection();

        return view('admin.reviews.create', compact('langs', 'blogs', 'blog'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Blog $blog, StoreReviewRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->reviewService->createReview($blog, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.blogs.reviews.index', ['blog' => $blog])->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Blog $blog, Review $review): View
    {
        return view('admin.reviews.show', compact('review', 'blog'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Blog $blog, Review $review): View
    {
        $langs = $this->reviewService->getLangs();
        $blogs = (new BlogService())->getBlogCollection();

        return view('admin.reviews.edit', compact('review', 'langs', 'blogs', 'blog'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Blog $blog, UpdateReviewRequest $request, Review $review): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->reviewService->updateReview($blog, $review, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.blogs.reviews.index', ['blog' => $blog])->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog, Review $review): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->reviewService->deleteReview($blog, $review);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.blogs.reviews.index', ['blog' => $blog])->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdateReviewPositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->reviewService->updateReviewPosition($data, $id);
        }

        return false;
    }
}
