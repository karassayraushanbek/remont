<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Admin\StoreAdminRequest;
use App\Http\Requests\Admin\Admin\UpdateAdminRequest;
use App\Http\Requests\Admin\Admin\UpdateAdminStatusRequest;
use App\Services\Admin\Admin\AdminService;
use App\Services\Admin\Role\RoleService;
use App\Models\Admin;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct(protected AdminService $adminService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-администраторы', ['only' => ['index','show']]);
        $this->middleware('permission:создание-администратора', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-администратора', ['only' => ['edit','update', 'updateAdminStatus']]);
        $this->middleware('permission:удаление-администратора', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $admin = $this->adminService->getAdmins();

        return view('admin.admin.index',compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $roles = (new RoleService())->getRoleNamesNames();

        return view('admin.admin.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAdminRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $adminEmail = config('mail.from.address');
            $m = ' Логин и Пароль отправлен на эту почту ' . $adminEmail;
            $this->adminService->createAdmin($data, $adminEmail);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.admin.index')->with('success', config('sessionmessages.successfully_added') . $m);
    }

    /**
     * Display the specified resource.
     */
    public function show(Admin $admin): View
    {
        return view('admin.admin.show', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Admin $admin): View
    {
        $roles = (new RoleService())->getRoleNamesNames();

        $adminRole = $this->adminService->getAdminRoles($admin);

        return view('admin.admin.edit', compact('admin', 'roles', 'adminRole'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAdminRequest $request, Admin $admin): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $adminEmail = config('mail.from.address');
            $m = $this->adminService->updateAdmin($admin, $data, $adminEmail);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.admin.index')->with('success', config('sessionmessages.successfully_updated') . $m);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Admin $admin): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->adminService->deleteAdmin($admin);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.admin.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updateAdminStatus(UpdateAdminStatusRequest $request, string $id): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->adminService->updateAdminStatus($id, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.admin.index')->with('success', config('sessionmessages.successfully_updated'));
    }
}
