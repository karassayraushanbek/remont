<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Phone\StorePhoneRequest;
use App\Http\Requests\Admin\Phone\UpdatePhoneRequest;

use App\Models\Phone;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Phone\PhoneService;

class PhoneController extends Controller
{
    public function __construct(protected PhoneService $phoneService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-номер-тел', ['only' => ['index','show']]);
        $this->middleware('permission:создание-номер-тел', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-номер-тел', ['only' => ['edit','update']]);
        $this->middleware('permission:удаление-номер-тел', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $phones = $this->phoneService->getPhones();

        return view('admin.phones.index', compact('phones'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.phones.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePhoneRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->phoneService->createPhone($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.phones.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Phone $phone): View
    {
        return view('admin.phones.show', compact('phone'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Phone $phone): View
    {
        return view('admin.phones.edit', compact('phone'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePhoneRequest $request, Phone $phone): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->phoneService->updatePhone($phone, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.phones.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Phone $phone): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->phoneService->deletePhone($phone);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.phones.index')->with('success', config('sessionmessages.successfully_deleted'));
    }
}
