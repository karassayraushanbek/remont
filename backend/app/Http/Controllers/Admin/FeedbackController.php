<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Feedback\StoreFeedbackRequest;
use App\Http\Requests\Admin\Feedback\UpdateFeedbackRequest;

use App\Models\Feedback;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Feedback\FeedbackService;

class FeedbackController extends Controller
{
    public function __construct(protected FeedbackService $feedbackService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-заявки', ['only' => ['index','show']]);
        $this->middleware('permission:создание-заявки', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-заявки', ['only' => ['edit','update']]);
        $this->middleware('permission:удаление-заявки', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $feedbacks = $this->feedbackService->getFeedbacks();

        return view('admin.feedbacks.index', compact('feedbacks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.feedbacks.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFeedbackRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->feedbackService->createFeedback($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.feedbacks.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Feedback $feedback): View
    {
        return view('admin.feedbacks.show', compact('feedback'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Feedback $feedback): View
    {
        return view('admin.feedbacks.edit', compact('feedback'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFeedbackRequest $request, Feedback $feedback): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->feedbackService->updateFeedback($feedback, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.feedbacks.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Feedback $feedback): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->feedbackService->deleteFeedback($feedback);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.feedbacks.index')->with('success', config('sessionmessages.successfully_deleted'));
    }
}
