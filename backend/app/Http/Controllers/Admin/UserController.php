<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\User\StoreUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;

use App\Http\Requests\Admin\User\UpdateUserStatusRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\User\UserService;

class UserController extends Controller
{
    public function __construct(protected UserService $userService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-пользователи', ['only' => ['index','show']]);
        $this->middleware('permission:создание-пользователя', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-пользователя', ['only' => ['edit','update', 'updateUserStatus']]);
        $this->middleware('permission:удаление-пользователя', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $users = $this->userService->getUsers();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $adminEmail = config('mail.from.address');
            $m = ' Логин и Пароль отправлен на эту почту ' . $adminEmail;
            $this->userService->createUser($data, $adminEmail);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.users.index')->with('success', config('sessionmessages.successfully_added') . $m);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user): View
    {
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user): View
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, User $user): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $adminEmail = config('mail.from.address');
            $m = $this->userService->updateUser($user, $data, $adminEmail);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.users.index')->with('success', config('sessionmessages.successfully_updated') . $m);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->userService->deleteUser($user);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.users.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updateUserStatus(UpdateUserStatusRequest $request, string $id): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->userService->updateUserStatus($id, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.users.index')->with('success', config('sessionmessages.successfully_updated'));
    }
}
