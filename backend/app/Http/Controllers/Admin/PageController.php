<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Page\StorePageRequest;
use App\Http\Requests\Admin\Page\UpdatePagePositionRequest;
use App\Http\Requests\Admin\Page\UpdatePageRequest;

use App\Models\Page;
use App\Services\Admin\Menu\MenuService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Page\PageService;

class PageController extends Controller
{
    public function __construct(protected PageService $pageService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-страницы', ['only' => ['index','show']]);
        $this->middleware('permission:создание-страницы', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-страницы', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-страницы', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $pages = $this->pageService->getPages();

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $langs = $this->pageService->getLangs();
        $menus = (new MenuService())->getMenuCollection();

        return view('admin.pages.create', compact('langs', 'menus'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePageRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->pageService->createPage($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.pages.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Page $page): View
    {
        return view('admin.pages.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Page $page): View
    {
        $langs = $this->pageService->getLangs();
        $menus = (new MenuService())->getMenuCollection();

        return view('admin.pages.edit', compact('page', 'langs', 'menus'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePageRequest $request, Page $page): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->pageService->updatePage($page, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.pages.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Page $page): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->pageService->deletePage($page);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.pages.index')->with('success', config('sessionmessages.successfully_deleted'));
    }

    public function updatePosition(UpdatePagePositionRequest $request, string $id): int|bool
    {
        $data = $request->validated();

        if (isset($data['position']) && !empty($data['position'])) {
            $this->pageService->updatePagePosition($data, $id);
        }

        return false;
    }
}
