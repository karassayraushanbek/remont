<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\Banner\StoreBannerRequest;
use App\Http\Requests\Admin\Banner\UpdateBannerRequest;

use App\Models\Banner;
use App\Models\Page;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\Banner\BannerService;

class BannerController extends Controller
{
    public function __construct(protected BannerService $bannerService)
    {
        $this->middleware('auth:admin');
        $this->middleware('permission:все-баннеры', ['only' => ['index','show']]);
        $this->middleware('permission:создание-баннеры', ['only' => ['create','store']]);
        $this->middleware('permission:редактирование-баннеры', ['only' => ['edit','update', 'updatePosition']]);
        $this->middleware('permission:удаление-баннеры', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Page $page): View
    {
        $banners = $this->bannerService->getBanners($page);

        return view('admin.banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Page $page): View
    {
        $langs = $this->bannerService->getLangs();

        return view('admin.banners.create', compact('langs', 'page'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Page $page, StoreBannerRequest $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->bannerService->createBanner($page, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.banners.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Page $page, Banner $banner): View
    {
        return view('admin.banners.show', compact('banner', 'page'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Page $page, Banner $banner): View
    {
        $langs = $this->bannerService->getLangs();

        return view('admin.banners.edit', compact('page', 'banner', 'langs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Page $page, UpdateBannerRequest $request, Banner $banner): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->bannerService->updateBanner($page, $banner, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->back()->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Page $page, Banner $banner): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->bannerService->deleteBanner($page, $banner);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('admin.banners.index')->with('success', config('sessionmessages.successfully_deleted'));
    }
}
