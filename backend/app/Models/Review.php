<?php

namespace App\Models;

use App\Contracts\HasDescriptionContract;
use App\Traits\HasDescriptionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\Review
 *
 * @property int $id
 * @property int $blog_id
 * @property string $image
 * @property string $name
 * @property int $description_tr
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $description
 * @property-read \App\Models\Blog $blog
 */

class Review extends Model implements HasDescriptionContract
{
    use HasFactory, HasDescriptionTrait;

    const IMAGE_PATH = 'images/reviews';

    protected $table = 'reviews';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function blog(): BelongsTo
    {
        return $this->belongsTo(Blog::class);
    }

    public function getImageUrlAttribute(): string|null
    {
        return $this->image ? Storage::disk('custom')->url(self::IMAGE_PATH . '/' . $this->image) : '';
    }
}
