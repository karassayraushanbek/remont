<?php

namespace App\Models;

use App\Contracts\HasTitleContract;
use App\Contracts\HasDescriptionContract;
use App\Traits\HasTitleTrait;
use App\Traits\HasDescriptionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\DescriptionService
 *
 * @property int $id
 * @property int $tag_description_service_id
 * @property int $title_tr
 * @property int $description_tr
 * @property string $phone
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $title
 * @property-read \App\Models\Translate $description
 * @property-read \App\Models\TagDescriptionService $tagDescriptionService
 */

class DescriptionService extends Model implements HasTitleContract, HasDescriptionContract
{
    use HasFactory, HasTitleTrait, HasDescriptionTrait;

    protected $table = 'description_services';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function tagDescriptionService(): BelongsTo
    {
        return $this->belongsTo(TagDescriptionService::class);
    }
}
