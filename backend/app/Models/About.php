<?php

namespace App\Models;

use App\Contracts\HasTitleContract;
use App\Contracts\HasDescriptionContract;
use App\Traits\HasTitleTrait;
use App\Traits\HasDescriptionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\About
 *
 * @property int $id
 * @property string $image
 * @property int $title_tr
 * @property int $description_tr
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $title
 * @property-read \App\Models\Translate $description
 */

class About extends Model implements HasTitleContract, HasDescriptionContract
{
    use HasFactory, HasTitleTrait, HasDescriptionTrait;

    const IMAGE_PATH = 'images/abouts';

    protected $appends = [
        'image_url'
    ];

    protected $table = 'abouts';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function getImageUrlAttribute(): string|null
    {
        return $this->image ? Storage::disk('custom')->url(self::IMAGE_PATH . '/' . $this->image) : '';
    }
}
