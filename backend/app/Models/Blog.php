<?php

namespace App\Models;

use App\Contracts\HasTitleContract;
use App\Contracts\HasDescriptionContract;
use App\Contracts\HasMetaContract;
use App\Traits\HasTitleTrait;
use App\Traits\HasDescriptionTrait;
use App\Traits\HasMetaTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\Blog
 *
 * @property int $id
 * @property int $tag_id
 * @property string $image
 * @property int $title_tr
 * @property int $description_tr
 * @property int $read
 * @property int $meta_title_tr
 * @property int $meta_description_tr
 * @property int $meta_keywords_tr
 * @property string $slug
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $title
 * @property-read \App\Models\Translate $description
 * @property-read \App\Models\Translate $meta_title
 * @property-read \App\Models\Translate $meta_description
 * @property-read \App\Models\Translate $meta_keywords
 * @property-read \App\Models\Tag $tag
 */

class Blog extends Model implements HasTitleContract, HasDescriptionContract, HasMetaContract
{
    use HasFactory, HasTitleTrait, HasDescriptionTrait, HasMetaTrait;

    const IMAGE_PATH = 'images/blogs';

    protected $table = 'blogs';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function tag(): BelongsTo
    {
        return $this->belongsTo(Tag::class);
    }

    public function getImageUrlAttribute(): string|null
    {
        return $this->image ? Storage::disk('custom')->url(self::IMAGE_PATH . '/' . $this->image) : '';
    }
}
