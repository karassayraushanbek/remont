<?php

namespace App\Models;

use App\Contracts\HasTitleContract;
use App\Contracts\HasDescriptionContract;
use App\Contracts\HasMetaContract;
use App\Traits\HasTitleTrait;
use App\Traits\HasDescriptionTrait;
use App\Traits\HasMetaTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property int $menu_id
 * @property int $title_tr
 * @property int $description_tr
 * @property int $meta_title_tr
 * @property int $meta_description_tr
 * @property int $meta_keywords_tr
 * @property string $slug
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $title
 * @property-read \App\Models\Translate $description
 * @property-read \App\Models\Translate $meta_title
 * @property-read \App\Models\Translate $meta_description
 * @property-read \App\Models\Translate $meta_keywords
 * @property-read \App\Models\Menu $menu
 */

class Page extends Model implements HasTitleContract, HasDescriptionContract, HasMetaContract
{
    use HasFactory, HasTitleTrait, HasDescriptionTrait, HasMetaTrait;

    protected $table = 'pages';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class);
    }
}
