<?php

namespace App\Models;

use App\Contracts\HasTitleContract;
use App\Traits\HasTitleTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\Certificate
 *
 * @property int $id
 * @property string $image
 * @property int $title_tr
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $title
 */

class Certificate extends Model implements HasTitleContract
{
    use HasFactory, HasTitleTrait;

    const IMAGE_PATH = 'images/certificates';

    protected $appends = [
        'image_url'
    ];

    protected $table = 'certificates';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function getImageUrlAttribute(): string|null
    {
        return $this->image ? Storage::disk('custom')->url(self::IMAGE_PATH . '/' . $this->image) : '';
    }
}
