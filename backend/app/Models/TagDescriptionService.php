<?php

namespace App\Models;

use App\Contracts\HasTitleContract;
use App\Traits\HasTitleTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TagDescriptionService
 *
 * @property int $id
 * @property int $title_tr
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $title
 */

class TagDescriptionService extends Model implements HasTitleContract
{
    use HasFactory, HasTitleTrait;

    protected $table = 'tag_description_services';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
