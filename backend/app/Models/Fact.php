<?php

namespace App\Models;

use App\Contracts\HasTitleContract;
use App\Traits\HasTitleTrait;
use App\Contracts\HasDescriptionContract;
use App\Traits\HasDescriptionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\Fact
 *
 * @property int $id
 * @property int $number
 * @property string $image
 * @property int $title_tr
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $title
 */

class Fact extends Model implements HasTitleContract, HasDescriptionContract
{
    use HasFactory, HasTitleTrait, HasDescriptionTrait;

    const IMAGE_PATH = 'images/facts';

    protected $appends = [
        'image_url'
    ];

    protected $table = 'facts';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function alt_title(): BelongsTo
    {
        return $this->belongsTo(Translate::class, 'alt_title_tr');
    }

    public function getImageUrlAttribute(): string|null
    {
        return $this->image ? Storage::disk('custom')->url(self::IMAGE_PATH . '/' . $this->image) : '';
    }
}
