<?php

namespace App\Models;

use App\Contracts\HasTitleContract;
use App\Contracts\HasDescriptionContract;
use App\Traits\HasTitleTrait;
use App\Traits\HasDescriptionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\Master
 *
 * @property int $id
 * @property string $image
 * @property string $name
 * @property string $reviews
 * @property int $title_tr
 * @property int $description_tr
 * @property boolean $is_published
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Translate $title
 * @property-read \App\Models\Translate $description
 */

class Master extends Model implements HasTitleContract, HasDescriptionContract
{
    use HasFactory, HasTitleTrait, HasDescriptionTrait;

    const IMAGE_PATH = 'images/masters';

    protected $table = 'masters';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function tagMasters(): BelongsToMany
    {
        return $this->belongsToMany(TagMaster::class, 'tag_master_masters');
    }

    public function getImageUrlAttribute(): string|null
    {
        return $this->image ? Storage::disk('custom')->url(self::IMAGE_PATH . '/' . $this->image) : '';
    }
}
