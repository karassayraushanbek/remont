<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\TagMasterMaster
 *
 * @property int $id
 * @property int $tag_master_id
 * @property int $master_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */

class TagMasterMaster extends Model
{
    use HasFactory;

    protected $table = 'tag_master_masters';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function masters(): BelongsToMany
    {
        return $this->belongsToMany(Master::class, 'tag_master_masters');
    }
}
