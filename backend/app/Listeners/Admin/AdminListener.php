<?php

namespace App\Listeners\Admin;

use App\Mail\AdminMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class AdminListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $admin = $event->admin;
        $password = $event->password;
        $adminEmail = $event->adminEmail;
        $subject = $event->subject;

        Mail::to($adminEmail)->send(new AdminMail($admin, $password, $subject));
    }
}
