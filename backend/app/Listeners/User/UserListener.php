<?php

namespace App\Listeners\User;

use App\Mail\UserMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class UserListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $user = $event->user;
        $password = $event->password;
        $adminEmail = $event->adminEmail;
        $subject = $event->subject;

        Mail::to($adminEmail)->send(new UserMail($user, $password, $subject));
    }
}
