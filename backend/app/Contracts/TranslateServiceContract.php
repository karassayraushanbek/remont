<?php

namespace App\Contracts;

use App\Models\Translate;

interface TranslateServiceContract
{
    public function createTranslation(string $string): Translate;

    public function updateTranslation(int $id, string $string, string $lang): Translate;

    public function deleteTranslation(int $id): Translate;

    public function getLanguages(): array;
}
