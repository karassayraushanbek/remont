<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasDescriptionContract
{
    public function description(): BelongsTo;
}
