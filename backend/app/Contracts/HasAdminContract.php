<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasAdminContract
{
    public function admin(): BelongsTo;
}
