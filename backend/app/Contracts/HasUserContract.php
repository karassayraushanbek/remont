<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasUserContract
{
    public function user(): BelongsTo;
}
