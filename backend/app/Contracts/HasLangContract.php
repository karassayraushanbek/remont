<?php

namespace App\Contracts;

interface HasLangContract
{
    public function getLangs(): array;
}
