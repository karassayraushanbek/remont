<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasTitleContract
{
    public function title(): BelongsTo;
}
