<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasMetaContract
{
    public function meta_description(): BelongsTo;

    public function meta_keywords(): BelongsTo;

    public function meta_title(): BelongsTo;
}
