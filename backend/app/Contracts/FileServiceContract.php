<?php

namespace App\Contracts;

interface FileServiceContract
{
    public function deleteFile(string $name, string $path): mixed;

    public function saveFile(object $file, string $path, string $oldFileName = null): mixed;
}
