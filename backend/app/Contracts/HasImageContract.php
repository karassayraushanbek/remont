<?php

namespace App\Contracts;

interface HasImageContract
{
    public function getImageUrlAttribute(): string|null;
}
