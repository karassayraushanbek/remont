<?php

namespace DummyNamespace;

use DummyRootNamespaceHttp\Controllers\Controller;

use App\Http\Requests\Admin\{{modelName}}\Store{{modelName}}Request;
use App\Http\Requests\Admin\{{modelName}}\Update{{modelName}}Request;

use DummyRootNamespace{{modelNamespace}}Models\{{modelName}};
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Admin\{{modelName}}\{{modelName}}Service;

class DummyClass extends Controller
{
    public function __construct(protected {{modelName}}Service ${{crudNameSingular}}Service)
    {

    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        ${{crudName}} = $this->{{crudNameSingular}}Service->get{{modelName}}s();

        return view('{{viewPath}}{{viewName}}.index', compact('{{crudName}}'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('{{viewPath}}{{viewName}}.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Store{{modelName}}Request $request): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->{{crudNameSingular}}Service->create{{modelName}}($data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('{{viewPath}}{{viewName}}.index')->with('success', config('sessionmessages.successfully_added'));
    }

    /**
     * Display the specified resource.
     */
    public function show({{modelName}} ${{crudNameSingular}}): View
    {
        return view('{{viewPath}}{{viewName}}.show', compact('{{crudNameSingular}}'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit({{modelName}} ${{crudNameSingular}}): View
    {
        return view('{{viewPath}}{{viewName}}.edit', compact('{{crudNameSingular}}'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Update{{modelName}}Request $request, {{modelName}} ${{crudNameSingular}}): RedirectResponse
    {
        $data = $request->validated();

        DB::beginTransaction();
        try {
            $this->{{crudNameSingular}}Service->update{{modelName}}(${{crudNameSingular}}, $data);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withInput()->withErrors($exception->getMessage());
        }

        return redirect()->route('{{viewPath}}{{viewName}}.index')->with('success', config('sessionmessages.successfully_updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy({{modelName}} ${{crudNameSingular}}): RedirectResponse
    {
        DB::beginTransaction();
        try {
            $this->{{crudNameSingular}}Service->delete{{modelName}}(${{crudNameSingular}});

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            return back()->withErrors($exception->getMessage());
        }

        return redirect()->route('{{viewPath}}{{viewName}}.index')->with('success', config('sessionmessages.successfully_deleted'));
    }
}
