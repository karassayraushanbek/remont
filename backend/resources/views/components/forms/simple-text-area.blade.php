@props(['model', 'name', 'label', 'editor'])

<div class="form-group {{ $errors->has($name) ? 'has-error' : ''}}">
    <label for="{{$name}}" class="control-label">{{ $label }}</label>
    <textarea class="form-control @if($editor == 1) ckeditor @endif" name="{{$name}}" id="{{$name}}" rows="4">{{ isset($model->{$name}) ? $model->{$name} : old($name) }}</textarea>
    {!! $errors->first($name, '<p class="text-danger">:message</p>') !!}
</div>
