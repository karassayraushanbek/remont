@props(['model', 'name', 'label', 'lang', 'editor'])

<div class="form-group {{ $errors->has($name . '.' . $lang) ? 'has-error' : ''}}">
    <label for="{{$name}}_{{ $lang }}" class="control-label">{{ $label . ' ' . strtoupper($lang) }}</label>
    <textarea class="form-control @if($editor == 1) ckeditor @endif" name="{{$name}}[{{ $lang }}]" id="{{$name}}_{{ $lang }}" rows="4">{{ isset($model->{$name}->{$lang}) ? $model->{$name}->{$lang} : old($name . $lang) }}</textarea>
    {!! $errors->first($name . '.' . $lang, '<p class="text-danger">:message</p>') !!}
</div>
