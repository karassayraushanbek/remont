@props(['collection', 'cname', 'clang', 'model', 'name', 'label'])

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $name }}" class="control-label">{{ $label }}</label>
    <select name="{{ $name }}" id="{{ $name }}" class="form-control select2" required>
        @forelse ($collection as $item)
            <option value="{{ $item->id }}" @if(isset($model->{$name}) && $item->id == $model->{$name}) selected @endif>
                @if($clang == 1)
                    <b>{{ $item->{$cname}->ru }}</b>
                @else
                    <b>{{ $item->{$cname} }}</b>
                @endif
            </option>
        @empty
            <option disabled>{{ $label }} не найдено</option>
        @endforelse
    </select>
    {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
</div>
