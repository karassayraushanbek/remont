@props(['model', 'name', 'label', 'on', 'off'])

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $name }}" class="control-label">{{ $label }}</label>
    <select name="{{ $name }}" id="{{ $name }}" class="form-control">
        <option value="0" @if (isset($model->{$name}) && $model->{$name} == 0) selected @endif>{{ $off }}</option>
        <option value="1" @if (isset($model->{$name}) && $model->{$name} == 1) selected @endif>{{ $on }}</option>
    </select>
    {!! $errors->first($name, '<p class="text-danger">:message</p>') !!}
</div>
