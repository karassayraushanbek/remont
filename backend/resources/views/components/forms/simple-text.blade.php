@props(['model', 'name', 'label'])

<div class="form-group {{ $errors->has($name) ? 'has-error' : ''}}">
    <label for="{{ $name }}" class="control-label">{{ $label }}</label>
    <input class="form-control" name="{{ $name }}" type="text" id="{{ $name }}" value="{{ isset($model->{$name}) ? $model->{$name} : old($name)}}" >
    {!! $errors->first($name, '<p class="text-danger">:message</p>') !!}
</div>
<hr>
