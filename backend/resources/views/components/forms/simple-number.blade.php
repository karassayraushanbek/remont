@props(['model', 'name', 'label', 'step'])

<div class="form-group {{ $errors->has($name) ? 'has-error' : ''}}">
    <label for="{{ $name }}" class="control-label">{{ $label }}</label>
    <input class="form-control" name="{{ $name }}" type="number" id="{{ $name }}" value="{{ isset($model->{$name}) ? $model->{$name} : old($name)}}" @if($step == 1) step="any" @endif>
    {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
</div>
