@props(['model', 'name', 'label', 'path', 'delete'])

<div class="form-group {{ $errors->has($name) ? 'has-error' : ''}}">
    <label for="{{ $name }}" class="control-label">{{ $label }}</label>
    <input class="form-control @error($name) is-invalid @enderror photo"
           name="{{ $name }}" type="file" id="{{ $name }}" accept="{{ $name }}/*">
    {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
    <br>
    @if(isset($model) && $model->{$path} != "")
        @if(
            strpos($model->{$path}, '.png') !== false ||
            strpos($model->{$path}, '.jpg') !== false ||
            strpos($model->{$path}, '.svg') !== false ||
            strpos($model->{$path}, '.jpeg') !== false ||
            strpos($model->{$path}, '.gif') !== false
        )
            <a href="{{ $model->{$path} }}" data-fancybox="image">
                <img id="image-preview-{{ $name }}" class="rounded" src="{{ $model->{$path} }}" style="max-width: 300px;" alt="{{ $name }}">
            </a>
            @if(isset($delete) && $delete == true)
                <br>
                <a class="delete-photo btn btn-danger btn-sm mt-2" data-photo-id="{{ $model->id }}" data-photo-name="{{ $name }}">Удалить</a>
            @endif
        @else
            <video width="320" height="240" controls>
                <source src="{{ $model->{$path} }}" type="video/mp4">
                <source src="{{ $model->{$path} }}" type="video/ogg">
            </video>
            @if(isset($delete) && $delete == true)
                <br>
                <a class="delete-photo btn btn-danger btn-sm mt-2" data-photo-id="{{ $model->id }}" data-photo-name="image">Удалить</a>
            @endif
        @endif
    @else
        <img id="image-preview-{{ $name }}" src="" class="rounded" style="display: none; max-width: 300px;" alt="{{ $name }}">
    @endif
</div>
