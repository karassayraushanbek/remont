@props(['model' => null, 'name', 'label', 'lang'])

<div class="form-group {{ $errors->has($name . '.' . $lang) ? 'has-error' : ''}}">
    <label for="{{ $name }}_{{ $lang }}" class="control-label">
        {{ $label . ' ' . strtoupper($lang) }}
    </label>
    <input class="form-control"
           name="{{ $name }}[{{ $lang }}]" type="text" id="{{ $name }}_{{ $lang }}"
           value="{{ isset($model?->{$name}->{$lang}) ? $model?->{$name}->{$lang} : old($name . '.' . $lang)}}"
    >
    {!! $errors->first($name . '.' . $lang, '<p class="text-danger">:message</p>') !!}
</div>
