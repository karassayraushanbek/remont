@extends('adminlte::page')

@section('title', 'Просмотр О нас')

@section('content_header')
    <h1>Просмотр О нас</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.abouts.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-о-нас')
                                <a href="{{ route('admin.abouts.edit', ['about' => $about]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-о-нас')
                                <form method="POST" action="{{ route('admin.abouts.destroy', ['about' => $about]) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $about->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $about->title?->ru }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $about->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Изображение </th>
                                        <td>
                                            <a href="{{ $about->image_url }}" data-fancybox="image">
                                                <img id="image-preview" class="rounded" src="{{ $about->image_url }}" style="max-width: 300px;" alt="{{ $about->title?->ru }}">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Заголовок </th>
                                        <td> {{ $about->title?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Описание </th>
                                        <td> {{ $about->description?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($about->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $about->position }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $about->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            Fancybox.bind('[data-fancybox="image"]', {
                keyboard: true,
            });
        });
    </script>
@stop

@section('plugins.fancybox', true)
