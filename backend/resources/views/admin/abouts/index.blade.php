@extends('adminlte::page')

@section('title', 'О нас')

@section('content_header')
    <h1>О нас</h1>
@stop

@section('content')

    @include('components.alert')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            @can('создание-о-нас')
                                <a href="{{ route('admin.abouts.create') }}" class="btn btn-success btn-sm" title="Добавить">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Добавить
                                </a>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive" id="tblSort">
                            <table id="tblData" class="table table-bordered table-striped dataTable dtr-inline">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Изображение</th>
                                        <th>Статус</th>
                                        <th>Действие</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                    @forelse($abouts as $item)
                                        <tr>
                                            <td data-id="{{$item->id}}">
                                                {{$abouts->perPage()*($abouts->currentPage()-1)+$count}}
                                            </td>
                                            <td>
                                                {{ $item->id }}
                                            </td>
                                            <td>
                                                <img src="{{ $item->image_url }}" class="rounded" style="max-width: 300px; height: 100px">
                                            </td>
                                            <td>
                                                @if($item->is_published == 1)
                                                    <span class="badge rounded-pill bg-success">
                                                        Опубликована
                                                    </span>
                                                @else
                                                    <span class="badge rounded-pill bg-danger">
                                                        Не опубликована
                                                    </span>
                                                @endif
                                            </td>
                                            <td>
                                                @can('все-о-нас')
                                                    <a href="{{ route('admin.abouts.show', ['about' => $item]) }}" class="btn btn-info btn-sm mb-2" title="Просмотр">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                @endcan
                                                @can('редактирование-о-нас')
                                                    <a href="{{ route('admin.abouts.edit', ['about' => $item]) }}" class="btn btn-primary btn-sm mb-2" title="Редактировать">
                                                        <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                                                    </a>
                                                @endcan
                                                @can('удаление-о-нас')
                                                    <form method="POST" action="{{ route('admin.abouts.destroy', ['about' => $item]) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm mb-2" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $item->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $item->title?->ru }}?&quot;)">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </button>
                                                    </form>
                                                @endcan
                                            </td>
                                        </tr>
                                        @php
                                            $count++;
                                        @endphp
                                    @empty
                                        <tr>
                                            <td align="center" class="text-danger" colspan="5">
                                                О нас не найдены
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Изображение</th>
                                        <th>Статус</th>
                                        <th>Действие</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="pagination-wrapper">
                                @if(is_object($abouts))
                                    {!! $abouts->links() !!}
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')

@stop

@section('js')
    <script>
        $(document).ready(function () {
            //console.log($(1).find('td:eq(0)').data('id'));
            $("#tblSort tbody").sortable({
                cursor: "move",
                placeholder: "sortable-placeholder",
                update: function (event, ui) {
                    $(".table tbody tr").each((item, i) => {
                        $(i).find('td:eq(0)').html(item + 1)
                        let position = $(i).find('td:eq(0)').data('id')
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            url: `/admin/abouts/position/${position}/update`,
                            data: {position: item + 1}
                        })
                        .done(function (msg) {

                        });
                    });
                },
                helper: function (e, tr) {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function (index) {
                        $(this).width($originals.eq(index).width());
                    });
                    return $helper;
                }
            }).disableSelection();
        });
    </script>
@stop

@section('plugins.jqueryUi', true)
