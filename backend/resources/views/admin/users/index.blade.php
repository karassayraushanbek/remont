@extends('adminlte::page')

@section('title', 'Пользователи')

@section('content_header')
    <h1>Пользователи</h1>
@stop

@section('content')

    @include('components.alert')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            @can('создание-пользователя')
                                <a href="{{ route('admin.users.create') }}" class="btn btn-success btn-sm" title="Добавить">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Добавить
                                </a>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive" id="tblSort">
                            <table id="tblData" class="table table-bordered table-striped dataTable dtr-inline">
                                <thead>
                                    <tr>
                                        <form>
                                            <th colspan="3">
                                                <div class="input-group input-group-sm">
                                                    <input type="text" name="search[name]" class="form-control float-right" placeholder="Название" value="{{request()->get('search')['name'] ?? ''}}">
                                                </div>
                                            </th>
                                            <th colspan="2">
                                                <div class="input-group input-group-sm">
                                                    <select name="search[status]" class="form-control float-right">
                                                        <option value="1" {{ (isset(request()->get('search')['status']) && request()->get('search')['status'] == 1) ? 'selected' : '' }}>Активирован</option>
                                                        <option value="0" {{ (isset(request()->get('search')['status']) && request()->get('search')['status'] == 0) ? 'selected' : '' }}>Деактивирован</option>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button type="submit" class="btn btn-default">
                                                            <i class="fas fa-search"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <a href="{{ route('admin.users.index') }}" title="Сбросить поиск" class="btn btn-success btn-sm">
                                                    <i class="fa fa-spinner" aria-hidden="true"></i> Сбросить поиск
                                                </a>
                                            </th>
                                        </form>
                                    </tr>
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Имя</th>
                                        <th>Email</th>
                                        <th>Статус</th>
                                        <th>Действие</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                    @forelse($users as $item)
                                        <tr>
                                            <td>
                                                {{$users->perPage()*($users->currentPage()-1)+$count}}
                                            </td>
                                            <td data-id="{{$item->id}}">
                                                {{ $item->id }}
                                            </td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>
                                                @if($item->status == 1)
                                                    <span class="badge rounded-pill bg-success">Активирован</span>
                                                @else
                                                    <span class="badge rounded-pill bg-danger">Деактивирован</span>
                                                @endif
                                            </td>
                                            <td>
                                                @can('все-пользователи')
                                                    <form method="POST" action="{{ route('admin.users.update.status', $item->id) }}" accept-charset="UTF-8" style="display:inline;">
                                                        {{ method_field('PATCH') }}
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="status" value="{{ $item->status == 1 ? 0 : 1 }}">
                                                        <button type="submit" class="btn btn-{{ $item->status == 1 ? 'danger' : 'success' }} btn-sm mb-2" title="@if($item->status == 1) Деактивировать @else Активировать @endif">
                                                            @if($item->status == 1)
                                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                                            @else
                                                                <i class="fa fa-lock-open" aria-hidden="true"></i>
                                                            @endif
                                                        </button>
                                                    </form>

                                                    <a href="{{ route('admin.users.show', ['user' => $item]) }}" class="btn btn-info btn-sm mb-2" title="Просмотр">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                @endcan
                                                @can('редактирование-пользователя')
                                                    <a href="{{ route('admin.users.edit', ['user' => $item]) }}" class="btn btn-primary btn-sm mb-2" title="Редактировать">
                                                        <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                                                    </a>
                                                @endcan
                                                @can('удаление-пользователя')
                                                    <form method="POST" action="{{ route('admin.users.destroy', ['user' => $item]) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm mb-2" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $item->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $item->name }}?&quot;)">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </button>
                                                    </form>
                                                @endcan
                                            </td>
                                        </tr>
                                        @php
                                            $count++;
                                        @endphp
                                    @empty
                                        <tr>
                                            <td align="center" class="text-danger" colspan="6">
                                                Пользователи не найдены
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Имя</th>
                                        <th>Email</th>
                                        <th>Статус</th>
                                        <th>Действие</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="pagination-wrapper">
                                @if(is_object($users))
                                    {!! $users->links() !!}
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')

@stop

@section('js')

@stop


