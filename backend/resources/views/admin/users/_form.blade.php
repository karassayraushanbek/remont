<x-forms.simple-text :model="$user ?? null" name="name" label="Имя" />

<x-forms.simple-text :model="$user ?? null" name="email" label="Email" />

<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">@if($formMode == 'create') {{ 'Пароль' }} @else {{ 'Обновить Пароль' }} @endif</label>
    <input class="form-control" name="password" type="text" id="password">
    {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}
</div>
<hr>

@if($formMode == 'create')
    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
        <label for="password_confirmation" class="control-label">{{ 'Повторить Пароль' }}</label>
        <input class="form-control" name="password_confirmation" type="text" id="password_confirmation">
        {!! $errors->first('password_confirmation', '<p class="text-danger">:message</p>') !!}
    </div>
    <hr>
@endif

@if($formMode == 'edit')
    <input name="user_id" type="hidden" value="{{$user->id}}">
@endif

<x-forms.simple-button :formMode="$formMode" />
