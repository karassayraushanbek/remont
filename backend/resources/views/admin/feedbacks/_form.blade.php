<x-forms.simple-text :model="$feedback ?? null" name="name" label="Имя" />

<x-forms.simple-text :model="$feedback ?? null" name="phone" label="Номер телефона" />

<x-forms.simple-button :formMode="$formMode" />
