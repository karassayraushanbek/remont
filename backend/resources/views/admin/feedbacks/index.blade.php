@extends('adminlte::page')

@section('title', 'Заявки')

@section('content_header')
    <h1>Заявки</h1>
@stop

@section('content')

    @include('components.alert')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
{{--                            @can('создание-заявки')--}}
{{--                                <a href="{{ route('admin.feedbacks.create') }}" class="btn btn-success btn-sm" title="Добавить">--}}
{{--                                    <i class="fa fa-plus" aria-hidden="true"></i> Добавить--}}
{{--                                </a>--}}
{{--                            @endcan--}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive" id="tblSort">
                            <table id="tblData" class="table table-bordered table-striped dataTable dtr-inline">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Имя</th>
                                        <th>Номер телефона</th>
                                        <th>Дата</th>
                                        <th>Действие</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                    @forelse($feedbacks as $item)
                                        <tr @if(date('d.m.Y') == $item->created_at->format('d.m.Y')) class="bg-success" @endif>
                                            <td data-id="{{$item->id}}">
                                                {{$feedbacks->perPage()*($feedbacks->currentPage()-1)+$count}}
                                            </td>
                                            <td>
                                                {{ $item->id }}
                                            </td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->phone }}</td>
                                            <td>{{ $item->created_at?->format('d.m.Y H:s') }}</td>
                                            <td>
                                                @can('все-заявки')
                                                    <a href="{{ route('admin.feedbacks.show', ['feedback' => $item]) }}" class="btn btn-info btn-sm mb-2" title="Просмотр">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                @endcan
{{--                                                @can('редактирование-заявки')--}}
{{--                                                    <a href="{{ route('admin.feedbacks.edit', ['feedback' => $item]) }}" class="btn btn-primary btn-sm mb-2" title="Редактировать">--}}
{{--                                                        <i class="fa fa-fw fa-edit" aria-hidden="true"></i>--}}
{{--                                                    </a>--}}
{{--                                                @endcan--}}
{{--                                                @can('удаление-заявки')--}}
{{--                                                    <form method="POST" action="{{ route('admin.feedbacks.destroy', ['feedback' => $item]) }}" accept-charset="UTF-8" style="display:inline">--}}
{{--                                                        {{ method_field('DELETE') }}--}}
{{--                                                        {{ csrf_field() }}--}}
{{--                                                        <button type="submit" class="btn btn-danger btn-sm mb-2" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $item->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $item->name }}?&quot;)">--}}
{{--                                                            <i class="fa fa-trash" aria-hidden="true"></i>--}}
{{--                                                        </button>--}}
{{--                                                    </form>--}}
{{--                                                @endcan--}}
                                            </td>
                                        </tr>
                                        @php
                                            $count++;
                                        @endphp
                                    @empty
                                        <tr>
                                            <td align="center" class="text-danger" colspan="6">
                                                Заявки не найдены
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Имя</th>
                                        <th>Номер телефона</th>
                                        <th>Дата</th>
                                        <th>Действие</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="pagination-wrapper">
                                @if(is_object($feedbacks))
                                    {!! $feedbacks->links() !!}
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')

@stop

@section('js')

@stop


