@extends('adminlte::page')

@section('title', 'Просмотр Заявки')

@section('content_header')
    <h1>Просмотр Заявки</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.feedbacks.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
{{--                            @can('редактирование-заявки')--}}
{{--                                <a href="{{ route('admin.feedbacks.edit', ['feedback' => $feedback]) }}" class="btn btn-primary" title="Редактировать">--}}
{{--                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать--}}
{{--                                </a>--}}
{{--                            @endcan--}}
{{--                            @can('удаление-заявки')--}}
{{--                                <form method="POST" action="{{ route('admin.feedbacks.destroy', ['feedback' => $feedback]) }}" accept-charset="UTF-8" style="display:inline">--}}
{{--                                    {{ method_field('DELETE') }}--}}
{{--                                    {{ csrf_field() }}--}}
{{--                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $feedback->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $feedback->name }}?&quot;)">--}}
{{--                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить--}}
{{--                                    </button>--}}
{{--                                </form>--}}
{{--                            @endcan--}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $feedback->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Имя </th>
                                        <td> {{ $feedback->name }} </td>
                                    </tr>
                                    <tr>
                                        <th> Номер телефона </th>
                                        <td> {{ $feedback->phone }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $feedback->created_at?->format('d.m.Y H:s') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
