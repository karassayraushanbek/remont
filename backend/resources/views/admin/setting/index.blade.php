@extends('adminlte::page')

@section('title', 'Настройки')

@section('content_header')
    <h1>Настройки</h1>
@stop

@section('content')

    @include('components.alert')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                @foreach($settings as $item)
                                    <tr>
                                        <th>
                                            <form method="POST" action="{{ route('admin.setting.update', $item->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                                {{ method_field('PATCH') }}
                                                {{ csrf_field() }}
                                                @include ('admin.setting._form')
                                            </form>
                                        </th>
                                        <td>
                                            <form method="POST" action="{{ route('admin.setting.destroy', $item->id) }}" accept-charset="UTF-8" style="display:inline;display:none;">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление&quot;)">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-body">
                        <h2 style="display:none;">Добавить новый параметр</h2>
                        <form method="POST" action="{{ route('admin.setting.store') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" style="display:none;">
                            {{ csrf_field() }}
                            <div class="form-group col-md-12 {{ $errors->has('display_name') ? 'has-error' : ''}}">
                                <label for="display_name" class="control-label">{{ 'Имя' }}</label>
                                <input class="form-control" name="display_name" type="text" id="display_name">
                                {!! $errors->first('display_name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group col-md-12 {{ $errors->has('key') ? 'has-error' : ''}}">
                                <label for="key" class="control-label">{{ 'Ключ(например:logo,text...)' }}</label>
                                <input class="form-control" name="key" type="text" id="key">
                                {!! $errors->first('key', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group col-md-12 {{ $errors->has('type') ? 'has-error' : '' }}">
                                <label for="type" class="control-label">{{ 'Тип' }}</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="text">Однострочный текст</option>
                                    <option value="textarea">Многострочный текст</option>
                                    <option value="editor">Визуальный редактор</option>
                                    <option value="image">Изображение</option>
                                </select>
                                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group col-md-12">
                                <input class="btn btn-primary" type="submit" value="Добавить">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
    <script>
        $(document).ready(function () {
            var summernoteConfig = {
                placeholder: 'контент',
                tabsize: 2,
                height: 300,
                toolbar: [
                    [ 'style', [ 'style' ] ],
                    [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
                    [ 'color', [ 'color' ] ],
                    [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
                    [ 'insert', [ 'link'] ],
                    [ 'view', [ 'codeview'] ]
                ]
            };
            $('#editor').summernote(summernoteConfig);

            IMask(
                document.getElementById('text'),
                {
                    mask: '+ {7} 700 000 0000'
                }
            )
        });
    </script>
@stop

@section('plugins.mask-input', true)
@section('plugins.summernote', true)
