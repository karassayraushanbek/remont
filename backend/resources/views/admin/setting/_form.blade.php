@if($item->type == 'text')
    <div class="form-group col-md-12">
        <label for="text" class="control-label">{{ $item->display_name }}</label>
        <input class="form-control" name="text" type="text" id="text" value="{{ !is_null($item->value) ? $item->value : '' }}">
    </div>
@endif

@if($item->type == 'textarea')
    <div class="form-group col-md-12">
        <label for="textarea" class="control-label">{{ $item->display_name }}</label>
        <textarea class="form-control" name="textarea" id="textarea" rows="10">{{ !is_null($item->value) ? $item->value : '' }}</textarea>
    </div>
@endif

@if($item->type == 'editor')
    <div class="form-group col-md-12">
        <label for="editor" class="control-label">{{ $item->display_name }}</label>
        <textarea class="form-control" name="editor" id="editor" rows="10">{{ !is_null($item->value) ? $item->value : '' }}</textarea>
    </div>
@endif

@if($item->type == 'image')
    <div class="form-group col-md-12">
        <label for="image" class="control-label">{{ $item->display_name }}</label>
        <input class="form-control" name="image" type="file" id="image">
        @if(!is_null($item->value))
            @php
                $path = '';
                if($item->key != 'logo') {
                    $path = 'storage/';
                }
            @endphp
            <img src="{{ url($path.$item->value) }}" alt="" style="max-width:300px; margin:10px;">
        @endif
    </div>
@endif

<div class="form-group col-md-12">
    <input class="btn btn-primary" type="submit" value="Сохранить настройки">
</div>
