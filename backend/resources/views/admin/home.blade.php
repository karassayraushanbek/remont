@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
    <h1>Добро пожаловать!</h1>
@stop

@section('content')
    @include('components.alert')
@stop

@section('css')

@stop

@section('js')

@stop
