<x-forms.simple-image :model="$banner ?? null" name="image" label="Изображение" path="image_url" />

<ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">

    @foreach ($langs as $item)
        <li class="nav-item">
            <a class="nav-link  @if ($loop->first) active @endif"
               id="custom-tabs-one-{{ $item }}-tab" data-toggle="pill" href="#custom-tabs-one-{{ $item }}"
               role="tab" aria-controls="custom-tabs-one-{{ $item }}"
               aria-selected="true">{{ $item }}</a>
        </li>
    @endforeach

</ul>

<div class="tab-content" id="custom-tabs-one-tabContent">

    @foreach ($langs as $lang)
        <div class="tab-pane fade in @if ($loop->first) show active @endif"
             id="custom-tabs-one-{{ $lang }}" role="tabpanel"
             aria-labelledby="custom-tabs-one-{{ $lang }}-tab">

            <x-forms.simple-text-tr :model="$banner ?? null" name="title" label="Название" :lang="$lang" translatable="1" />

            <x-forms.simple-text-area-tr :model="$banner ?? null" name="description" label="Описание" :lang="$lang" editor="0" translatable="1" />

        </div>
    @endforeach

</div>

<input type="hidden" name="page_id" value="{{$page->id}}">

<x-forms.simple-button :formMode="$formMode" />
