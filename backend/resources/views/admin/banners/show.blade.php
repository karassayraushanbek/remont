@extends('adminlte::page')

@section('title', 'Просмотр Баннер')

@section('content_header')
    <h1>Просмотр Баннер</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.banners.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-администратора')
                                <a href="{{ route('admin.banners.edit', ['banner' => $banner]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-администратора')
                                <form method="POST" action="{{ route('admin.banners.destroy', ['banner' => $banner]) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $banner->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $banner->title?->ru }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $banner->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Изображение </th>
                                        <td>
                                            <a href="{{ $banner->image_url }}" data-fancybox="image">
                                                <img id="image-preview" class="rounded" src="{{ $banner->image_url }}" style="max-width: 300px;" alt="{{ $banner->title?->ru }}">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Название </th>
                                        <td> {{ $banner->title?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Описание </th>
                                        <td> {!! $banner->description?->ru !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($banner->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $banner->position }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $banner->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            Fancybox.bind('[data-fancybox="image"]', {
                keyboard: true,
            });
        });
    </script>
@stop

@section('plugins.fancybox', true)
