@extends('adminlte::page')

@section('title', 'Просмотр Теги')

@section('content_header')
    <h1>Просмотр Теги</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.tag-masters.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-тег-мастера')
                                <a href="{{ route('admin.tag-masters.edit', ['tag_master' => $tagMaster]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-тег-мастера')
                                <form method="POST" action="{{ route('admin.tag-masters.destroy', ['tag_master' => $tagMaster]) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $tagMaster->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $tagMaster->title?->ru }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $tagMaster->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Название </th>
                                        <td> {{ $tagMaster->title?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($tagMaster->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $tagMaster->position }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $tagMaster->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
