<x-forms.simple-text :model="$email ?? null" name="email" label="Email" />

<x-forms.simple-button :formMode="$formMode" />
