@extends('adminlte::page')

@section('title', 'Просмотр Email')

@section('content_header')
    <h1>Просмотр Email</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.emails.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-администратора')
                                <a href="{{ route('admin.emails.edit', ['email' => $email]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-администратора')
                                <form method="POST" action="{{ route('admin.emails.destroy', ['email' => $email]) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $email->email }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $email->email }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $email->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Email </th>
                                        <td> {{ $email->phone }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $email->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
