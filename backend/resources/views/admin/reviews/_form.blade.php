{{--<x-forms.simple-image :model="$review ?? null" name="image" label="Изображение" path="image_url" />--}}

{{--<x-forms.simple-select :collection="$blogs ?? null" cname="title" clang="1" :model="$review ?? null" name="blog_id" label="Блог" />--}}

<x-forms.simple-text :model="$review ?? null" name="name" label="Имя" />

<ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">

    @foreach ($langs as $item)
        <li class="nav-item">
            <a class="nav-link  @if ($loop->first) active @endif"
               id="custom-tabs-one-{{ $item }}-tab" data-toggle="pill" href="#custom-tabs-one-{{ $item }}"
               role="tab" aria-controls="custom-tabs-one-{{ $item }}"
               aria-selected="true">{{ $item }}</a>
        </li>
    @endforeach

</ul>

<div class="tab-content" id="custom-tabs-one-tabContent">

    @foreach ($langs as $lang)
        <div class="tab-pane fade in @if ($loop->first) show active @endif"
             id="custom-tabs-one-{{ $lang }}" role="tabpanel"
             aria-labelledby="custom-tabs-one-{{ $lang }}-tab">

            <x-forms.simple-text-area-tr :model="$review ?? null" name="description" label="Комментарий" :lang="$lang" editor="0" translatable="1" />

        </div>
    @endforeach

</div>

<input type="hidden" name="blog_id" value="{{ $blog->id }}">

<x-forms.simple-select-status :model="$review ?? null" name="is_published" label="Статус" on="Опубликована" off="Не опубликована" />

<x-forms.simple-number :model="$review ?? null" name="position" label="Позиция" step="0" />

<x-forms.simple-number :model="$review ?? null" name="rating" label="Рейтинг(от 1 до 5)" step="0" />

<x-forms.simple-button :formMode="$formMode" />
