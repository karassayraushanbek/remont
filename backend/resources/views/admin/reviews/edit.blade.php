@extends('adminlte::page')

@section('title', 'Редактировать Отзывы')

@section('content_header')
    <h1>Редактировать Отзывы</h1>
@stop

@section('content')

    @include('components.alert')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.blogs.reviews.index', $blog) }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                        </div>
                    </div>
                    <div class="card-body">

                        <form method="POST" action="{{ route('admin.blogs.reviews.update', ['blog' => $blog, 'review' => $review]) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('admin.reviews._form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .select2-container .select2-selection--single {
            height: 40px !important;
        }
    </style>
@stop

@section('js')
    <script>
        $(document).ready(function () {
            Fancybox.bind('[data-fancybox="image"]', {
                keyboard: true,
            });
            $('.select2').select2();
        });
    </script>
@stop

@section('plugins.fancybox', true)
@section('plugins.Select2', true)
