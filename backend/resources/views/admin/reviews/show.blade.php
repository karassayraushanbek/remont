@extends('adminlte::page')

@section('title', 'Просмотр Отзывы')

@section('content_header')
    <h1>Просмотр Отзывы</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.blogs.reviews.index', $blog) }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-отзывы')
                                <a href="{{ route('admin.blogs.reviews.edit', ['blog' => $blog, 'review' => $review]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-отзывы')
                                <form method="POST" action="{{ route('admin.blogs.reviews.destroy', ['blog' => $blog, 'review' => $review]) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $review->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $review->name }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $review->id }}</td>
                                    </tr>
{{--                                    <tr>--}}
{{--                                        <th> Изображение </th>--}}
{{--                                        <td>--}}
{{--                                            <a href="{{ $review->image_url }}" data-fancybox="image">--}}
{{--                                                <img id="image-preview" class="rounded" src="{{ $review->image_url }}" style="max-width: 300px;" alt="{{ $review->name }}">--}}
{{--                                            </a>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
                                    <tr>
                                        <th> Имя </th>
                                        <td> {{ $review->name }} </td>
                                    </tr>
                                    <tr>
                                        <th> Комментарий </th>
                                        <td> {!! $review->description?->ru !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($review->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $review->position }} </td>
                                    </tr>
                                    <tr>
                                      <th> Рейтинг </th>
                                      <td> {{ $review->rating }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $review->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            Fancybox.bind('[data-fancybox="image"]', {--}}
{{--                keyboard: true,--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
@stop

@section('plugins.fancybox', true)
