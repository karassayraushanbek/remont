<x-forms.simple-text :model="$subscribe ?? null" name="email" label="Email" />

<x-forms.simple-button :formMode="$formMode" />
