@extends('adminlte::page')

@section('title', 'Просмотр Подписчики')

@section('content_header')
    <h1>Просмотр Подписчики</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.subscribes.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
{{--                            @can('редактирование-подписчики')--}}
{{--                                <a href="{{ route('admin.subscribes.edit', ['subscribe' => $subscribe]) }}" class="btn btn-primary" title="Редактировать">--}}
{{--                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать--}}
{{--                                </a>--}}
{{--                            @endcan--}}
{{--                            @can('удаление-подписчики')--}}
{{--                                <form method="POST" action="{{ route('admin.subscribes.destroy', ['subscribe' => $subscribe]) }}" accept-charset="UTF-8" style="display:inline">--}}
{{--                                    {{ method_field('DELETE') }}--}}
{{--                                    {{ csrf_field() }}--}}
{{--                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $subscribe->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $subscribe->title?->ru }}?&quot;)">--}}
{{--                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить--}}
{{--                                    </button>--}}
{{--                                </form>--}}
{{--                            @endcan--}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $subscribe->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Email </th>
                                        <td> {{ $subscribe->email }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $subscribe->created_at?->format('d.m.Y H:s') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
