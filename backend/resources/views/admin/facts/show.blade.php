@extends('adminlte::page')

@section('title', 'Просмотр Интересные факты')

@section('content_header')
    <h1>Просмотр Интересные факты</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.facts.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-факты')
                                <a href="{{ route('admin.facts.edit', ['fact' => $fact]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-факты')
                                <form method="POST" action="{{ route('admin.facts.destroy', ['fact' => $fact]) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $fact->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $fact->title?->ru }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $fact->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Изображение </th>
                                        <td>
                                            <a href="{{ $fact->image_url }}" data-fancybox="image">
                                                <img id="image-preview" class="rounded" src="{{ $fact->image_url }}" style="max-width: 300px;" alt="{{ $fact->title?->ru }}">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> ALT заголовок </th>
                                        <td> {{ $fact->title?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($fact->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $fact->position }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $fact->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            Fancybox.bind('[data-fancybox="image"]', {
                keyboard: true,
            });
        });
    </script>
@stop

@section('plugins.fancybox', true)
