<ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">

    @foreach ($langs as $item)
        <li class="nav-item">
            <a class="nav-link  @if ($loop->first) active @endif"
               id="custom-tabs-one-{{ $item }}-tab" data-toggle="pill" href="#custom-tabs-one-{{ $item }}"
               role="tab" aria-controls="custom-tabs-one-{{ $item }}"
               aria-selected="true">{{ $item }}</a>
        </li>
    @endforeach

</ul>

<div class="tab-content" id="custom-tabs-one-tabContent">

    @foreach ($langs as $lang)
        <div class="tab-pane fade in @if ($loop->first) show active @endif"
             id="custom-tabs-one-{{ $lang }}" role="tabpanel"
             aria-labelledby="custom-tabs-one-{{ $lang }}-tab">

            <x-forms.simple-text-tr :model="$service ?? null" name="title" label="Заголовок" :lang="$lang" translatable="1" />

            <x-forms.simple-text-area-tr :model="$service ?? null" name="description" label="Описание" :lang="$lang" editor="1" translatable="1" />

        </div>
    @endforeach

</div>

<x-forms.simple-select-status :model="$service ?? null" name="is_published" label="Статус" on="Опубликована" off="Не опубликована" />

<x-forms.simple-text :model="$service ?? null" name="phone" label="Номер телефона" />

<x-forms.simple-number :model="$service ?? null" name="position" label="Позиция" step="0" />

<x-forms.simple-button :formMode="$formMode" />
