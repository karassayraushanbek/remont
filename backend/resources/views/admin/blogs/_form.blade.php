<x-forms.simple-image :model="$blog ?? null" name="image" label="Изображение" path="image_url" />

<x-forms.simple-select :collection="$tags ?? null" cname="title" clang="1" :model="$blog ?? null" name="tag_id" label="Тег" />

<ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">

    @foreach ($langs as $item)
        <li class="nav-item">
            <a class="nav-link  @if ($loop->first) active @endif"
               id="custom-tabs-one-{{ $item }}-tab" data-toggle="pill" href="#custom-tabs-one-{{ $item }}"
               role="tab" aria-controls="custom-tabs-one-{{ $item }}"
               aria-selected="true">{{ $item }}</a>
        </li>
    @endforeach

</ul>

<div class="tab-content" id="custom-tabs-one-tabContent">

    @foreach ($langs as $lang)
        <div class="tab-pane fade in @if ($loop->first) show active @endif"
             id="custom-tabs-one-{{ $lang }}" role="tabpanel"
             aria-labelledby="custom-tabs-one-{{ $lang }}-tab">

            <x-forms.simple-text-tr :model="$blog ?? null" name="title" label="Название" :lang="$lang" translatable="1" />

            <x-forms.simple-text-area-tr :model="$blog ?? null" name="description" label="Описание" :lang="$lang" editor="1" translatable="1" />

            <x-forms.simple-text-tr :model="$blog ?? null" name="meta_title" label="Meta Заголовок" :lang="$lang" translatable="1" />

            <x-forms.simple-text-area-tr :model="$blog ?? null" name="meta_description" label="Meta Описание" :lang="$lang" editor="0" translatable="1" />

            <x-forms.simple-text-area-tr :model="$blog ?? null" name="meta_keywords" label="Meta Ключевые Слова" :lang="$lang" editor="0" translatable="1" />

        </div>
    @endforeach

</div>

<x-forms.simple-number :model="$blog ?? null" name="read" label="Количество читающих" step="0" />

<x-forms.simple-select-status :model="$blog ?? null" name="is_published" label="Статус" on="Опубликована" off="Не опубликована" />

<x-forms.simple-number :model="$blog ?? null" name="position" label="Позиция" step="0" />

<x-forms.simple-button :formMode="$formMode" />
