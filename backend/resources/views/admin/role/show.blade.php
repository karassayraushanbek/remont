@extends('adminlte::page')

@section('title', 'Просмотр Роли')

@section('content_header')
    <h1>Просмотр Роли</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.roles.index') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-роли')
                                <a href="{{ route('admin.roles.edit', $role) }}" title="Редактировать" class="btn btn-primary">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-роли')
                                <form method="POST" action="{{ route('admin.roles.destroy', $role) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $role->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $role->name }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $role->id }}</td>
                                </tr>
                                <tr>
                                    <th>Название</th>
                                    <td>{{ $role->name }}</td>
                                </tr>
                                <tr>
                                    @if(!empty($rolePermissions))
                                        <th>Доступы</th>
                                        <td>
                                            @foreach($rolePermissions as $v)
                                                {{ $v->name }}
                                                <br>
                                            @endforeach
                                        </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Дата создания</th>
                                    <td>{{ $role->created_at?->format('d.m.Y') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
