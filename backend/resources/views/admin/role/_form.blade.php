<x-forms.simple-text :model="$role ?? null" name="name" label="Названия" />

<br/>
<hr>
@if($permission)
    @foreach($permission as $value)
        <label>
            <input class="name" name="permission[]" type="checkbox" value="{{$value->name}}" @if(isset($rolePermissions)){{ in_array($value->id, $rolePermissions) ? 'checked="checked"' : ''}}@endif>
            {{ $value->name }}
        </label>
        <br/>
        @if(strpos($value->name, 'удаление') !== false)
            <hr>
        @endif
    @endforeach
@endif

<x-forms.simple-button :formMode="$formMode" />
