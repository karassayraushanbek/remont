@extends('adminlte::page')

@section('title', 'Просмотр Языки')

@section('content_header')
    <h1>Просмотр Языки</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.language.index') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-языки')
                                <a href="{{ route('admin.language.edit', $language) }}" title="Редактировать" class="btn btn-primary">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-языки')
                                <form method="POST" action="{{ route('admin.language.destroy', $language) }}" accept-charset="UTF-8" style="display:inline;">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $language->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $language->name }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $language->id }}</td>
                                </tr>
                                <tr>
                                    <th>Код</th>
                                    <td>{{ $language->slug }}</td>
                                </tr>
                                <tr>
                                    <th>Названия</th>
                                    <td>{{ $language->name }}</td>
                                </tr>
                                <tr>
                                    <th>Дата</th>
                                    <td>{{ $language->created_at->format('d.m.Y') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
@stop
