@extends('adminlte::page')

@section('title', 'Языки')

@section('content_header')
    <h1>Языки</h1>
@stop

@section('content')

    @include('components.alert')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            @can('создание-языки')
                                <a href="{{ route('admin.language.create') }}" title="Добавить" class="btn btn-success btn-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Добавить
                                </a>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tblData" class="table table-bordered table-striped dataTable dtr-inline">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Код</th>
                                    <th>Названия</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($languages as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$item->slug}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>
                                            @can('все-языки')
                                                <a href="{{ route('admin.language.show', $item) }}" title="Просмотр" class="btn btn-info btn-sm mb-2">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                            @can('редактирование-языки')
                                                <a href="{{ route('admin.language.edit', $item) }}" title="Редактировать" class="btn btn-primary btn-sm mb-2">
                                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                            @can('удаление-языки')
                                                <form method="POST" action="{{ route('admin.language.destroy', $item) }}" accept-charset="UTF-8" style="display:inline;">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm mb-2" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $item->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $item->name }}?&quot;)">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            Языки еще не добавлены!
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="pagination-wrapper">
                                {!! $languages->appends(['search' => Request::get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop

@section('plugins.Datatables', true)
