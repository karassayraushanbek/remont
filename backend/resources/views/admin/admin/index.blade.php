@extends('adminlte::page')

@section('title', 'Администраторы')

@section('content_header')
    <h1>Администраторы</h1>
@stop

@section('content')

    @include('components.alert')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            @can('создание-администратора')
                                <a href="{{ route('admin.admin.create') }}" title="Добавить" class="btn btn-success btn-sm">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Добавить
                                </a>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive" id="tblSort">
                            <table id="tblData" class="table table-bordered table-striped dataTable dtr-inline">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Имя</th>
                                    <th>Email</th>
                                    <th>Роли</th>
                                    <th>Статус</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($admin as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>
                                            @if(!empty($item->getRoleNames()))
                                                @foreach($item->getRoleNames() as $v)
                                                    <a href="{{ route('admin.roles.show', $item->roles[0]?->id) }}">
                                                        <span class="badge rounded-pill bg-dark">{{ $v }}</span>
                                                    </a>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->status == 1)
                                                <span class="badge rounded-pill bg-success">Активирован</span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">Деактивирован</span>
                                            @endif
                                        </td>
                                        <td>
                                            @can('все-администраторы')
                                                <form method="POST" action="{{ route('admin.admin.update.status', $item->id) }}" accept-charset="UTF-8" style="display:inline;">
                                                    {{ method_field('PATCH') }}
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="status" value="{{ $item->status == 1 ? 0 : 1 }}">
                                                    <button type="submit" class="btn btn-{{ $item->status == 1 ? 'danger' : 'success' }} btn-sm mb-2" title="@if($item->status == 1) Деактивировать @else Активировать @endif">
                                                        @if($item->status == 1)
                                                            <i class="fa fa-lock" aria-hidden="true"></i>
                                                        @else
                                                            <i class="fa fa-lock-open" aria-hidden="true"></i>
                                                        @endif
                                                    </button>
                                                </form>

                                                <a href="{{ route('admin.admin.show', $item) }}" title="Просмотр" class="btn btn-info btn-sm mb-2">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                            @can('редактирование-администратора')
                                                <a href="{{ route('admin.admin.edit', $item) }}" title="Редактировать" class="btn btn-primary btn-sm mb-2">
                                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                                                </a>
                                            @endcan
                                            @can('удаление-администратора')
                                                <form method="POST" action="{{ route('admin.admin.destroy', $item) }}" accept-charset="UTF-8" style="display:inline;">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm mb-2" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $item->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $item->name }}?&quot;)">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6" class="text-center">
                                            Администратор еще не добавлен!
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="pagination-wrapper">
                                {!! $admin->appends(['search' => Request::get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop

