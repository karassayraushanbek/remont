@extends('adminlte::page')

@section('title', 'Просмотр Администратора')

@section('content_header')
    <h1>Просмотр Администратора</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.admin.index') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-администратора')
                                <a href="{{ route('admin.admin.edit', $admin) }}" title="Редактировать" class="btn btn-primary">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-администратора')
                                <form method="POST" action="{{ route('admin.admin.destroy', $admin) }}" accept-charset="UTF-8" style="display:inline;">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $admin->name }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $admin->name }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $admin->id }}</td>
                                </tr>
                                <tr>
                                    <th>Имя</th>
                                    <td>{{ $admin->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $admin->email }}</td>
                                </tr>
                                <tr>
                                    <th>Роли</th>
                                    <td>
                                        @if(!empty($admin->getRoleNames()))
                                            @foreach($admin->getRoleNames() as $v)
                                                <a href="{{ route('admin.roles.show', $admin->roles[0]?->id) }}">
                                                    <span class="badge rounded-pill bg-dark">{{ $v }}</span>
                                                </a>
                                            @endforeach
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Статус</th>
                                    <td>
                                        @if($admin->status == 1)
                                            <span class="badge rounded-pill bg-success">Активирован</span>
                                        @else
                                            <span class="badge rounded-pill bg-danger">Деактивирован</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Дата создания</th>
                                    <td>{{ $admin->created_at->format('d.m.Y') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
@stop
