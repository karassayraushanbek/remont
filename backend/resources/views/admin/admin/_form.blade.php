<x-forms.simple-text :model="$admin ?? null" name="name" label="Имя" />

<x-forms.simple-text :model="$admin ?? null" name="email" label="Email" />

<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">@if($formMode == 'create') {{ 'Пароль' }} @else {{ 'Обновить Пароль' }} @endif</label>
    <input class="form-control" name="password" type="text" id="password">
    {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}
</div>
<hr>

@if($formMode == 'create')
    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
        <label for="password_confirmation" class="control-label">{{ 'Повторить Пароль' }}</label>
        <input class="form-control" name="password_confirmation" type="text" id="password_confirmation">
        {!! $errors->first('password_confirmation', '<p class="text-danger">:message</p>') !!}
    </div>
    <hr>
@endif

@if($formMode == 'edit')
    <input name="admin_id" type="hidden" value="{{$admin->id}}">
@endif

@if ($roles)
    <div class="form-group col-md-6 {{ $errors->has('roles') ? 'has-error' : '' }}">
        <label for="roles" class="control-label">{{ 'Роли' }}</label>
        <select name="roles" id="roles" class="form-control js-example-basic-single" required>
            @foreach ($roles as $item)
                <option value="{{ $item }}" @if (isset($adminRole) && $item == $adminRole) selected @endif>
                    <b>{{ $item }}</b>
                </option>
            @endforeach
        </select>
        {!! $errors->first('roles', '<p class="text-danger">:message</p>') !!}
    </div>
    <hr>
@endif

<x-forms.simple-button :formMode="$formMode" />
