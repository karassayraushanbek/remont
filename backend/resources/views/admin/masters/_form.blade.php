<x-forms.simple-image :model="$master ?? null" name="image" label="Изображение" path="image_url" />

<x-forms.simple-text :model="$master ?? null" name="name" label="Имя" />

<ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">

    @foreach ($langs as $item)
        <li class="nav-item">
            <a class="nav-link  @if ($loop->first) active @endif"
               id="custom-tabs-one-{{ $item }}-tab" data-toggle="pill" href="#custom-tabs-one-{{ $item }}"
               role="tab" aria-controls="custom-tabs-one-{{ $item }}"
               aria-selected="true">{{ $item }}</a>
        </li>
    @endforeach

</ul>

<div class="tab-content" id="custom-tabs-one-tabContent">

    @foreach ($langs as $lang)
        <div class="tab-pane fade in @if ($loop->first) show active @endif"
             id="custom-tabs-one-{{ $lang }}" role="tabpanel"
             aria-labelledby="custom-tabs-one-{{ $lang }}-tab">

            <x-forms.simple-text-tr :model="$master ?? null" name="title" label="Расстояние(км)" :lang="$lang" translatable="1" />

            <x-forms.simple-text-tr :model="$master ?? null" name="description" label="Опыт работы" :lang="$lang" translatable="1" />

        </div>
    @endforeach

</div>

<x-forms.simple-number :model="$master ?? null" name="reviews" label="Количество отызвов" step="0" />

<x-forms.simple-select-status :model="$master ?? null" name="is_published" label="Статус" on="Опубликована" off="Не опубликована" />

<x-forms.simple-number :model="$master ?? null" name="position" label="Позиция" step="0" />

<div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
    <label for="tags" class="control-label">{{ 'Теги' }}</label>
    <select name="tags[]" id="tags" class="form-control select2" multiple="multiple" data-placeholder="Выберите тег">
        @foreach($tags as $item)
            <option value="{{ $item->id }}" {{ isset($master) && $master->tagMasters->contains($item->id) ? 'selected' : '' }}>
                {{ $item->title?->ru }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('tags', '<p class="help-block">:message</p>') !!}
</div>

<x-forms.simple-button :formMode="$formMode" />
