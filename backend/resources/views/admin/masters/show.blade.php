@extends('adminlte::page')

@section('title', 'Просмотр Мастера')

@section('content_header')
    <h1>Просмотр Мастера</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.masters.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-мастера')
                                <a href="{{ route('admin.masters.edit', ['master' => $master]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-мастера')
                                <form method="POST" action="{{ route('admin.masters.destroy', ['master' => $master]) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $master->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $master->title?->ru }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $master->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Изображение </th>
                                        <td>
                                            <a href="{{ $master->image_url }}" data-fancybox="image">
                                                <img id="image-preview" class="rounded" src="{{ $master->image_url }}" style="max-width: 300px;" alt="{{ $master->name }}">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Имя </th>
                                        <td> {{ $master->name }} </td>
                                    </tr>
                                    <tr>
                                        <th> Кол-во отзывов </th>
                                        <td> {{ $master->reviews }} </td>
                                    </tr>
                                    <tr>
                                        <th> Расстояние(км) </th>
                                        <td> {{ $master->title?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Опыт работы </th>
                                        <td> {!! $master->description?->ru !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Теги </th>
                                        <td>
                                            @forelse($master->tagMasters as $tag)
                                                <a href="{{ route('admin.tag-masters.show', ['tag_master' => $tag]) }}">{{ $tag->title?->ru }}</a><br>
                                            @empty
                                                Теги не найдены
                                            @endforelse
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($master->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $master->position }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $master->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            Fancybox.bind('[data-fancybox="image"]', {
                keyboard: true,
            });
        });
    </script>
@stop

@section('plugins.fancybox', true)
