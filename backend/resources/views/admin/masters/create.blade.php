@extends('adminlte::page')

@section('title', 'Добавить Мастера')

@section('content_header')
    <h1>Добавить Мастера</h1>
@stop

@section('content')

    @include('components.alert')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.masters.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                        </div>
                    </div>
                    <div class="card-body">

                        <form method="POST" action="{{ route('admin.masters.store') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('admin.masters._form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .select2-container .select2-selection--single {
            height: 40px !important;
        }
    </style>
@stop

@section('js')
    <script>
        $(document).ready(function () {
            $('.select2').select2();
        });
        $(document).on('change', '.photo', function(){
            const file = this.files[0];
            var previewElement = $('#image-preview-image');
            previewElement.css('display', 'block');
            if (file){
                let reader = new FileReader();
                reader.onload = function(event){
                    previewElement.attr('src', event.target.result);
                }
                reader.readAsDataURL(file);
            }
        });
    </script>
@stop

@section('plugins.Select2', true)
