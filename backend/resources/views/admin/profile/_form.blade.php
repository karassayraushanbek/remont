<x-forms.simple-text :model="$user ?? null" name="name" label="Имя" />

<x-forms.simple-text :model="$user ?? null" name="email" label="Email" />

<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'Обновить пароль' }}</label>
    <input class="form-control" name="password" type="text" id="password">
    {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}
</div>
<hr>

<input name="admin_id" type="hidden" value="{{$user->id}}">

<x-forms.simple-button :formMode="$formMode" />
