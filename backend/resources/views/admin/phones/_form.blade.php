<x-forms.simple-text :model="$phone ?? null" name="phone" label="Номер телефона" />

<x-forms.simple-button :formMode="$formMode" />
