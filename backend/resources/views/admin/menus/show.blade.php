@extends('adminlte::page')

@section('title', 'Просмотр Меню')

@section('content_header')
    <h1>Просмотр Меню</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.menus.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-меню')
                                <a href="{{ route('admin.menus.edit', ['menu' => $menu]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
{{--                            @can('удаление-меню')--}}
{{--                                <form method="POST" action="{{ route('admin.menus.destroy', ['menu' => $menu]) }}" accept-charset="UTF-8" style="display:inline">--}}
{{--                                    {{ method_field('DELETE') }}--}}
{{--                                    {{ csrf_field() }}--}}
{{--                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $menu->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $menu->title?->ru }}?&quot;)">--}}
{{--                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить--}}
{{--                                    </button>--}}
{{--                                </form>--}}
{{--                            @endcan--}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $menu->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Название </th>
                                        <td> {{ $menu->title?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($menu->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $menu->position }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $menu->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
