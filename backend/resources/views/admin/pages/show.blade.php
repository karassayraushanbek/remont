@extends('adminlte::page')

@section('title', 'Просмотр Страниц')

@section('content_header')
    <h1>Просмотр Страниц</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.pages.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-страницы')
                                <a href="{{ route('admin.pages.edit', ['page' => $page]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
{{--                            @can('удаление-страницы')--}}
{{--                                <form method="POST" action="{{ route('admin.pages.destroy', ['page' => $page]) }}" accept-charset="UTF-8" style="display:inline">--}}
{{--                                    {{ method_field('DELETE') }}--}}
{{--                                    {{ csrf_field() }}--}}
{{--                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $page->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $page->title?->ru }}?&quot;)">--}}
{{--                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить--}}
{{--                                    </button>--}}
{{--                                </form>--}}
{{--                            @endcan--}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $page->id }}</td>
                                    </tr>
{{--                                    <tr>--}}
{{--                                        <th> Меню </th>--}}
{{--                                        <td> {{ $page->menu?->title?->ru }} </td>--}}
{{--                                    </tr>--}}
                                    <tr>
                                        <th> Название </th>
                                        <td> {{ $page->title?->ru }} </td>
                                    </tr>
{{--                                    <tr>--}}
{{--                                        <th> Описание </th>--}}
{{--                                        <td> {!! $page->description?->ru !!} </td>--}}
{{--                                    </tr>--}}
                                    <tr>
                                        <th> Meta Заголовок </th>
                                        <td> {{ $page->meta_title?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Meta Описание </th>
                                        <td> {!! $page->meta_description?->ru !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Meta Ключевые слова </th>
                                        <td> {!! $page->meta_keywords?->ru !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Ссылка(Уникальная) </th>
                                        <td> {{ $page->slug }} </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($page->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $page->position }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $page->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
