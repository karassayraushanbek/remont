@extends('adminlte::page')

@section('title', 'Просмотр Теги')

@section('content_header')
    <h1>Просмотр Теги</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <a href="{{ route('admin.tags.index') }}" class="btn btn-warning" title="Назад">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
                            </a>
                            @can('редактирование-тег-блог')
                                <a href="{{ route('admin.tags.edit', ['tag' => $tag]) }}" class="btn btn-primary" title="Редактировать">
                                    <i class="fa fa-fw fa-edit" aria-hidden="true"></i> Редактировать
                                </a>
                            @endcan
                            @can('удаление-тег-блог')
                                <form method="POST" action="{{ route('admin.tags.destroy', ['tag' => $tag]) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger" title="Удалить" onclick="return confirm(&quot;Подтвердите удаление {{ $tag->title?->ru }}!&quot;) && confirm(&quot;Вы уверены что хотите удалить {{ $tag->title?->ru }}?&quot;)">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $tag->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Название </th>
                                        <td> {{ $tag->title?->ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Ссылка(Уникальная) </th>
                                        <td> {{ $tag->slug }} </td>
                                    </tr>
                                    <tr>
                                        <th> Статус </th>
                                        <td>
                                            @if($tag->is_published == 1)
                                                <span class="badge rounded-pill bg-success">
                                                    Опубликована
                                                </span>
                                            @else
                                                <span class="badge rounded-pill bg-danger">
                                                    Не опубликована
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Позиция </th>
                                        <td> {{ $tag->position }} </td>
                                    </tr>
                                    <tr>
                                        <th> Дата </th>
                                        <td> {{ $tag->created_at?->format('d.m.Y') }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
