@extends('layouts.mail')

@section('email-content')
    <div>
        <h4>{{ $title }}</h4>
        <b>ID: </b> {{ $user->id }} <br>
        <b>Имя: </b> {{ $user->name }} <br>
        <b>Email: </b> {{ $user->email }} <br>
        <b>Пароль: </b> {{ $password }} <br>
        <b>Дата Создании: </b> {{ $user->created_at?->format('d.m.Y H:i') }} <br>
        <b>Дата Обновлении: </b> {{ $user->updated_at?->format('d.m.Y H:i') }} <br>
    </div>
@endsection
