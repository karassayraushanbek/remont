#!/bin/bash

git pull
sudo docker compose --env-file ./backend/.env --env-file ./frontend/.env up -d --build
sudo docker compose --env-file ./backend/.env --env-file ./frontend/.env exec remont-backend bash -c "php artisan migrate --force"
sudo docker system prune -f
sudo df / -h
