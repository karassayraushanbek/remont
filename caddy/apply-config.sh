#!/bin/bash

docker compose -f ../docker-compose.yml --env-file ../backend/.env --env-file ../frontend/.env exec remont-caddy /bin/ash -c "cd /etc/caddy/ && sh /etc/caddy/apply-config.sh"
