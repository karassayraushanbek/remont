#!/bin/bash

caddy fmt --overwrite
caddy fmt --overwrite ./sites-enabled/*
caddy validate && caddy validate ./sites-enabled/* && caddy reload
