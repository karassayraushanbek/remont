#!/bin/bash

git config --global user.email "zolotarev@its.bz"
git config --global user.name "Andrey Zolotarev"
(cd frontend && cp .env.example .env && vim .env) # Заполнить используемый домен если запускаем не на ПРОД
(cd backend && cp .env.example .env && vim .env)  # Заполнить параметры, если нестандартные
