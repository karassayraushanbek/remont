sudo docker compose --env-file ./backend/.env --env-file ./frontend/.env exec remont-backend bash -c "php artisan optimize:clear"
sudo docker compose --env-file ./backend/.env --env-file ./frontend/.env exec remont-backend bash -c "php artisan migrate:refresh --seed"
#docker compose --env-file ./backend/.env --env-file ./frontend/.env exec remont-backend bash -c "php artisan migrate"
#docker compose --env-file ./backend/.env --env-file ./frontend/.env exec remont-backend bash -c "php artisan db:seed"
sudo chmod -v a+w ./backend/storage -R
sudo chmod -v a+w ./backend/bootstrap -R
sudo docker compose --env-file ./backend/.env --env-file ./frontend/.env exec remont-backend bash -c "php artisan storage:link"
